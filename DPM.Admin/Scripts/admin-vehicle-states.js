﻿"use strict";
// Class definition

var KTDatatableRemoteAjaxDemo = function () {
    // Private functions

    // basic demo
    var demo = function () {

        var status = {
            1: { 'title': 'Oturum Başlatıldı', 'state': 'kt-badge--primary' },
            2: { 'title': 'Oturum Kapatıldı', 'state': 'kt-badge--dark' },
            3: { 'title': 'Aktif Çalışma', 'state': 'kt-badge--success' },
            4: { 'title': 'Çalışma Bitirildi', 'state': 'kt-badge--danger' },
            5: { 'title': 'Mola', 'state': 'kt-badge--info' },
            6: { 'title': 'Mola Bitirildi-Çalışma Başladı', 'state': 'kt-badge--dark' },
            7: { 'title': 'Arıza', 'state': 'kt-badge--warning' },
            8: { 'title': 'Arıza Bitti', 'state': 'kt-badge--danger' },
            9: { 'title': 'Yemek', 'state': 'kt-badge--secondary' },
            10: { 'title': 'Yemek Bitti-Çalışma Başladı', 'state': 'kt-badge--dark' },

        };

        window.AdminVehicleStateTable = $('.kt-datatable-state').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: appBase.ApiUrl + "api/Admin/GetVehicleStates",
                        params: { Token: appBase.Helpers.GetCookie("Token") },
                        // sample custom headers
                        //headers: { 'x-my-custokt-header': 'some value', 'x-test-header': 'the value' },
                        //map: function (raw) {
                        //    // sample data mapping
                        //    var dataSet = raw;
                        //    if (typeof raw.data !== 'undefined') {
                        //        dataSet = raw.data;
                        //    }
                        //    return dataSet;
                        //},
                        method: 'get',
                    },
                },
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
                class: 'kt-datatable--brand',
            },

            // column sorting
            sortable: true,

            pagination: true,
            serverSorting: false,

            //search: {
            //    input: $('#generalSearch'),
            //},

            // columns definition
            columns: [
                {
                    field: 'Vehicletype',
                    title: 'Araç Tipi',
                    width: 150,
                    autoHide: false,
                    template: function (row) {

                        var result = '';

                        switch (row.Vehicletype) {
                            case 1:
                                result = 'RTG';
                                break;
                            case 2:
                                result = 'ITV';
                                break;
                            case 3:
                                result = 'RS';
                                break;
                            default:
                                result = 'RTG';
                                break;
                        }

                        return result;
                    }
                },
                {
                    field: 'VehicleName',
                    title: 'Araç Adı',
                    autoHide: false
                },
                {
                    field: 'CurrentState',
                    title: 'Şuan ki durumu',
                    template: function (row) {
                        return '<span class="kt-badge ' + status[row.CurrentStateType].state + ' kt-badge--inline kt-badge--pill">' + status[row.CurrentStateType].title + '</span>';
                    },
                },
                {
                    //width: 40,
                    field: 'UserName',
                    title: 'Kullanan Operatör',
                    template: function (row) {
                        if (row.UserName == null) {
                            return '-';
                        }
                        return row.UserName;
                    },
                    autoHide: false
                },
                {
                    field: 'Actions',
                    title: 'Aksiyon',
                    autoHide: false,
                    template: function (row) {
                        if (row.CurrentStateType == appBase.EVENTS.FAULT_STARTED) {//Arızalıysa
                            return '<button type="button" class="btn btn-warning" id="FaultEnd" data-id="' + row.VehicleId+'">Arızayı Bitir</button>';

                        }
                        else {//değilse
                            //return '<button type="button" class="btn btn-danger" id="FaultStart" data-id="' + row.VehicleId +'">Operasyonu Durdur</button>';
                            return '-';

                        }
                    }
                },
                {
                    field: 'Order',
                    autoHide: true,
                },
            ],
        });

    };

    return {
        // public functions
        init: function () {
            demo();
        },
    };
}();

jQuery(document).ready(function () {
    //KTDatatableRemoteAjaxDemo.init();
});