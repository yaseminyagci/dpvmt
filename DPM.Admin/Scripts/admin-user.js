﻿; (function () {

    var DEF = {
        ELEMENTS: {
            DataTable: null,
            Table: $('.kt-datatable'),
            AddButton: $('#UserAdd'),
            UserModal: $('#newUser'),
            FirstName: $('#FirstName'),
            LastName: $('#LastName'),            
            Email: $('#Email'),            
            Password: $('#Password'),
            Text: $('#Text'),
            Rows: ('.clickable'),
            EditModal: $('#editUser'),
            EditButton: $('#userEdit'),
            UserId: $('#UserId'),
            Remove: $('#remove')
        },

        HELPERS: {
            InitTable: function (jsonData) {

                DEF.ELEMENTS.DataTable = DEF.ELEMENTS.Table.KTDatatable({
                    // datasource definition
                    data: {
                        type: 'local',
                        source: jsonData,
                        pageSize: 10,
                    },

                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        footer: false // display/hide footer
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#generalSearch')
                    },

                    // columns definition
                    columns: [
                        //{
                        //    field: 'UserId',
                        //    title: 'User Id',
                        //    autoHide: true
                        //},
                        {
                            field: 'FirstName',
                            title: 'Kullanıcı Adı',
                            width: 60,
                            autoHide: false
                        },
                        {
                            field: 'LastName',
                            title: 'Kullanıcı Soyadı',
                            width: 60,
                            autoHide: false
                        },
                        {
                            field: 'Email',
                            title: 'Kullanıcı Maili',
                            autoHide: false
                        },
                        {
                            field: 'Password',
                            title: 'Kullanıcı Şifresi',
                            autoHide: false
                        }//,
                        //{
                        //    field: 'Delete',
                        //    title: 'Sil',
                        //    template: function (row) {
                        //        console.log(row.UserId);
                        //        return '<button type="button" class="btn btn-danger btn-elevate btn-circle btn-icon removeButton" data-id="' + row.UserId + '" title="Sil"><i class="fa fa-trash"></i></button>';
                        //    } 
                        //}, {
                        //    field: 'Edit',
                        //    title: 'Düzenle',
                        //    template: function (row) {
                        //        console.log(row.UserId);
                               
                        //        return '<button type="button" class="btn btn-danger btn-elevate btn-circle btn-icon editButton" data-toggle="modal" data-target="#newUser" data-id="' + row.UserId + '" title="Düzenle"><i class="fa fa-trash"></i></button>';
                        //    }
                        //}
                        //{
                        //    field: 'CreatedDate',
                        //    title: 'Oluşturulma Tarihi',
                        //    type: 'date',
                        //    format: 'DD/MM/YYYY',
                        //},
                        //{
                        //    field: 'UpdatedDate',
                        //    title: 'Güncelleme Tarihi',
                        //    type: 'date',
                        //    format: 'DD/MM/YYYY',
                        //},
                        //{
                        //    field: 'Operators',
                        //    title: 'Operatörler',
                        //    template: function (row) {

                        //        var label = "";

                        //        $.each(function (i, v) {

                        //        })

                        //        return label;
                        //    }
                        //},
                    ],
                    rows: {
                        callback: function (row, data, index) {

                            $(row).addClass('clickable');
                            $(row).data('id', data.UserId);
                            $(row).attr('data-toggle', 'modal');
                            $(row).attr('data-target', '#editUser');

                        }
                    }

                });

            },
            GetQuestions: function () {
                appBase.Helpers.Request('api/admin/GetAllUsers', 'get', {}, function (response) {
                    if (response.Code == 200) {

                        DEF.HELPERS.InitTable(response.Data);
                    }
                });

            }
        },

        MODELS: {
            PostModel: {
                FirstName: '',
                LastName: '',
                Email: '',
                Password: '',
            }
        },

        BindEvents: function () {
            DEF.HELPERS.GetQuestions();
            DEF.ELEMENTS.AddButton.on('click', function (e) {
                e.preventDefault();

                //var model = [];

                //model.push({
                //    VehicleName: DEF.ELEMENTS.VehicleName.val(),
                //    VehicleType: DEF.ELEMENTS.VehicleType.val()
                //})

                //DEF.MODELS.VehicleModel.push(model);
                debugger
                var model = {
                    FirstName: DEF.ELEMENTS.FirstName.val(),
                    LastName: DEF.ELEMENTS.LastName.val(),                    
                    Email: DEF.ELEMENTS.Email.val(),
                    Password: DEF.ELEMENTS.Password.val()                  
                }

                KTApp.blockPage()

                appBase.Helpers.Request('/api/Admin/AddUser', 'post', model, function (response) {

                    KTApp.unblockPage()

                    if (response.Code == 200) {
                        swal.fire("Başarı ile eklenmiştir").then(function () { window.location.reload(); });
                    }
                })
            });

            $(document).on('click', '.clickable', function (e) {

                var id = $(this).data('id');

                KTApp.blockPage({
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'success',
                    message: 'Lütfen bekleyiniz...'
                });

                DEF.ELEMENTS.UserId.val(id);
             
                appBase.Helpers.Request("/api/admin/GetUserById?id=" + id, "get", {}, function (response) {

                 

                    DEF.ELEMENTS.EditModal.find('#FirstName').val(response.Data.FirstName);
                    DEF.ELEMENTS.EditModal.find('#LastName').val(response.Data.LastName);
                    DEF.ELEMENTS.EditModal.find('#Email').val(response.Data.Email);
                    DEF.ELEMENTS.EditModal.find('#Password').val(response.Data.Password);
                    KTApp.unblockPage();

                }, function (error) {
                    KTApp.unblockPage();

                })
            });

            $(document).on('click', '#userEdit', function (e) {
              
                var model = {
                    UserId:    DEF.ELEMENTS.UserId.val(),
                    FirstName: DEF.ELEMENTS.EditModal.find('#FirstName').val(),
                    LastName:  DEF.ELEMENTS.EditModal.find('#LastName').val(),
                    Email:     DEF.ELEMENTS.EditModal.find('#Email').val(),
                    Password:  DEF.ELEMENTS.EditModal.find('#Password').val()
                    
                };

                debugger
                appBase.Helpers.Request("/api/admin/UpdateUser", "post", model, function (response) {
                    if (response.Code == 200) {
                        swal.fire("Başarı ile düzenlenmiştir.").then(
                            function () { window.location.reload(); })
                    }
                })

            });
           
            $(document).on('click', '#Remove', function (e) {

                //var id = DEF.ELEMENTS.QuestionId.val();

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Seçmiş olduğunuz kullanıcı kaldırılacak?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/admin/RemoveUser?userId=' + DEF.ELEMENTS.UserId.val(), 'get', {}, function (response) {
                            if (response.Code == 200) {
                                KTApp.unblockPage();

                                swal.fire("Başarı ile silinniştir.").then(
                                    function () { window.location.reload(); })
                            }
                            else {
                                Swal.fire(response.Message);
                            }
                        });
                    }
                });

            });

            $(document).on('click', '.removeButton', function (e) {

                e.preventDefault();

                var vId = $(this).data('id');

                Swal.fire({
                    title: 'Kullanıcıyı silmek istediğinize emin misiniz?',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/admin/RemoveUser', 'get', { userId: vId }, function (response) {

                            KTApp.unblockPage();

                            if (response.Code == 200) {

                                appBase.Helpers.ToastrMsg("success", "Başarıyla kaldırıldı, sayfa tekrar yükleniyor.");

                                window.location.reload();

                            }
                            else {
                                appBase.Helpers.ToastrMsg("error", "Bir hata oluştu");

                            }

                        }, function (error) {

                            KTApp.unblockPage();

                            appBase.Helpers.ToastrMsg("error", "Bir hata oluştu");


                        });
                        

                    }
                });
            });

          

        }
    }


    $(function () {
        DEF.BindEvents();
        
      
              
    });

})();

