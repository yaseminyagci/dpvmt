﻿; (function () {

    var DEF = {
        ELEMENTS: {
            DataTable: null,
            Table: $('.kt-datatable'),
            AddButton: $('#VehicleAdd'),
            VehicleModal: $('#newVehicle'),
            VehicleName: $('#VehicleName'),
            VehicleType: $('#VehicleType'),
            MaccAddress: $('#MaccAddress'),
            IP: $('#IP'),
            SerialNumber: $('#SerialNumber'),
            Model: $('#Model'),
            ID: $('#ID'),
            Description: $('#Description')

        },

        HELPERS: {
            InitTable: function (jsonData) {

                DEF.ELEMENTS.DataTable = DEF.ELEMENTS.Table.KTDatatable({
                    // datasource definition
                    data: {
                        type: 'local',
                        source: jsonData,
                        pageSize: 10,
                    },

                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        footer: false // display/hide footer
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#generalSearch')
                    },

                    // columns definition
                    columns: [
                        //{
                        //    field: 'VehicleId',
                        //    title: 'Araç ID',
                        //},
                        {
                            field: 'VehicleName',
                            title: 'Araç Adı',
                            width: 60,
                            autoHide: false
                        },
                        {
                            field: 'MacAddress',
                            title: 'Mac Adresi',
                            autoHide: false
                        },
                        {
                            field: 'Ip',
                            title: 'Ip',
                            autoHide: false
                        },
                        {
                            field: 'SerialNumber',
                            title: 'Seri Numarası',
                            autoHide: false
                        },
                        {
                            field: 'Description',
                            title: 'Açıklama',
                            autoHide: false
                        },
                        {
                            field: 'VehicleType',
                            title: 'Araç Tipi',
                            autoHide: false
                        },
                        {
                            field: 'Actions',
                            title: 'Aksiyon',
                            template: function (row) {
                                console.log(row.VehicleId);
                                return '<button type="button" class="btn btn-danger btn-elevate btn-circle btn-icon removeButton" data-id="' + row.VehicleId + '" title="Sil"><i class="fa fa-trash"></i></button>';
                            }

                        }
                        //{
                        //    field: 'CreatedDate',
                        //    title: 'Oluşturulma Tarihi',
                        //    type: 'date',
                        //    format: 'DD/MM/YYYY',
                        //},
                        //{
                        //    field: 'UpdatedDate',
                        //    title: 'Güncelleme Tarihi',
                        //    type: 'date',
                        //    format: 'DD/MM/YYYY',
                        //},
                        //{
                        //    field: 'Operators',
                        //    title: 'Operatörler',
                        //    template: function (row) {

                        //        var label = "";

                        //        $.each(function (i, v) {

                        //        })

                        //        return label;
                        //    }
                        //},
                    ],

                });

            },
            GetQuestions: function () {
                appBase.Helpers.Request('api/admin/GetAllVehicles', 'get', {}, function (response) {
                    if (response.Code == 200) {

                        DEF.HELPERS.InitTable(response.Data);
                    }
                });

            }
        },

        MODELS: {
            VehicleModel: [],
        },

        BindEvents: function () {
            DEF.HELPERS.GetQuestions();
            DEF.ELEMENTS.AddButton.on('click', function (e) {
                e.preventDefault();

                if (DEF.ELEMENTS.VehicleName.val() == '') {
                    return false;

                }

                //var model = [];

                //model.push({
                //    VehicleName: DEF.ELEMENTS.VehicleName.val(),
                //    VehicleType: DEF.ELEMENTS.VehicleType.val()
                //})

                //DEF.MODELS.VehicleModel.push(model);

                var model = {
                    VehicleName: DEF.ELEMENTS.VehicleName.val(),
                    VehicleType: DEF.ELEMENTS.VehicleType.val(),
                    Description: DEF.ELEMENTS.Description.val(),
                    SerialNumber: DEF.ELEMENTS.SerialNumber.val(),
                    Model: DEF.ELEMENTS.Model.val(),
                    Ip: DEF.ELEMENTS.IP.val(),
                    ItemId: DEF.ELEMENTS.ID.val(),
                    MacAddress: DEF.ELEMENTS.MaccAddress.val()
                }

                KTApp.blockPage()

                appBase.Helpers.Request('/api/Admin/AddVehicle', 'post', model, function (response) {

                    KTApp.unblockPage()

                    if (response.Code == 200) {
                        swal.fire("Başarı ile eklenmiştir").then(function () { window.location.reload(); });
                    }
                })
            });

            $(document).on('click', '.removeButton', function (e) {

                e.preventDefault();

                var vId = $(this).data('id');

                Swal.fire({
                    title: 'Aracı silmek istediğinize emin misiniz?',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/admin/RemoveVehicle', 'get', { vehicleId: vId }, function (response) {

                            KTApp.unblockPage();

                            if (response.Code == 200) {

                                appBase.Helpers.ToastrMsg("success", "Başarıyla kaldırıldı, sayfa tekrar yükleniyor.");

                                window.location.reload();

                            }
                            else {
                                appBase.Helpers.ToastrMsg("error", "Bir hata oluştu");

                            }

                        }, function (error) {

                            KTApp.unblockPage();

                            appBase.Helpers.ToastrMsg("error", "Bir hata oluştu");


                        });
                        

                    }
                });
            });

        }
    }


    $(function () {
        DEF.BindEvents();
    });

})();

