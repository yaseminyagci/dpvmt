﻿; (() => {
    var DEF = {
        BIND_EVENTS: function () {
            DEF.ELEMENTS.SaveButton.on('click', function (e) {
                var $this = $(this);
                var buttonType = $this.data("buttontype");

                var model = {
                    ACategoryMail: DEF.ELEMENTS.ACategoryMail.is(':checked'),
                    ACategoryFault: DEF.ELEMENTS.ACategoryFault.is(':checked'),
                    BCategoryFault: DEF.ELEMENTS.BCategoryFault.is(':checked'),
                    BCategoryMail: DEF.ELEMENTS.BCategoryMail.is(':checked'),
                    CCategoryFault: DEF.ELEMENTS.CCategoryFault.is(':checked'),
                    CCategoryMail: DEF.ELEMENTS.CCategoryMail.is(':checked'),
                };
                

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Değişiklikler kaydedilecektir.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/admin/ChangeSurveyOptions', 'post', model, function (response) {
                            if (response.Code == 200) {
                                KTApp.unblockPage();
                                DEF.HELPERS.SetButtonStatus();

                                appBase.Helpers.ToastrMsg('success', 'İşlem başarıyla kaydedildi');

                            }
                            else {
                                KTApp.unblockPage();

                                appBase.Helpers.ToastrMsg('error', 'bir hata oluştu');

                            }
                        });
                    }
                });
            });
        },

        ELEMENTS: {
            ACategoryMail: $('input[name="ACategoryMail"]'),
            ACategoryFault: $('input[name="ACategoryFault"]'),
            BCategoryMail: $('input[name="BCategoryMail"]'),
            BCategoryFault: $('input[name="BCategoryFault"]'),
            CCategoryMail: $('input[name="CCategoryMail"]'),
            CCategoryFault: $('input[name="CCategoryFault"]'),
            SaveButton: $('button[name="Save"]'),
        },

        HELPERS: {
            SetButtonStatus: function () {
                appBase.Helpers.Request('api/admin/CheckSurveyOptions', 'get', {}, function (response) {
                    if (response.Code == 200) {

                        var responseData = response.Data;

                        responseData.ACategoryMail ? DEF.ELEMENTS.ACategoryMail.attr('checked', 'checked') : DEF.ELEMENTS.ACategoryMail.removeAttr('checked');
                        responseData.ACategoryFault ? DEF.ELEMENTS.ACategoryFault.attr('checked', 'checked') : DEF.ELEMENTS.ACategoryFault.removeAttr('checked');
                        responseData.BCategoryMail ? DEF.ELEMENTS.BCategoryMail.attr('checked', 'checked') : DEF.ELEMENTS.BCategoryMail.removeAttr('checked');
                        responseData.BCategoryFault ? DEF.ELEMENTS.BCategoryFault.attr('checked', 'checked') : DEF.ELEMENTS.BCategoryFault.removeAttr('checked');
                        responseData.CCategoryMail ? DEF.ELEMENTS.CCategoryMail.attr('checked', 'checked') : DEF.ELEMENTS.CCategoryMail.removeAttr('checked');
                        responseData.CCategoryFault ? DEF.ELEMENTS.CCategoryFault.attr('checked', 'checked') : DEF.ELEMENTS.CCategoryFault.removeAttr('checked');

                        KTApp.unblockPage();
                    }
                }, function (error) {

                    KTApp.unblockPage();

                    appBase.Helpers.ToastrMsg('error', 'bir hata oluştu');

                });
            }
        },
    }

    $(function () {

        KTApp.blockPage();

        DEF.HELPERS.SetButtonStatus();
        DEF.BIND_EVENTS();


    });

})();
