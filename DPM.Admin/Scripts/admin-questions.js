﻿; (function () {

    var DEF = {
        ELEMENTS: {
            DataTable: null,
            Table: $('.kt-datatable'),
            Modal: $('#newQuestion'),
            VehicleType: $('#VehicleType'),
            QuestionCategory: $('#QuestionCategory'),
            GroupType: $('#GroupType'),
            Text: $('#Text'),
            AddButton: $('#QuestionAdd'),
            Rows: ('.clickable'),
            EditModal: $('#editQuestion'),
            EditButton: $('#QuestionEdit'),
            QuestionId: $('#QuestionId'),
            Remove: $('#remove'),
        },

        HELPERS: {
            InitTable: function (jsonData) {

                DEF.ELEMENTS.DataTable = DEF.ELEMENTS.Table.KTDatatable({
                    // datasource definition
                    data: {
                        type: 'local',
                        source: jsonData,
                        pageSize: 10,
                    },

                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        footer: false // display/hide footer
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#generalSearch')
                    },

                    // columns definition
                    columns: [
                        {
                            field: 'QuestionId',
                            title: 'Order ID',
                            autoHide: false
                        },
                        {
                            field: 'Text',
                            title: 'Soru',
                            //template: function (row) {
                            //    return row.Country + ' ' + row.ShipCountry;
                            //},
                            autoHide: false,
                        },
                        {
                            field: 'Category',
                            title: 'Kategori',
                            autoHide: false
                        },
                        {
                            field: 'VehicleType',
                            title: 'Araç Tipi',
                            autoHide: false
                        },
                        {
                            field: 'GroupType',
                            title: 'Grup',
                            template: function (row) {
                                if (row == 1) {
                                    return "Vardiya Öncesi";
                                }
                                else {
                                    return "Vardiya Sonrası";
                                }
                            },
                            autoHide: false
                        },
                        //{
                        //    field: 'Status',
                        //    title: 'Aksiyon',
                        //    template: function (row) {
                        //        return '<button type="button" class="btn btn-danger remove">Sil</button>';
                        //    },
                        //    autoHide: false
                        //},
                    ],
                    rows: {
                        callback: function (row, data, index) {

                            $(row).addClass('clickable');
                            $(row).data('id', data.QuestionId);
                            $(row).attr('data-toggle', 'modal');
                            $(row).attr('data-target', '#editQuestion');

                        }
                    }

                });

            },
            GetQuestions: function () {
                appBase.Helpers.Request('api/admin/GetAllQuestions', 'get', {}, function (response) {
                    if (response.Code == 200) {

                        DEF.HELPERS.InitTable(response.Data);
                    }
                });
            },
            LoadVehicleType: function () {
                appBase.Helpers.Request('/api/Admin/GetVehicleType', 'get', {}, function (response) {

                    if (response.Code != 200) {
                        swal.fire("Sistem hatası lütfen yöneticinize başvurun");
                    }

                    var customVehiclesTypeSelect2 = [{ id: '', text: 'Select a vehicle type' }];

                    $.each(response.Data, function (i, item) {
                        customVehiclesTypeSelect2.push({ id: item.Id, text: item.Name });
                    });

                    $('select[name="VehicleType"]').select2({
                        placeholder: "Select a vehicle type",
                        allowClear: false,
                        tags: true,
                        data: customVehiclesTypeSelect2
                    });

                    KTApp.unblockPage();
                });
            },
        },

        MODELS: {
            PostModel: {
                QuestionText: '',
                CategoryId: 0,
                GroupType: 0,
                VehicleId: [],
            }
        },

        BindEvents: function () {
            DEF.HELPERS.GetQuestions();
            DEF.HELPERS.LoadVehicleType();
            //DEF.ELEMENTS.VehicleType.select2({
            //    placeholder: "Sorunun görüneceği araç tiplerini seçiniz.",
            //});

            DEF.ELEMENTS.AddButton.on('click', function (e) {
                e.preventDefault();

                var model = [];

                DEF.MODELS.PostModel.QuestionText = DEF.ELEMENTS.Text.val();
                DEF.MODELS.PostModel.CategoryId = parseInt(DEF.ELEMENTS.QuestionCategory.val());
                DEF.MODELS.PostModel.GroupType = parseInt(DEF.ELEMENTS.GroupType.val());
                DEF.MODELS.PostModel.VehicleId = DEF.ELEMENTS.VehicleType.val();

                //model.push(DEF.MODELS.PostModel);

                //console.log(model);

                appBase.Helpers.Request("/api/admin/addQuestion", "post", DEF.MODELS.PostModel, function (response) {
                    if (response.Code == 200) {
                        swal.fire("Başarı ile eklenmiştir").then(
                            function () { window.location.reload(); })
                    }
                })
            });

            $(document).on('click', '.clickable', function (e) {
                
                var id = $(this).data('id');

                KTApp.blockPage({
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'success',
                    message: 'Lütfen bekleyiniz...'
                });

                DEF.ELEMENTS.QuestionId.val(id);

                appBase.Helpers.Request("/api/admin/GetQuestionById?id=" + id, "get", {}, function (response) {

                    DEF.ELEMENTS.EditModal.find('select[name="VehicleType"]').val(response.Data.Vehicle).trigger('change');
                    DEF.ELEMENTS.EditModal.find('select[name="Category"]').val(response.Data.Category);
                    DEF.ELEMENTS.EditModal.find('select[name="GroupType"]').val(response.Data.Group);

                    DEF.ELEMENTS.EditModal.find('textarea').val(response.Data.Question);
                    KTApp.unblockPage();

                }, function (error) {
                    KTApp.unblockPage();

                })
            });

            $(document).on('click', '#QuestionEdit', function (e) {


                var model = {
                    QuestionId: DEF.ELEMENTS.QuestionId.val(),
                    Question: DEF.ELEMENTS.EditModal.find('textarea').val(),
                    Category: parseInt(DEF.ELEMENTS.EditModal.find('select[name="Category"]').val()),
                    Group: parseInt(DEF.ELEMENTS.EditModal.find('select[name="GroupType"]').val()),
                    Vehicle: DEF.ELEMENTS.EditModal.find('select[name="VehicleType"]').val(),
                };


                appBase.Helpers.Request("/api/admin/editQuestion", "post", model, function (response) {
                    if (response.Code == 200) {
                        swal.fire("Başarı ile düzenlenmiştir.").then(
                            function () { window.location.reload(); })
                    }
                })

            });

            $(document).on('click', '#Remove', function (e) {

                //var id = DEF.ELEMENTS.QuestionId.val();

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Seçmiş olduğunuz soru anketlerden kaldırılacak?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/admin/RemoveQuestion?questionId=' + DEF.ELEMENTS.QuestionId.val(), 'get', {}, function (response) {
                            if (response.Code == 200) {
                                KTApp.unblockPage();

                                swal.fire("Başarı ile silinniştir.").then(
                                    function () { window.location.reload(); })
                            }
                            else {
                                Swal.fire(response.Message);
                            }
                        });
                    }
                });

            });
        }
    }


    $(function () {
        DEF.BindEvents();
    });

    window.DEF = DEF;

})();

