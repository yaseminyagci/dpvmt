﻿"use strict";
// Class definition

var KTDatatableVehicleUsers = function () {
    // Private functions

    // basic demo
    var demo = function ($element) {

        var status = {
            "Oturum": { 'title': 'Oturum', 'state': 'kt-badge--primary' },
            "Vardiya": { 'title': 'Vardiya', 'state': 'kt-badge--success' },
            "Arıza": { 'title': 'Arıza', 'state': 'kt-badge--warning' },
            "Yemek": { 'title': 'Yemek', 'state': 'kt-badge--dark' },
            "Mola": { 'title': 'Çalışma Bitirildi', 'state': 'kt-badge--info' }


        };

       
        if ($element === undefined)
            return;

        var vehicleId = $element.data('id');

           

        var table = function ($element) {

            return $element.KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: appBase.ApiUrl + "api/Admin/GetVehicleLogDataTable",
                            params: { Token: appBase.Helpers.GetCookie("Token"), vehicleId: vehicleId },
                            // sample custom headers
                            //headers: { 'x-my-custokt-header': 'some value', 'x-test-header': 'the value' },
                            //map: function (raw) {
                            //    // sample data mapping
                            //    var dataSet = raw;
                            //    if (typeof raw.data !== 'undefined') {
                            //        dataSet = raw.data;
                            //    }
                            //    return dataSet;
                            //},
                            method: 'get',
                        },
                    },
                    pageSize: 10,
                },

                // layout definition
                layout: {
                    scroll: true,
                    footer: false,
                },

                // column sorting
                sortable: true,

                pagination: true,
                order: [3, "desc"],

                //search: {
                //    input: $('#generalSearch'),
                //},

                // columns definition
                columns: [
                    {
                        field: 'UserName',
                        title: 'Operatör Adı',
                        width: 150
                    },
                    {
                        field: 'StartTime',
                        title: 'Başlangıç Zamanı',
                        format: 'DD/MM/YYYY HH:mm:ss',
                        template: function (row) {
                            return moment(row.StartTime).format("DD/MM/YYYY HH:mm:ss")
                        },
                        width: 150
                     
                    },
                    {
                        field: 'EndTime',
                        title: 'Bitiş Zamanı',
                        format: 'DD/MM/YYYY HH:mm:ss',
                        template: function (row) {
                            return moment(row.EndTime).format("DD/MM/YYYY HH:mm:ss")
                        },
                        width: 150
                    },
                    {
                        field: 'PassingTime',
                        title: 'Geçen Süre',
                        template: function (row) {
                            return moment(row.PassingTime, 'HH:mm:ss').format("HH:mm:ss")
                        },
                        autoHide: false,
                        width: 150
                    },
                    {
                        field: 'Event',
                        title: 'İşlem',
                        template: function (row) {
                            return '<span class="kt-badge ' + status[row.Event].state + ' kt-badge--inline kt-badge--pill">' + status[row.Event].title + '</span>';

                        },


                    },
                ],
            });
        }

        switch (vehicleId) {
            case 1:
                window.RTGTable = table($element);
                window.RTGTable.sort("EndTime", 'desc');
                break;
            case 2:
                window.ITVTable = table($element);
                window.ITVTable.sort("EndTime", 'desc');
                break;
            case 3:
                window.RSTable = table($element);
                window.RSTable.sort("EndTime", 'desc');
                break;
            default:
        }         

    };

    return {
        // public functions
        init: function (element) {
            demo(element);
        },
    };
}();

jQuery(document).ready(function () {
    //KTDatatableRemoteAjaxDemo.init();
});