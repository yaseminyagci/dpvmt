﻿$(function () {
    $('#getexcel').on('click', function () {
        var date = $('#kt_datepicker_1').val().replace(/\-/g, '');
        var vehicle = $('#vehicle').val();

        var pathName = date + '_' + vehicle + '.xls';

        $('#frm').attr('src', generalWEBUrl + 'reports/User%20Activity%20Report/Excel/' + pathName);
    });

    $('#vehicle').on('change', function () {
        var $this = $(this);

        var itemType = $('#vehicleItem');

        switch ($this.val()) {
            case 'RTG':
                var rtg = '<option>RTG01</option>' +
                    '<option>RTG02</option>' +
                    '<option>RTG03</option>' +
                    '<option>RTG04</option>' +
                    '<option>RTG05</option>' +
                    '<option>RTG06</option>' +
                    '<option>RTG07</option>' +
                    '<option>RTG08</option>' +
                    '<option>RTG09</option>' +
                    '<option>RTG10</option>' +
                    '<option>RTG11</option>' +
                    '<option>RTG12</option>' +
                    '<option>RTG13</option>' +
                    '<option>RTG14</option>' +
                    '<option>RTG15</option>' +
                    '<option>RTG16</option>' +
                    '<option>RTG17</option>' +
                    '<option>RTG18</option>';
                itemType.html('').html(rtg);
                break;
            case 'ITV':
                var itv =
                    '<option>ITV1</option>' +
                    '<option>ITV 2</option>' +
                    '<option>ITV 3</option>' +
                    '<option>ITV 4</option>' +
                    '<option>ITV 5</option>' +
                    '<option>ITV 6</option>' +
                    '<option>ITV 7</option>' +
                    '<option>ITV 8</option>' +
                    '<option>ITV 9</option>' +
                    '<option>ITV 10</option>' +
                    '<option>ITV 11</option>' +
                    '<option>ITV 12</option>' +
                    '<option>ITV 13</option>' +
                    '<option>ITV 14</option>' +
                    '<option>ITV 15</option>' +
                    '<option>ITV 16</option>' +
                    '<option>ITV 17</option>' +
                    '<option>ITV 18</option>' +
                    '<option>ITV 19</option>' +
                    '<option>ITV 20</option>' +
                    '<option>ITV 21</option>' +
                    '<option>ITV 22</option>' +
                    '<option>ITV 23</option>' +
                    '<option>ITV 24</option>' +
                    '<option>ITV 25</option>' +
                    '<option>ITV 26</option>' +
                    '<option>ITV 27</option>' +
                    '<option>ITV 28</option>' +
                    '<option>ITV 29</option>' +
                    '<option>ITV 30</option>' +
                    '<option>ITV 31</option>' +
                    '<option>ITV 32</option>' +
                    '<option>ITV 33</option>' +
                    '<option>ITV 34</option>' +
                    '<option>ITV 35</option>' +
                    '<option>ITV 36</option>' +
                    '<option>ITV 37</option>' +
                    '<option>ITV 38</option>' +
                    '<option>ITV 39</option>' +
                    '<option>ITV 40</option>' +
                    '<option>ITV 41</option>' +
                    '<option>ITV 42</option>' +
                    '<option>ITV 43</option>' +
                    '<option>ITV 44</option>';
                itemType.html('').html(itv);
                break;
            case 'RS':
                var rs = '<option>ITV 43</option>' +
                    '<option>ITV 44</option>';
                itemType.html('').html(rs);
                break;
            case 'EH':
                var eh = '<option>EH01</option>' +
                    '<option>EH02</option>';
                itemType.html('').html(eh);
                break;
            default:
        }
    });
});