﻿"use strict";

var ChartBegin = function () {
    var profitShare = function (id, data) {

        var chartID = "chart";
        chartID += id;

        if (!KTUtil.getByID(chartID)) {
            return;
        }

        var randomScalingFactor = function () {
            return Math.round(Math.random() * 100);
        };

        var charts = {
            "RTGChart": window.RTGChart,
            "ITVhart": window.ITVChart,
            "RSChart": window.RSChart

        }

        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: data,
                    backgroundColor: [
                        KTApp.getStateColor('success'),
                        KTApp.getStateColor('brand'),
                        KTApp.getStateColor('warning'),
                        KTApp.getStateColor('danger')

                    ]
                }],
                labels: [
                    'Mola',
                    'Yemek',
                    'Arıza',
                    'Çalışma'
                ]
            },
            options: {
                cutoutPercentage: 75,
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Technology'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 10,
                    xPadding: 10,
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: KTApp.getStateColor('brand'),
                    titleFontColor: '#ffffff',
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0,
                }
            }
        };

        var ctx = KTUtil.getByID(chartID).getContext('2d');
        charts[id] = new Chart(ctx, config);


    };

    return {
        init: function (id, data) {
            profitShare(id, data);
        }
    }
}();

