﻿; (() => {
    var DEF = {
        BIND_EVENTS: function () {
            $('form input').on('change', function (e) {
                var $this = $(this);
                var buttonType = $this.data("buttontype");

                var model = {
                    BreakButton: DEF.ELEMENTS.BreakButton.is(':checked'),
                    MealButton: DEF.ELEMENTS.MealButton.is(':checked'),
                    FaultButton: DEF.ELEMENTS.FaultButton.is(':checked'),
                    RelayEndButton: DEF.ELEMENTS.RelayEndButton.is(':checked')
                };

                //switch (buttonType) {
                //    case DEF.MODELS.ButtonModel.BreakButton:
                //        model.BreakButton = true;
                //        break;
                //    case DEF.MODELS.ButtonModel.MealButton:
                //        model.MealButton = true;
                //        break;
                //    case DEF.MODELS.ButtonModel.FaultButton:
                //        model.FaultButton = true;
                //        break;
                //    case DEF.MODELS.ButtonModel.RelayEndButton:
                //        model.RelayEndButton = true;
                //        break;
                //    default:
                //        break;
                //}

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Buton durumu değiştirilicektir.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/admin/ChangeButtons', 'post', model, function (response) {
                            if (response.Code == 200) {
                                KTApp.unblockPage();
                                DEF.HELPERS.SetButtonStatus();
                            }
                        });
                    }
                    else {
                        if ($this.is(':checked')) {
                            $this.removeAttr('checked');
                        }
                        else {
                            $this.attr('checked', 'checked');
                        }
                    }
                });
            });
        },

        ELEMENTS: {
            BreakButton: $('input[data-buttontype="BreakButton"]'),
            MealButton: $('input[data-buttontype="MealButton"]'),
            FaultButton: $('input[data-buttontype="FaultButton"]'),
            RelayEndButton: $('input[data-buttontype="RelayEndButton"]')
        },

        HELPERS: {
            SetButtonStatus: function () {
                appBase.Helpers.Request('api/admin/CheckButtons', 'get', {}, function (response) {
                    if (response.Code == 200) {

                        var responseData = response.Data;


                        responseData.BreakButton ? DEF.ELEMENTS.BreakButton.attr('checked', 'checked') : DEF.ELEMENTS.BreakButton.removeAttr('checked');
                        responseData.MealButton ? DEF.ELEMENTS.MealButton.attr('checked', 'checked') : DEF.ELEMENTS.MealButton.removeAttr('checked');
                        responseData.FaultButton ? DEF.ELEMENTS.FaultButton.attr('checked', 'checked') : DEF.ELEMENTS.FaultButton.removeAttr('checked');
                        responseData.RelayEndButton ? DEF.ELEMENTS.RelayEndButton.attr('checked', 'checked') : DEF.ELEMENTS.RelayEndButton.removeAttr('checked');
                    }
                });
            }
        },

        MODELS: {
            ButtonModel: {
                BreakButton: 'BreakButton',
                MealButton: 'MealButton',
                FaultButton: 'FaultButton',
                RelayEndButton: 'RelayEndButton'
            }
        }
    }

    $(function () {

        DEF.HELPERS.SetButtonStatus();
        DEF.BIND_EVENTS();


    });

})();
