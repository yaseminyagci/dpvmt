﻿var flag = true;

; (function () {

    var DEF = {
        Helpers: {
            GetCookie: function (cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            },
            SetCookie: function (cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            },
            Request: function (url, requestType, data, success, fail, options, hasExternalURL) {

                var genericAPIURL = generalAPIUrl;

                if (!hasExternalURL) {
                    url = genericAPIURL + url;
                }

                data.Token = DEF.Helpers.GetCookie("Token");

                if (data.Token == "" || data.Token == "null") {
                    window.location = "/admin/auth/login";
                }
                else {
                    $.ajax({
                        url: url,
                        method: requestType,
                        data: data,
                        success: function (response) {
                            success(response);
                        },
                        error: function (response) {

                            if (response.responseJSON.Code == 302) {//Token Expired
                                appBase.Helpers.SetCookie("Token", null);
                                window.location = "/admin/auth/login";
                            }

                            if (fail)
                                fail(response);
                        }
                    })
                }



            },
            GetUserName: function () {
                appBase.Helpers.Request("api/User/GetUserName", "get", {}, function (response) {
                    if (response.Code == 200) {
                        $('#userName').text(response.Data);

                    }
                })
            },
            GetUserRole: function () {
                appBase.Helpers.Request("api/User/GetUserRole", "get", {}, function (response) {
                    if (response.Code == 200 && flag == true && response.Data == "2") {
                        flag = false;

                        //$('#userRole').val(response.Data);

                        var userEl = '<li class="kt-menu__item " aria-haspopup="true"><a href="/admin/User/Index?userRole=da4b9237bacccdf19c0760cab7aec4a8359010b0" class="kt-menu__link "><span class="kt-menu__link-text">Kullanıcı Ekle</span></a></li>';

                        if (response.Data == "2") {
                            $("#menuNav").append(userEl);
                        }

                    }
                })
            },

            ToastrMsg: function (type, msg, title) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr[type](msg, title)
            }
        },

        MODELS: {},

        EVENTS: {
            LOGIN: 1,
            LOGOUT: 2,
            RELAY_START: 3,
            RELAY_ENDED: 4,
            BREAK_STARTED: 5,
            BREAK_ENDED: 6,
            FAULT_STARTED: 7,
            FAULT_ENDED: 8,
            MEAL_STARTED: 9,
            MEAL_END: 10,
        },
        ApiUrl: generalAPIUrl,

    }

    window.bPage = KTApp.blockPage();
    window.unblockPage = KTApp.unblockPage();
    window.appBase = DEF;
    $(function () {
        DEF.Helpers.GetUserName();
        DEF.Helpers.GetUserRole();

        $('#logout').on('click', function (e) {

            appBase.Helpers.Request(
                'api/Admin/AdminLogout',
                'GET', {},
                function (response) {
                    if (response.Code == 200) {
                        window.location = '/admin/auth/login';

                        appBase.Helpers.SetCookie("Token", "");
                    }
                    else {
                        swal("Bir sorun oluştu");
                    }

                },
                function (response) {
                    console.log(response);
                }
            );
        });
    })
})();

