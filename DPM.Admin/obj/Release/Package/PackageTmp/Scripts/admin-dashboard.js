﻿; (function () {

    var DEF = {
        ELEMENTS: {
            StateField: $('#StateField'),
            RTGUsers: $('#rtgTable'),
            ITVUsers: $('#itvTable'),
            RSUsers: $('#rsTable'),
            FaultStarButton: $('#FaultStart'),
            FaultEndButton: $('#FaultEnd'),
            LogoutButton: $('#logout'),
            UserName: $('#userName'),
        },
        CONSTANT: {
            StateTable: window.AdminVehicleStateTable,
            RTGVehicleType: 1,
            ITVVehicleType: 2,
            RSVehicleType: 3,
        },

        HELPERS: {
            GetStates: function () {

                KTApp.blockPage()
                appBase.Helpers.Request(
                    'api/Admin/GetVehicleStates',
                    'GET', {},
                    function (response) {
                        console.log(response);

                        var template = $('#status-template').html();
                        var rendered = Mustache.render(template, response.Data);

                        DEF.ELEMENTS.StateField.html(rendered);

                        KTApp.unblockPage();

                    },
                    function (response) {
                    }
                );

            },
            GetVehicleState: function () {
                KTDatatableRemoteAjaxDemo.init();
            },
            GetRTGUsers: function () {
                KTDatatableVehicleUsers.init(DEF.ELEMENTS.RTGUsers);
            },
            GetITVUsers: function () {
                KTDatatableVehicleUsers.init(DEF.ELEMENTS.ITVUsers);
            },
            GetRSUsers: function () {
                KTDatatableVehicleUsers.init(DEF.ELEMENTS.RSUsers);
            },
            GetChart: function (vehicleType) {
                appBase.Helpers.Request("api/Admin/GetChart", "get", { vehicleTypeId: vehicleType }, function (response) {
                    if (response.Code == 200) {
                        ChartBegin.init(vehicleType, response.Data.Rates);
                        $('#chart' + vehicleType).siblings('.kt-widget14__stat').text(moment(response.Data.TotalSession, 'HH:mm:ss').format("HH:mm:ss"));

                        $('#chart' + vehicleType).parent().siblings('.kt-widget14__legends').find('span[name="work"]').text(response.Data.Rates[3].toFixed(2));
                        $('#chart' + vehicleType).parent().siblings('.kt-widget14__legends').find('span[name="break"]').text(response.Data.Rates[0].toFixed(2));
                        $('#chart' + vehicleType).parent().siblings('.kt-widget14__legends').find('span[name="meal"]').text(response.Data.Rates[1].toFixed(2));
                        $('#chart' + vehicleType).parent().siblings('.kt-widget14__legends').find('span[name="fault"]').text(response.Data.Rates[2].toFixed(2));

                    }
                })
            },
            SetFault: function (vehicleId) {

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request("api/admin/setfault", "get", { vehicleId: vehicleId }, function (response) {

                            KTApp.unblockPage();
                            if (response.Code == 200 && response.Data) {

                                if (window.AdminVehicleStateTable)
                                    window.AdminVehicleStateTable.reload();
                                else
                                    DEF.HELPERS.GetVehicleState();

                                toastr.warning("Araç üzerindeki işlemler durduruldu", "Arıza Başlatıldı");
                            }
                            else {
                                swal("Bir Hata oluştu"); 
                            }
                        });
                    }
                });


            },
            RemoveFault: function (vehicleId) {

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request("api/admin/removefault", "get", { vehicleId: vehicleId }, function (response) {
                            KTApp.unblockPage();
                            if (response.Code == 200 && response.Data) {

                                if (window.AdminVehicleStateTable)
                                    window.AdminVehicleStateTable.reload();
                                else
                                    DEF.HELPERS.GetVehicleState();

                                toastr.danger("Araç tekrar aktif hale getirildi.", "Arıza Bitirildi");

                            }
                            else {
                                swal("Bir Hata oluştu");

                            }
                        });
                    }
                });


            },


        },

        MODELS: {
        },

        BindEvents: function () {
            $(document).on('click', '#FaultEnd', function () {
                console.log($(this).data('id'));

                DEF.HELPERS.RemoveFault($(this).data('id'));

            });


            $(document).on('click', '#FaultStart', function () {
                console.log($(this).data('id'));

                DEF.HELPERS.SetFault($(this).data('id'));

            });

            DEF.ELEMENTS.LogoutButton.on('click', function (e) {

                appBase.Helpers.Request(
                    'api/Admin/AdminLogout',
                    'GET', {},
                    function (response) {
                        if (response.Code == 200) {
                            window.location = '/auth/login';

                            appBase.Helpers.SetCookie("Token", "");
                        }
                        else {
                            swal("Bir sorun oluştu");
                        }

                    },
                    function (response) {
                        console.log(response);
                    }
                );
            });


        }
    }


    $(function () {
        //DEF.HELPERS.GetStates();
        DEF.BindEvents();
        DEF.HELPERS.GetVehicleState();

        setInterval(function () {
            if (window.AdminVehicleStateTable)
                window.AdminVehicleStateTable.reload();
            else
                DEF.HELPERS.GetVehicleState();

        }, 1000 * 10);

        DEF.HELPERS.GetRTGUsers();
        DEF.HELPERS.GetITVUsers();
        DEF.HELPERS.GetRSUsers();

        DEF.HELPERS.GetChart(DEF.CONSTANT.RTGVehicleType);
        DEF.HELPERS.GetChart(DEF.CONSTANT.ITVVehicleType);
        DEF.HELPERS.GetChart(DEF.CONSTANT.RSVehicleType);

        //setTimeout(function () {
        //    window.RTGTable.sort("EndTime", 'desc');
        //    window.ITVTable.sort("EndTime", 'desc');
        //    window.RSTable.sort("EndTime", 'desc');

        //}, 3000)



    });
    window.AdminDashboard = DEF;

})();

