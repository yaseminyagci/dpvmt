﻿$(function () {
    $('#getexcel').on('click', function () {
        var date = $('#kt_datepicker_1').val().replace(/\-/g, '');
        var vehicle = $('#vehicle').val();

        var pathName = date + '_' + vehicle + '.xls';

        $('#frm').attr('src', generalWEBUrl+'reports/Survey%20Daily%20Report/Excel/' + pathName);
    });
});