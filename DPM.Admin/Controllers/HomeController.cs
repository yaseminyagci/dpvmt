﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DPM.Admin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString(); 
            return View();
        }

        public ActionResult Settings()
        {
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
            return View();
        }

        public ActionResult SurveyOptions()
        {
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
            return View();
        }

        public ActionResult Questions()
        {
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
            return View();
        }

        public ActionResult Vehicles()
        {
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
            return View();
        }

        public ActionResult DailyReport()
        {
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
            ViewBag.WebUrl = ConfigurationManager.AppSettings["weburl"].ToString();
            return View();
        }

        public ActionResult DailySurveyReport()
        {
            ViewBag.WebUrl = ConfigurationManager.AppSettings["weburl"].ToString();
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
            return View();
            
        }

    }
}