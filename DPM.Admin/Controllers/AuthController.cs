﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DPM.Admin.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Login()
        {
                ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
                return View();           
        }
    }
}