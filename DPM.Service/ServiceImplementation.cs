﻿using DPM.Service.Framework;
using System.ServiceProcess;

 
using Quartz.Impl;
using Quartz; 


namespace DPM.Service
{
    /// <summary>
    /// The actual implementation of the windows service goes here...
    /// </summary>
    [WindowsService("DPM.Service",
        DisplayName = "DPM.Service",
        Description = "The description of the DPM.Service service.",
        EventLogSource = "DPM.Service",
        StartMode = ServiceStartMode.Automatic)]
    public class ServiceImplementation : IWindowsService
    {
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
        }

        /// <summary>
        /// This method is called when the service gets a request to start.
        /// </summary>
        /// <param name="args">Any command line arguments</param>
        public async void OnStart(string[] args)
        {
            string cronExpressionForDaily = "0 0/1 * 1/1 * ? *";   // her gece 1 
             
            StdSchedulerFactory priceFactory = new StdSchedulerFactory();
            IScheduler schedPrice = await priceFactory.GetScheduler();
            await schedPrice.Start();
            IJobDetail job = JobBuilder.Create<RunService>().WithIdentity("myJob1", "group1").Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("myTrigger1", "group1")
                //.WithCronSchedule(cronExpressionForDaily)
                .StartNow()
                .Build();
            await schedPrice.ScheduleJob(job, trigger);

        }

        /// <summary>
        /// This method is called when the service gets a request to stop.
        /// </summary>
        public void OnStop()
        {
        }

        /// <summary>
        /// This method is called when a service gets a request to pause,
        /// but not stop completely.
        /// </summary>
        public void OnPause()
        {
        }

        /// <summary>
        /// This method is called when a service gets a request to resume 
        /// after a pause is issued.
        /// </summary>
        public void OnContinue()
        {
        }

        /// <summary>
        /// This method is called when the machine the service is running on
        /// is being shutdown.
        /// </summary>
        public void OnShutdown()
        {
        }

        /// <summary>
        /// This method is called when a custom command is issued to the service.
        /// </summary>
        /// <param name="command">The command identifier to execute.</param >
        public void OnCustomCommand(int command)
        {
        }
    }
}
