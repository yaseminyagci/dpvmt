﻿using DPM.Model;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;


namespace DPM.Service
{
    public class RunService : IJob
    {
        string path = @"C:\inetpub\wwwroot\web\Reports\";

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                Run();
            }
            catch (Exception ex)
            {

            }
            await Task.Delay(1);
        }
        public void Run()
        {
            DubaiPortDB db = new DubaiPortDB();

            // var oneDayAgo = DateTime.UtcNow.AddHours(-4).AddMinutes(-5);
            var oneDayAgo = DateTime.UtcNow;
            var allVehicleTypes = db.VehicleTypes.Where(w => w.Status == 1).ToList();

            foreach (var item in allVehicleTypes)
            {
                var getLog = GetLogByVehicle(item.VehicleTypeId);

                SaveAs(getLog, path + "User Activity Report\\", oneDayAgo.ToString("ddMMyyyy") + "_" + item.Code + ".pdf");
                SaveAsExcel(path + "User Activity Report\\" + oneDayAgo.ToString("ddMMyyyy") + "_" + item.Code + ".pdf", path + "User Activity Report\\Excel\\" + oneDayAgo.ToString("ddMMyyyy") + "_" + item.Code + ".xls");


                //MemoryStream generatedForUserActivity = GeneratePdf(getLog);

                //using (FileStream file = new FileStream(path + "User Activity Report\\" + oneDayAgo.ToString("ddMMyyyy") + "_" + item.Code + ".pdf", FileMode.Create, System.IO.FileAccess.Write))
                //    generatedForUserActivity.CopyTo(file);


                var getHtml = GetSurveyLogByUser(item.VehicleTypeId);

                SaveAs(getHtml, path + "Survey Daily Report\\", oneDayAgo.ToString("ddMMyyyy") + "_" + item.Code + ".pdf");
                SaveAsExcel(path + "Survey Daily Report\\" + oneDayAgo.ToString("ddMMyyyy") + "_" + item.Code + ".pdf", path + "Survey Daily Report\\Excel\\" + oneDayAgo.ToString("ddMMyyyy") + "_" + item.Code + ".xls");

                //MemoryStream generatedForSurveyDailyReport = GeneratePdf(getHtml);

                //using (FileStream file = new FileStream(path + "Survey Daily Report\\" + oneDayAgo.ToString("ddMMyyyy") + "_" + item.Code + ".pdf", FileMode.Create, System.IO.FileAccess.Write))
                //    generatedForSurveyDailyReport.CopyTo(file);

            }
        }

        public void SaveAs(string html, string path, string fileName)
        {
            //IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf(new IronPdf.PdfPrintOptions() { InputEncoding = Encoding.UTF8 });
            ////Render an HTML document or snippet as a string
            //Renderer.RenderHtmlAsPdf(html).SaveAs(path + fileName);

            // Advanced: 
            // Set a "base url" or file path so that images, javascript and CSS can be loaded  

            //var PDF = Renderer.RenderHtmlAsPdf(html, path);
            //PDF.SaveAs(fileName);

           
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBytes = htmlToPdf.GeneratePdf(html);

            htmlToPdf.GeneratePdf(html, null, path + fileName);

        }

        public MemoryStream GeneratePdf(string html)
        {
            var pdfDoc = new Document(PageSize.A4);
            var memoryStream = new MemoryStream();
            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, memoryStream);

            pdfWriter.RgbTransparencyBlending = true;
            pdfDoc.Open();
            var cssResolver = new StyleAttrCSSResolver();

            XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
            //Following line will be required in case of lanuguages other than english i.e I have added following for arabic font.
            //fontProvider.Register(HttpContext.Server.MapPath("~/Tahoma Regular font.ttf"));
            //CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
            CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
            HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
            htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
            // Pipelines
            PdfWriterPipeline pdf = new PdfWriterPipeline(pdfDoc, pdfWriter);
            HtmlPipeline html1 = new HtmlPipeline(htmlContext, pdf);
            CssResolverPipeline css = new CssResolverPipeline(cssResolver, html1);
            // XML Worker
            XMLWorker worker = new XMLWorker(css, true);
            XMLParser p = new XMLParser(worker);
            p.Parse(new StringReader(html.Replace("ğ", "g").Replace("ı", "i").Replace("ş", "s")));
            pdfWriter.CloseStream = false;
            pdfDoc.Close();
            memoryStream.Position = 0;

            return memoryStream;
        }

        public string GetLogByVehicle(int vehicleTypeId)
        {
            DubaiPortDB db = new DubaiPortDB();

            List<ReturnModel> rtgModel = new List<ReturnModel>();

            var currentVehicleType = db.VehicleTypes.Where(w => w.VehicleTypeId == vehicleTypeId).FirstOrDefault();


            List<int> allRTGVehicles = db.Vehicles.Where(w => w.VehicleTypeId == vehicleTypeId).Select(s => s.VehicleId).ToList();

            var oneDayAgo = DateTime.UtcNow.AddHours(-4).AddMinutes(-5);
            DateTime customDate = oneDayAgo.Date;


            var allLogs = db.EventLogs
                .Where(w => allRTGVehicles.Contains(w.VehicleId.Value) && DbFunctions.TruncateTime(w.LogDate) == customDate)
                .Include(i => i.User)
                .Include(i => i.EventLogTimes)
                .ToList();

            string html = File.ReadAllText(@"C:\Repository\dubai-port-management-system\DubaiPortManagementSystem\DPM.Service\template.html");

            string TableItem = string.Empty;

            foreach (var item in allLogs.GroupBy(x => x.VehicleId).ToList())
            {
                int vehicleId = item.Select(s => s.VehicleId).FirstOrDefault().Value;
                var vehicle = db.Vehicles.Where(w => w.VehicleId == vehicleId).FirstOrDefault();

                TableItem += "<br/>" +
                                   vehicle.Name +
                                   "<table class=\"custom-table\" style=\"border-style: solid;\">" +
                                     "<tr>" +
                                       "<th>Operator</th>" +
                                      "<th>Çalışma Süresi</th>" +
                                       "<th>Mola Süresi</th>" +
                                       "<th>Yemek Süresi</th>" +
                                       "<th>Arıza Süresi</th>" +
                                     "</tr>" +
                                      "{{ROWITEM}}" +
                                   "</table>";

                string rows = string.Empty;

                foreach (var currentVehicleLog in item.GroupBy(x => x.UserId).ToList())
                {


                    ReturnModel model = new ReturnModel();

                    var eventTimes = currentVehicleLog.SelectMany(s => s.EventLogTimes).ToList();

                    var totalTime = eventTimes.Where(w => w.EventTypeId == 7).Sum(s => s.LogTimeSpan);
                    var breakTime = eventTimes.Where(w => w.EventTypeId == 11).Sum(s => s.LogTimeSpan);
                    var faultTime = eventTimes.Where(w => w.EventTypeId == 15).Sum(s => s.LogTimeSpan);
                    var mealTime = eventTimes.Where(w => w.EventTypeId == 19).Sum(s => s.LogTimeSpan);

                    model.BreakTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromTicks(breakTime));
                    model.TotalTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromTicks(totalTime));
                    model.MealTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromTicks(mealTime));
                    model.FaultTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromTicks(faultTime));

                    User user = currentVehicleLog.Select(s => s.User).FirstOrDefault();



                    model.UserId = user.UserId;
                    model.UserName = $"{user.FirstName} {user.LastName}";

                    model.VehicleId = vehicle.VehicleId;
                    model.VehicleName = vehicle.Name;

                    rtgModel.Add(model);


                    rows += "<tr>" +
                                    "<td>" + model.UserName + "</td>" +
                                    "<td>" + model.TotalTime + "</td>" +
                                    "<td>" + model.BreakTime + "</td>" +
                                    "<td>" + model.MealTime + "</td>" +
                                    "<td>" + model.FaultTime + "</td>" +
                                "</tr>";
                }

                var totalDailyEvents = allLogs.Where(w => w.VehicleId == item.Key.Value).SelectMany(s => s.EventLogTimes).ToList();

                var totalRowModel = new ReturnModel()
                {
                    TotalTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromTicks(totalDailyEvents.Where(w => w.EventTypeId == 7).Sum(s => s.LogTimeSpan))),
                    BreakTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromTicks(totalDailyEvents.Where(w => w.EventTypeId == 11).Sum(s => s.LogTimeSpan))),
                    FaultTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromTicks(totalDailyEvents.Where(w => w.EventTypeId == 15).Sum(s => s.LogTimeSpan))),
                    MealTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromTicks(totalDailyEvents.Where(w => w.EventTypeId == 19).Sum(s => s.LogTimeSpan))),

                    UserName = "Toplam",
                    UserId = 0,
                    VehicleName = rtgModel[0].VehicleName,

                };

                rtgModel.Add(totalRowModel);

                rows += "<tr>" +
                                    "<td>Toplam</td>" +
                                    "<td>" + totalRowModel.TotalTime + "</td>" +
                                    "<td>" + totalRowModel.BreakTime + "</td>" +
                                    "<td>" + totalRowModel.MealTime + "</td>" +
                                    "<td>" + totalRowModel.FaultTime + "</td>" +
                                "</tr>";

                TableItem = TableItem.Replace("{{ROWITEM}}", rows);

            }

            html = html.Replace("{{AllTABLE}}", TableItem).Replace("{{VehicleType}}", currentVehicleType.Code).Replace("{{Date}}", oneDayAgo.ToString("dd-MM-yyyy"));

            return html;
        }

        public string GetSurveyLogByUser(int vehicleTypeId)
        {
            DubaiPortDB db = new DubaiPortDB();

            var oneDayAgo = DateTime.UtcNow.AddHours(-4).AddMinutes(-5);
            //DateTime customDate = oneDayAgo.Date;
            DateTime customDate = new DateTime(2019, 10, 10);

            List<int> userVehicleRelationshipIDByVehichle = db.UserVehicleRelationships.Include(i => i.Vehicle).Where(w => w.Vehicle.VehicleTypeId == vehicleTypeId).Select(s => s.UserVehicleRelationId).ToList();

            List<Survey> surveyByUserVehicleRelationship = db.Surveys.Include(i => i.UserVehicleRelationship).Include(i => i.UserVehicleRelationship.Vehicle).Include(i => i.UserVehicleRelationship.User).Where(w => userVehicleRelationshipIDByVehichle.Contains(w.UserDeviceRelationshipId) && DbFunctions.TruncateTime(w.CreatedDate) == customDate).OrderBy(o => o.UserVehicleRelationship.Vehicle.Name).ToList();

            string html = File.ReadAllText(@"C:\Repository\dubai-port-management-system\DubaiPortManagementSystem\DPM.Service\templateforsurvey.html");
            string tableList = string.Empty;

            foreach (var item in surveyByUserVehicleRelationship)
            {
                string table = "<table class=\"tg\">" +
                                  "<tr>" +
                                      "<td class=\"tg-yla0\">Araç Adı :</td>" +
                                      "<td class=\"tg-cly1\">" + item.UserVehicleRelationship.Vehicle.Name + "</td>" +
                                      "<td class=\"tg-yla0\">Operatör :</td>" +
                                      "<td class=\"tg-cly1\">" + item.UserVehicleRelationship.User.FirstName + " " + item.UserVehicleRelationship.User.LastName + "</td>" +
                                      "<td class=\"tg-yla0\">Tarih :</td>" +
                                      "<td class=\"tg-0lax\">" + customDate.ToString("dd/MM/yyyy") + "</td>" +
                                  "</tr>" +
                                  "<tr>" +
                                      "<td class=\"tg-cly1\" colspan=\"6\"></td>" +
                                  "</tr>" +
                                  "<tr>" +
                                      "<td class=\"tg-yla0\">Kategori</td>" +
                                      "<td class=\"tg-wa1i\" colspan=\"3\">Soru</td>" +
                                      "<td class=\"tg-wa1i\" colspan=\"2\">Cevap</td>" +
                                  "</tr>" +
                                  "{{TableRowList}}" +
                            "</table>" +
                            Environment.NewLine;


                List<SurveyAnswer> currentAnswers = db.SurveyAnswers.Include(i => i.Question).Include(i => i.Question.QuestionVehicleRelationships).Where(w => w.SurveyId == item.SurveyId).ToList();

                string rowList = string.Empty;
                foreach (var answerItem in currentAnswers)
                {
                    string ans = answerItem.Answer ? "OK" : "ARIZA/HATA";

                    string catName = string.Empty;

                    if (item.UserVehicleRelationship.Vehicle.VehicleTypeId == 1)
                    {
                        if (answerItem.Question.GroupType == 1)
                        {
                            catName = "Vardiya öncesi";
                        }
                        else
                        {
                            catName = "Vardiya sonrası";
                        }
                    }
                    else
                    {
                        catName = answerItem.Question.QuestionVehicleRelationships.FirstOrDefault().Category.Code;
                    }

                    string rowItem = "<tr>" +
                                          "<td class=\"tg-baqh\">" + catName + "</td>" +
                                          "<td class=\"tg-baqh\" colspan=\"3\">" + answerItem.Question.Text + "</td>" +
                                          "<td class=\"tg-baqh\" colspan=\"2\">" + ans + "</td>" +
                                      "</tr>";

                    rowList += rowItem;
                }

                table = table.Replace("{{TableRowList}}", rowList);
                tableList += table;
            }

            html = html.Replace("{{TableList}}", tableList);

            return html;
        }

        public void SaveAsExcel(string pathPdf,string pathExcel) {

            SautinSoft.PdfFocus pdfToExcel = new SautinSoft.PdfFocus();
            pdfToExcel.OpenPdf(pathPdf);
            pdfToExcel.ToExcel(pathExcel);
            pdfToExcel.ClosePdf();

          

        }

    }

    public class ReturnModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string BreakTime { get; set; }
        public string MealTime { get; set; }
        public string FaultTime { get; set; }
        public string TotalTime { get; set; }
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }
    }
}