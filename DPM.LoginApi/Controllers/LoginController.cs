﻿using DPM.Model;
using System.Linq;
using System.Web.Http;

namespace DPM.LoginApi.Controllers
{
    public class LoginController : ApiController
    {
        [HttpGet]
        public bool Login(string email, string password)
        {
            bool result;

            using (DubaiPortDB db = new DubaiPortDB())
            {
                result = db.Users.Where(w => w.Email == email && w.Password == password).Any();
            }

            return result;
        }
    }
}
