﻿using DPM.Api.ZodiacTestServiceReference;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Web;

namespace DPM.Api.Helpers
{
    public partial class ZodiacService
    {
        internal static readonly string USERNAME = "Username";
        internal static readonly string PASSWORD = "Password";
        internal static readonly string IMPERSONATE = "ZodiacImpersonate";
        internal static readonly string CLIENTID = "ClientId";
        private string username;
        private string password;
        public void setHttpCredentialInfo(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
        //protected override WebRequest GetWebRequest(Uri uri)
        //{
        //    setHttpCredentialInfo();
        //    HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(uri);
        //    request.Headers.Add(USERNAME, username);
        //    request.Headers.Add(IMPERSONATE, username);
        //    request.Headers.Add(PASSWORD, password);
        //    request.Headers.Add(CLIENTID,
        //    Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(
        //    o => o.AddressFamily == AddressFamily.InterNetwork
        //    ).First().ToString());
        //    return request;
        //}

        public void Login()
        {
            //ZodiacTestServiceReference.ZodiacServiceSoapClient zodiacServiceSoapClient = new ZodiacTestServiceReference.ZodiacServiceSoapClient();

            ChannelFactory<ZodiacServiceSoap> channelFactory = new ChannelFactory<ZodiacServiceSoap>("ZodiacServiceSoap12", new EndpointAddress("http://uat-app/zodiacwebservice/zodiacservice.asmx"));
            channelFactory.Credentials.SupportInteractive = false;

            var zodiacServiceSoapClient = channelFactory.CreateChannelWithIssuedToken(SecurityToken.CreateToken());

            var result=zodiacServiceSoapClient.serviceRequestWithMessageType("", "", "");
            
        }
    }
}