﻿using DPM.Model;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using static DPM.Api.Constants.Enums;

namespace DPM.Api.Helpers
{
    public class EmailSender
    {

        public void SendMail(List<string> to, List<string> cc, string subject, List<string> body)
        {
            string bodyMessage = string.Empty;

            foreach (var item in body)
            {
                bodyMessage += $"{item}{Environment.NewLine}";
            }

            SendMail(to, cc, subject, bodyMessage);
        }

        private void SendMail(List<string> to, List<string> cc, string subject, string body)
        {

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("ekipmankontrol@dpworld.com", "Info");

            for (int i = 0; i < to.Count; i++)
                mailMessage.To.Add(to[i]);

            for (int i = 0; i < cc.Count; i++)
                mailMessage.CC.Add(cc[i]);

            mailMessage.Subject = subject;

            mailMessage.Body = body;
            mailMessage.IsBodyHtml = false;

            SmtpClient smtpClient = new SmtpClient();

            smtpClient.Host = "10.202.134.72";
            //smtpClient.Host = "10.202.134.72";
            //smtpClient.Port = 587;
            try
            {
                smtpClient.Send(mailMessage);

            }
            catch (Exception e)
            {
                using (DubaiPortDB db = new DubaiPortDB())
                {
                    db.ErrorLogs.Add(new Model.ErrorLog
                    {

                        Detail = e.ToString(),
                        Type = (byte)ErrorType.MAIL_EXCEPTION,
                        CreatedDate = DateTime.Now,

                    });

                    db.SaveChanges();
                }
            }
        }
    }
}