﻿using DPM.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static DPM.Api.Constants.Enums;
using static DPM.Model.ApiModels.QuestionModel;

namespace DPM.Api.Helpers
{
    public class QuestionHelper : MainHelper
    {

        public int _vehicleTypeId { get; set; }
        public List<Question> questions = new List<Question>();

        public QuestionHelper()
        {
            db = new DubaiPortDB();
        }

        public QuestionHelper(int vehicleTypeId)
        {
            db = new DubaiPortDB();

            _vehicleTypeId = vehicleTypeId;

            questions = GetQuestionList(_vehicleTypeId);

        }

        public void AddQuestion(QuestionAddPostModel model)
        {
            //foreach (var item in model)
            //{
            //}
            Question newQuestion = db.Questions.Add(new Question
            {
                GroupType = model.GroupType,
                Text = model.QuestionText,
            });

            DbSavechanges();

            for (int i = 0; i < model.VehicleId.Length; i++)
            {
                db.QuestionVehicleRelationships.Add(new QuestionVehicleRelationship
                {
                    QuestionId = newQuestion.QuestionId,
                    VehicleTypeId = model.VehicleId[i],
                    CategoryId = model.CategoryId,
                });

                DbSavechanges();
            }
        }

        public bool EditQuestion(GetQuestionModel model)
        {
            var question = db.Questions.Where(w => w.QuestionId == model.QuestionId).Include(i => i.QuestionVehicleRelationships).FirstOrDefault();

            if (question != null)
            {
                question.Text = model.Question;
                question.GroupType = model.Group;
                question.UpdatedDate = DateTime.Now;

                DbSavechanges();

                question.QuestionVehicleRelationships.ToList().ForEach(f =>
                {

                    f.CategoryId = model.Category;

                    if (model.Vehicle.Contains(f.VehicleTypeId))
                        f.Status = (byte)GeneralStatus.PASSIVE;

                    DbSavechanges();
                });

                for (int i = 0; i < model.Vehicle.Length; i++)
                {
                    if (!question.QuestionVehicleRelationships.Where(w => w.Status == (byte)GeneralStatus.ACTIVE && w.VehicleTypeId == model.Vehicle[i]).Any())
                    {
                        db.QuestionVehicleRelationships.Add(new QuestionVehicleRelationship
                        {
                            QuestionId = model.QuestionId,
                            CategoryId = model.Category,
                            VehicleTypeId = model.Vehicle[i],
                        });

                        DbSavechanges();
                    }
                }

                return true;

            }

            return false;
        }

        public bool AnyFinishSurvey(int userId, int vehicleId)
        {
            return questions.Where(w => w.GroupType == (int)QuestionGroupType.RELAY_AFTER).Any();
        }

        public QuestionReturnModel GetQuestions(int userId, int vehicleId, QuestionGroupType groupType = QuestionGroupType.RELAY_BEFORE)
        {
            QuestionReturnModel resultModel = new QuestionReturnModel();

            List<CategorizedQuestionModel> categorizedModel = new List<CategorizedQuestionModel>();

            var vTypeID = db.Vehicles.Where(w => w.VehicleId == vehicleId).Select(s => s.VehicleTypeId).FirstOrDefault();

            var questionsForVehicleType = db.QuestionVehicleRelationships
                .Where(w => w.VehicleTypeId == vTypeID && w.Status == (byte)GeneralStatus.ACTIVE)
                .GroupBy(g => g.CategoryId)
                .ToList();

            int counter = 1, countquestion = 1;

            foreach (var item in questionsForVehicleType)
            {
                CategorizedQuestionModel categorizedQuestionModel = new CategorizedQuestionModel();

                if (counter == 1)
                    categorizedQuestionModel.ActiveClass = "current";
                else
                    categorizedQuestionModel.ActiveClass = "pending";

                categorizedQuestionModel.CategoryIndex = counter;
                counter++;

                List<Question> categorizedQuestions = item.Where(w => w.Question.GroupType == (int)groupType && w.Status == (byte)GeneralStatus.ACTIVE).Select(s => s.Question).ToList();



                categorizedQuestionModel.CategoryText = item.Where(w => w.Question.GroupType == (int)groupType && w.Status == (byte)GeneralStatus.ACTIVE).Select(s => s.Category.Description).FirstOrDefault();


                categorizedQuestionModel.Questions.AddRange(categorizedQuestions.Select(s => new QuestionResponseModel
                {
                    QuestionId = s.QuestionId,
                    QuestionText = s.Text,

                }).ToList());

                categorizedQuestionModel.Questions.ForEach(f =>
                {
                    f.Class = countquestion % 2 == 0 ? "even" : "odd";
                    countquestion++;
                });

                categorizedQuestionModel.Partial = questionsForVehicleType.Count;
                categorizedQuestionModel.CategoryName = item.Where(w => w.Question.GroupType == (int)groupType).Select(s => s.Category.Code).FirstOrDefault();

                categorizedModel.Add(categorizedQuestionModel);



            }

            resultModel.CategorizedModel.AddRange(categorizedModel);

            resultModel.VehicleName = db.UserVehicleRelationships
                .Where(w => w.UserId == userId && w.VehicleId == vehicleId)
                .Select(s => s.Vehicle.Name)
                .FirstOrDefault();

            return resultModel;
        }

        public List<AdminQuestionModel> GetQuestionsForAdmin()
        {
            List<AdminQuestionModel> generalModel = new List<AdminQuestionModel>();
            List<AdminQuestionPostModel> model = new List<AdminQuestionPostModel>();

            try
            {
                var grouppedQuestions = db.Questions
               .Where(w => w.Status == (int)GeneralStatus.ACTIVE)
               .Include(i => i.QuestionVehicleRelationships)
               .ToList();

                foreach (var item in grouppedQuestions)
                {
                    AdminQuestionModel questionModel = new AdminQuestionModel();

                    //List<AdminQuestionModel> adminQuestions = item.Select(s => new AdminQuestionModel
                    //{
                    //    Category = s.Category,
                    //    GroupType = s.Question.GroupType,
                    //    Text = s.Question.Text,
                    //    QuestionId = s.Question.QuestionId,
                    //    VehicleType = s.VehicleType.Code,
                    //}).ToList();



                    //questionModel.Questions = adminQuestions;
                    //questionModel.VehicleTypeName = item
                    //    .Select(s => s.VehicleType.Code)
                    //    .FirstOrDefault();
                    string vehicleNames = string.Empty;

                    item.QuestionVehicleRelationships.ToList().ForEach(f => vehicleNames += " " + f.VehicleType.Code);

                    questionModel.Category = item.QuestionVehicleRelationships.FirstOrDefault().Category.Code;
                    questionModel.GroupType = item.GroupType;
                    questionModel.Text = item.Text;
                    questionModel.QuestionId = item.QuestionId;
                    questionModel.VehicleType = vehicleNames;

                    generalModel.Add(questionModel);

                    //model.Add(questionModel);
                }
            }
            catch (System.Exception ex)
            {
                Message = ex.ToString();
                Code = ServiceCode.FAIL;
            }



            return generalModel;
        }

        public GetQuestionModel GetQuestionById(int Id)
        {
            GetQuestionModel model = new GetQuestionModel();

            model = db.Questions.Where(w => w.QuestionId == Id).Select(s => new GetQuestionModel
            {
                Group = s.GroupType,
                Question = s.Text,
                QuestionId = s.QuestionId,
                Category = s.QuestionVehicleRelationships.FirstOrDefault().Category.CategoryId,
            }).FirstOrDefault();

            model.Vehicle = db.QuestionVehicleRelationships.Where(w => w.QuestionId == Id).Select(s => s.VehicleTypeId).ToArray();

            return model;
        }

        public bool RemoveQuestion(int questionId)
        {
            bool result = false;

            Question question = db.Questions.Where(w => w.QuestionId == questionId).FirstOrDefault();

            if (question != null)
            {
                try
                {
                    var questionVehicle = db.QuestionVehicleRelationships.Where(w => w.QuestionId == questionId).ToList();

                    questionVehicle.ForEach(f => f.Status = (int)GeneralStatus.PASSIVE);

                    DbSavechanges();

                    question.Status = (int)GeneralStatus.PASSIVE;

                    DbSavechanges();

                    result = true;
                }
                catch (System.Exception e)
                {
                    Message = e.ToString();
                    Code = ServiceCode.FAIL;

                }

            }
            else
            {
                Message = "Soru bulunamadı";
                Code = ServiceCode.NOT_FOUND;

            }

            return result;
        }

        private List<Question> GetQuestionList(int vehicleId)
        {
            return db.QuestionVehicleRelationships
                .Where(w => w.VehicleTypeId == _vehicleTypeId)
                .Select(s => s.Question)
                .ToList();
        }





    }
}