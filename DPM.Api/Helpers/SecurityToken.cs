﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IdentityModel.Tokens;
using Microsoft.IdentityModel.Protocols.WSTrust;
using static Microsoft.IdentityModel.Protocols.WSTrust.WSTrust13Constants;
using System.ServiceModel;

namespace DPM.Api.Helpers
{
    public class SecurityToken
    {
        public static System.IdentityModel.Tokens.SecurityToken CreateToken()
        {
            WSTrustChannelFactory trustChannelFactory = new WSTrustChannelFactory("STSIssuerService");
            trustChannelFactory.TrustVersion = System.ServiceModel.Security.TrustVersion.WSTrust13;
            trustChannelFactory.Credentials.UserName.UserName = "Metacortex";
            trustChannelFactory.Credentials.UserName.Password = "Welcome2019!";

            WSTrustChannel channel = (WSTrustChannel)trustChannelFactory.CreateChannel();
            RequestSecurityToken rst = new RequestSecurityToken(RequestTypes.Issue);
            rst.AppliesTo = new EndpointAddress("http://uat-app/zodiacwebservice/zodiacservice.asmx");
            RequestSecurityTokenResponse rstr;

            var token = channel.Issue(rst, out rstr);
            return token;
        }
    }
}