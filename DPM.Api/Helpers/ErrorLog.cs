﻿using DPM.Model;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using static DPM.Api.Constants.Enums;

namespace DPM.Api.Helpers
{
    public class ErrorLog
    {
        public static string pathErrorLog = "C:\\inetpub\\Errors\\DPMApi\\";
        private static readonly object LogErrorlocker = new object();

        public ErrorLog()
        {

        }

        public static void LogError(string msg, string path = null)
        {
            lock (LogErrorlocker)
            {
                if (path == null)
                    path = pathErrorLog;

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string logFileName = string.Format("{0:yyyy-MM-dd}", DateTime.Now) + ".txt";

                StreamWriter sw = new StreamWriter(path + logFileName, true);

                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + msg + "\n\t");
                sw.Dispose();
            }
        }

        public static void LogError(Exception exp = null, string path = null)
        {
            lock (LogErrorlocker)
            {
                if (path == null)
                    path = pathErrorLog;

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                string logFileName = string.Format("{0:yyyy-MM-dd}", DateTime.Now) + ".txt";

                StreamWriter sw = new StreamWriter(path + logFileName, true);

                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + "\n\t" + GetExceptionDetails(exp));
                sw.Dispose();
            }
        }

        public static string GetExceptionDetails(Exception exp = null)
        {

            if (exp == null)
                return "";
            String errorMessage = string.Join(" ", new string[] {
                                                               "____________________________ERROR BEGIN____________________________",Environment.NewLine,"\n\n",
                                                               "Method Name : ", new StackTrace(new StackFrame(2)).GetFrame(0).ToString(),Environment.NewLine, "\n",
                                                               "Error Message: [ " , (exp.Message != null ? exp.Message.ToString() : "") , " ] ",Environment.NewLine,"\n",
                                                               "Error InnerException: [ " , (exp.InnerException != null ? exp.InnerException.ToString() : "") , " ] ",Environment.NewLine,"\n",
                                                               "Error Source: [ " , (exp.Source != null ? exp.Source.ToString() : "") , " ] ",Environment.NewLine,"\n",
                                                               "Error Data: [ " , (exp.Data != null ? exp.Data.ToString() : "") , " ] ",Environment.NewLine,"\n",
                                                               "Error StackTrace: [ " , (exp.StackTrace != null ? exp.StackTrace.ToString() : "") , " ] ",Environment.NewLine,"\n",
                                                               "____________________________ERROR END______________________________",
                                                                "\n\n",});



            return errorMessage;
        }

        public static void AddDB(Exception ex, int? userId, ErrorType type = ErrorType.FAIL )
        {
            using (DubaiPortDB db = new DubaiPortDB())
            {
                if(db.ErrorLogs.LastOrDefault().Detail!= ex.Message && db.ErrorLogs.LastOrDefault().Detail != ex.InnerException.Message.ToString())
                {
                    db.ErrorLogs.Add(new Model.ErrorLog
                    {
                        Detail = ex.Message != null ? ex.Message.ToString() : ex.InnerException.Message.ToString(),
                        UserId = userId,
                        Type = (byte)type,
                    });

                    db.SaveChanges();
                }
            }

            LogError(ex);

        }
    }
}