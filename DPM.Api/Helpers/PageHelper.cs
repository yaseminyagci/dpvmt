using DPM.Model;
using DPM.Model.ApiModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static DPM.Api.Constants.Enums;
using static DPM.Model.ApiModels.PageModel;

namespace DPM.Api.Helpers
{
    public class PageHelper : MainHelper
    {
        public User CurrentUser { get; set; }

        public PageHelper(int userId)
        {
            CurrentUser = db.Users.Where(w => w.UserId == userId).FirstOrDefault();
        }

        public List<PageModel.EventLogs> GetLogHistory()
        {
            List<PageModel.EventLogs> result = new List<PageModel.EventLogs>();

            var logs = db.EventLogs
                .Where(w => w.UserId == CurrentUser.UserId)
                .Include(i => i.EventLogTimes)
                .OrderByDescending(o => o.LogDate)
                .ToList();

            try
            {
                result = logs.Where(w => w.EventLogTimes != null)
               .Select(s => new PageModel.EventLogs
               {
                   Event = s.Event.Description,
                   EventStart = logs.Where(w => w.LogId == s.StartLogId).FirstOrDefault().LogDate,
                   EventEnd = s.LogDate,
                   Time = new TimeSpan(s.EventLogTimes.FirstOrDefault().LogTimeSpan),
                   UsedVehicle = s.VehicleId.ToString(),

               }).ToList();

            }
            catch (Exception ex)
            {
                Code = Constants.Enums.ServiceCode.FAIL;
                Message = ex.ToString();
            }



            return result;
        }

        public PageModel.DetailPageModel GetDetailPage()
        {
            PageModel.DetailPageModel model = new PageModel.DetailPageModel();

            model.Logs = GetLogHistory();
            model.CurrentEvent = CurrentUser.Event.Description;
            model.CurrentVehicle = CurrentUser.Vehicle.Name;

            return model;
        }

        public AdminModel.Buttons CheckButtons()
        {
            AdminModel.Buttons buttons = new AdminModel.Buttons();

            List<Option> options = db.Options.ToList();

            buttons.FaultButton = options.Where(w => w.Key == "FaultButton").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;
            buttons.BreakButton = options.Where(w => w.Key == "BreakButton").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;
            buttons.MealButton = options.Where(w => w.Key == "MealButton").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;
            buttons.RelayEndButton = options.Where(w => w.Key == "RelayEndButton").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;

            return buttons;
        }

        public AdminModel.Buttons ChangeButtons(AdminModel.Buttons buttons)
        {

            List<Option> options = db.Options.ToList();

            options.Where(w => w.Key == "FaultButton").FirstOrDefault().Value = buttons.FaultButton ? "1" : "0";
            options.Where(w => w.Key == "BreakButton").FirstOrDefault().Value = buttons.BreakButton ? "1" : "0";
            options.Where(w => w.Key == "MealButton").FirstOrDefault().Value = buttons.MealButton ? "1" : "0";
            options.Where(w => w.Key == "RelayEndButton").FirstOrDefault().Value = buttons.RelayEndButton ? "1" : "0";

            DbSavechanges();

            return buttons;
        }

        public AdminModel.NotificationOptions ChangeNotificationOptions(AdminModel.NotificationOptions option)
        {
            List<Option> options = db.Options.ToList();

            options.Where(w => w.Key == "ACategoryMail").FirstOrDefault().Value = option.ACategoryMail? "1" : "0";
            options.Where(w => w.Key == "ACategoryFault").FirstOrDefault().Value = option.ACategoryFault ? "1" : "0";
            options.Where(w => w.Key == "BCategoryMail").FirstOrDefault().Value = option.BCategoryMail ? "1" : "0";
            options.Where(w => w.Key == "BCategoryFault").FirstOrDefault().Value = option.BCategoryFault ? "1" : "0";
            options.Where(w => w.Key == "CCategoryMail").FirstOrDefault().Value = option.CCategoryMail ? "1" : "0";
            options.Where(w => w.Key == "CCategoryFault").FirstOrDefault().Value = option.CCategoryFault ? "1" : "0";

            DbSavechanges();

            return option;
        }

        public AdminModel.NotificationOptions CheckNotificationOptions()
        {
            AdminModel.NotificationOptions option = new AdminModel.NotificationOptions();

            List<Option> options = db.Options.ToList();

            option.ACategoryFault = options.Where(w => w.Key == "ACategoryFault").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;
            option.ACategoryMail = options.Where(w => w.Key == "ACategoryMail").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;
            option.BCategoryFault = options.Where(w => w.Key == "BCategoryFault").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;
            option.BCategoryMail = options.Where(w => w.Key == "BCategoryMail").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;
            option.CCategoryFault = options.Where(w => w.Key == "CCategoryFault").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;
            option.CCategoryMail = options.Where(w => w.Key == "CCategoryMail").Select(s => s.Value).FirstOrDefault() == "1" ? true : false;

            return option;
        }

        public ChartModel GetChart(int vehicleType)
        {
            ChartModel model = new ChartModel();

            TimeSpan[] allEventTime = new TimeSpan[4];
            int j = 0;


            DateTime lastWeek = DateTime.Now.AddDays(-7);

            List<int> vehicleIds = db.Vehicles
                .Where(w => w.VehicleTypeId == vehicleType
                && w.Status == (int)GeneralStatus.ACTIVE)
                .Select(s => s.VehicleId)
                .ToList();

            var totalEvents = db.EventLogTimes
                .Where(w => w.EventLog.LogDate >= lastWeek
                && vehicleIds.Contains(w.EventLog.VehicleId.Value))
                .ToList();

            for (int i = 3; i < 16; i += 4)
            {
                allEventTime[j] = TimeSpan.FromTicks(totalEvents.Where(w => w.EventTypeId == i).Sum(s => s.LogTimeSpan));
                j++;
            }

            if (allEventTime[0] != TimeSpan.Zero)
            {
                model.Rates[0] = (double)allEventTime[1].Ticks / (allEventTime[0].Ticks - allEventTime[1].Ticks) * 100;
                model.Rates[1] = (double)allEventTime[2].Ticks / (allEventTime[0].Ticks - allEventTime[2].Ticks) * 100;
                model.Rates[2] = (double)allEventTime[3].Ticks / (allEventTime[0].Ticks - allEventTime[3].Ticks) * 100;
                model.Rates[3] = (double)(allEventTime[0].Ticks - allEventTime[1].Ticks - allEventTime[2].Ticks - allEventTime[3].Ticks) / allEventTime[0].Ticks * 100;
            }


            model.TotalSession = allEventTime[0];

            return model;
        }

        public bool SetFault(int vehicleId)
        {

            var vehicle = db.Vehicles.Where(w => w.VehicleId == vehicleId).FirstOrDefault();

            vehicle.CurrentState = (byte)EventLogType.FAULT_STARTED;

            DbSavechanges();

            return true;
        }

        public bool RemoveFault(int vehicleId)
        {

            var vehicle = db.Vehicles.Where(w => w.VehicleId == vehicleId).FirstOrDefault();

            vehicle.CurrentState = (byte)EventLogType.FAULT_ENDED;

            DbSavechanges();

            return true;
        }
    }
}