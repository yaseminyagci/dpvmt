﻿using DPM.Api.Constants;
using DPM.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static DPM.Api.Constants.Enums;
using static DPM.Model.ApiModels.LogModels;

namespace DPM.Api.Helpers
{
    public class EventLogHelper : MainHelper
    {

        public static void AddEventLog(EventLogType type, int userId, int? vehicleId, bool isSurveyEndQuestion = false)
        {
            EventLog log = new EventLog();

            using (DubaiPortDB db = new DubaiPortDB())
            {
                log = db.EventLogs.Add(new EventLog
                {
                    EventId = (byte)type,
                    UserId = userId,
                    VehicleId = vehicleId,
                });

                db.SaveChanges();

                UpdateCurrentState(type, userId, vehicleId.Value, isSurveyEndQuestion);

            }

            if (type == EventLogType.BREAK_ENDED || type == EventLogType.RELAY_ENDED || type == EventLogType.FAULT_ENDED || type == EventLogType.MEAL_END || type == EventLogType.LOGOUT)
                EventEnd(log, type);

            if (type == EventLogType.RELAY_ENDED || type == EventLogType.FAULT_ENDED)//Vardiya bitimi veya arıza bitimi ise logout yap.
            {
                AddEventLog(EventLogType.LOGOUT, userId, vehicleId, isSurveyEndQuestion);
            }

            if (type == EventLogType.FAULT_STARTED)//arıza başlangıcı ise arıza bitimi yap dolaylı olarak logout olacak
            {
                AddEventLog(EventLogType.FAULT_ENDED, userId, vehicleId, isSurveyEndQuestion);
            }

        }

        private static void EventEnd(EventLog log, EventLogType type)
        {
            using (DubaiPortDB db = new DubaiPortDB())
            {
                EventLog lastEventLog = db.EventLogs.Where(w => w.EventId == (byte)(type - 1))//Başlangıç logu
                .OrderByDescending(o => o.LogDate)
                .FirstOrDefault();

                TimeSpan exactTime = log.LogDate - lastEventLog.LogDate;

                int eventType = (int)type + ((int)type - 1);//Eventin başlangıç ve bitişleri toplamı ana event türünü veriyor

                db.EventLogTimes.Add(new EventLogTime
                {
                    EventLogId = log.LogId,
                    EventTypeId = (byte)eventType,
                    LogTimeSpan = exactTime.Ticks,
                });

                db.SaveChanges();

                log = db.EventLogs.Where(w => w.LogId == log.LogId).FirstOrDefault();

                log.StartLogId = lastEventLog.LogId;

                db.SaveChanges();

            }
        }

        private static void UpdateCurrentState(EventLogType type, int userId, int vehicleId, bool isSurveyEndQuestion = false)
        {
            using (DubaiPortDB db = new DubaiPortDB())
            {
                User user = db.Users.Where(w => w.UserId == userId)
                    .Where(w => w.Status == (int)GeneralStatus.ACTIVE)
                    .FirstOrDefault();

                user.CurrentState = (byte)type;

                db.SaveChanges();

                Vehicle vehicle = db.Vehicles
                    .Where(w => w.Status == (int)GeneralStatus.ACTIVE && w.VehicleId == vehicleId)
                    .FirstOrDefault();

                if (type != EventLogType.FAULT_ENDED && vehicle.CurrentState != (int)EventLogType.FAULT_STARTED)//Arıza başlaması ve bitmesi user için hemen gerçekleşiyor.Araç için adminin kapatması gerekli
                    vehicle.CurrentState = (byte)type;

                db.SaveChanges();

                if (type == EventLogType.LOGOUT && !isSurveyEndQuestion)//Logout ise tokeni sil
                {
                    UserToken userToken = db.UserTokens.Where(w => w.UserId == userId).FirstOrDefault();

                    userToken.TokenValue = string.Empty;

                    try
                    {
                        db.SaveChanges();

                    }
                    catch (Exception)
                    {

                    }
                }
            }

        }

        public static DataTableModel<UserLogModel> GetUserLogs(int[] userIds)
        {
            DataTableModel<UserLogModel> dataTable = new DataTableModel<UserLogModel>();

            List<UserLogModel> model = new List<UserLogModel>();
            EventType eventType;

            using (DubaiPortDB db = new DubaiPortDB())
            {

                List<EventLogTime> logs = db.EventLogTimes
                    .WhereIf(userIds.Length > 0, w => userIds.Contains(w.EventLog.UserId))
                .OrderByDescending(o => o.LogDate)
                .Include(i => i.EventLog)
                .ToList();

                var vehicleIds = logs.GroupBy(g => g.EventLog.VehicleId).Select(s => s.Key).ToList();

                List<Vehicle> vehicles = db.Vehicles.Where(w => vehicleIds.Contains(w.VehicleId)).ToList();

                foreach (var item in logs)
                {
                    UserLogModel userLog = new UserLogModel();

                    if (item.EventLog.StartLogId == 0)
                        continue;

                    EventLog startLog = db.EventLogs.Where(w => w.LogId == item.EventLog.StartLogId).Include(i => i.User).FirstOrDefault();

                    userLog.UserId = startLog.UserId;
                    userLog.UserName = $"{startLog.User.FirstName} {startLog.User.LastName}";
                    userLog.StartTime = startLog.LogDate;
                    userLog.EndTime = item.LogDate;
                    userLog.Vehicle = vehicles.Where(w => w.VehicleId == item.EventLog.VehicleId).FirstOrDefault().Name;
                    userLog.Event = Enum.TryParse(item.EventTypeId.ToString(), out eventType) ? eventType.ToEvent() : "-";
                    userLog.PassingTime = new TimeSpan(item.LogTimeSpan);

                    model.Add(userLog);
                }
            }

            dataTable.meta = new Meta(); 


            dataTable.meta.total = model.Count;
            dataTable.meta.field = "EndTime";
            dataTable.data = model;

            return dataTable;
        }

        public static List<UserLogModel> GetVehicleLogs(int[] vehicleIds)
        {
            List<UserLogModel> model = new List<UserLogModel>();
            EventType eventType;


            using (DubaiPortDB db = new DubaiPortDB())
            {
                List<Vehicle> vehicles = db.Vehicles.Where(w => vehicleIds.Contains(w.VehicleTypeId)).ToList();

                var vehicleID = vehicles.Select(s => s.VehicleId).ToList();

                List<EventLogTime> logs = db.EventLogTimes
                    .Where(w => vehicleID.Contains(w.EventLog.VehicleId.Value))
                .OrderByDescending(o => o.LogDate)
                .Include(i => i.EventLog)
                .ToList();


                foreach (var item in logs)
                {
                    UserLogModel userLog = new UserLogModel();

                    if (item.EventLog.StartLogId == 0)
                        continue;

                    EventLog startLog = db.EventLogs.Where(w => w.LogId == item.EventLog.StartLogId).Include(i => i.User).FirstOrDefault();

                    userLog.UserId = startLog.UserId;
                    userLog.UserName = $"{startLog.User.FirstName} {startLog.User.LastName}";
                    userLog.StartTime = startLog.LogDate;
                    userLog.EndTime = item.LogDate;
                    userLog.Vehicle = vehicles.Where(w => w.VehicleId == item.EventLog.VehicleId).FirstOrDefault().Name;
                    userLog.Event = Enum.TryParse(item.EventTypeId.ToString(), out eventType) ? eventType.ToEvent() : "-";
                    userLog.PassingTime = new TimeSpan(item.LogTimeSpan);

                    model.Add(userLog);
                }
            }

            return model;

        }

    }
}