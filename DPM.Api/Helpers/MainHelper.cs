﻿using DPM.Model;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using static DPM.Api.Constants.Enums;

namespace DPM.Api.Helpers
{
    public class MainHelper
    {

        public ServiceCode Code { get; set; } = ServiceCode.OK;

        public string Message { get; set; }

        public DubaiPortDB db { get; set; } = new DubaiPortDB();

        public void DbSavechanges()
        {
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Code = ServiceCode.FAIL;
                Message = ex.InnerException.Message.ToString();
                ErrorLog.AddDB(ex, null, ErrorType.FAIL);
            }
            catch (Exception e)
            {
                Code = ServiceCode.FAIL;
                Message = e.ToString();

                ErrorLog.AddDB(e, null, ErrorType.FAIL);
            }
        }

        public string CreateToken()
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            return Convert.ToBase64String(time.Concat(key).ToArray());
        }

        public string GetIP()
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;

                string result = string.Empty;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        result = addresses[0];
                    }
                }
                else
                    result = context.Request.ServerVariables["REMOTE_ADDR"];

                return result = result == "::1" ? "127.0.0.1" : result;
            }
            catch (Exception)
            {
                //Debug.WriteLine($"Threadsafe processing error handled. {ex.Message}");
            }

            return string.Empty;

        }

        public static void ClearSession(User currentUser)
        {
            using (DubaiPortDB db = new DubaiPortDB())
            {
                UserToken userToken = db.UserTokens.Where(w => w.UserId == currentUser.UserId).FirstOrDefault();

                if (userToken != null)
                    userToken.TokenValue = string.Empty;

                db.SaveChanges();
            }
        }
    }
}