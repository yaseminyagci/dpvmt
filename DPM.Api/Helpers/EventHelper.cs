﻿using DPM.Api.Properties;
using DPM.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static DPM.Api.Constants.Enums;
using static DPM.Model.ApiModels.QuestionModel;

namespace DPM.Api.Helpers
{
    public class EventHelper : MainHelper
    {
        public EventHelper(int userId)
        {
            CurrentUser = db.Users.Where(w => w.UserId == userId).FirstOrDefault();

            EventLogType tempType;

            if (Enum.TryParse<EventLogType>(CurrentUser?.CurrentState.ToString(), out tempType))
                CurrentEvent = tempType;
        }

        public EventHelper(string token)
        {
            var tokenUser = db.UserTokens.Where(w => w.TokenValue == token).Include(i=>i.User).FirstOrDefault();

            CurrentUser = tokenUser.User;
        }

        public EventLogType CurrentEvent { get; set; }
        private User CurrentUser { get; set; }

        public string GetCurrentEvent()
        {
            db = new DubaiPortDB();
            CurrentUser = db.Users.Where(w => w.UserId == CurrentUser.UserId).FirstOrDefault();

            CurrentEvent = (EventLogType)CurrentUser.Event.EventId;

            return CurrentUser.Event.Description;
        }

        #region Event Action Methods

        /// <summary>
        /// Mola başlat
        /// </summary>
        /// <returns></returns>
        public string BreakStart()
        {
            if (CurrentEvent == EventLogType.LOGOUT)
            {
                Message = "Kullanıcı online durumunda değil";

                Code = ServiceCode.USER_IN_PROCESS;
            }
            else if (CheckButton(ButtonType.BreakButton.ToString()))
            {
                Message = "Bu işlem yönetici tarafından engellendi";
                Code = ServiceCode.EVENT_CLOSED;
            }
            else
            {
                EventLogHelper.AddEventLog(EventLogType.BREAK_STARTED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);
            }


            return GetCurrentEvent();
        }

        /// <summary>
        /// Mola Bitir
        /// </summary>
        /// <returns></returns>
        public string BreakEnd()
        {
            if (CurrentEvent == EventLogType.BREAK_STARTED)
            {
                EventLogHelper.AddEventLog(EventLogType.BREAK_ENDED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);

            }
            else
            {
                Code = ServiceCode.USER_IN_PROCESS;
                Message = "Kulllanıcı Mola durumunda değil";
            }


            return GetCurrentEvent();
        }

        /// <summary>
        /// Arıza başlat
        /// </summary>
        /// <returns></returns>
        public string FaultStart()
        {
            if (CurrentEvent == EventLogType.LOGOUT)
            {
                Message = "Kullanıcı online durumunda değil";
                Code = ServiceCode.USER_IN_PROCESS;
            }
            else if (CheckButton(ButtonType.FaultButton.ToString()))
            {
                Message = "Bu işlem yönetici tarafından engellendi";
                Code = ServiceCode.EVENT_CLOSED;
            }
            else
            {

                var option = db.Options.Where(w => w.Key == "ACategoryFault").FirstOrDefault();

                if (option != null && option.Value == "1")
                {
                    EventLogHelper.AddEventLog(EventLogType.FAULT_STARTED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);
                }

                EmailSender emailSender = new EmailSender();

                var message = $"{CurrentUser.FirstName} {CurrentUser.LastName} kullanıcısı {CurrentUser.Vehicle.Name} aracında arıza bildirimi yaptı.";

                emailSender.SendMail(new List<string>() { Settings.Default.SuperVisorEmail }, new List<string>() { }, "Operatör Arıza Bildirimi",new List<string>() { message});

            }


            return GetCurrentEvent();
        }

        /// <summary>
        /// Arıza Bitir
        /// </summary>
        /// <returns></returns>
        public string FaultEnd()
        {
            if (CurrentEvent == EventLogType.LOGOUT)
            {
                Message = "Kullanıcı online durumunda değil";
                Code = ServiceCode.USER_IN_PROCESS;
            }

            EventLogHelper.AddEventLog(EventLogType.FAULT_ENDED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);

            return GetCurrentEvent();
        }

        /// <summary>
        /// Vardiya Başlat
        /// </summary>
        /// <returns></returns>
        public string RelayStart()
        {
            if (CurrentEvent == EventLogType.LOGOUT)
            {
                Message = "Kullanıcı online durumunda değil";
                Code = ServiceCode.USER_IN_PROCESS;
            }

            EventLogHelper.AddEventLog(EventLogType.RELAY_STARTED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);

            return GetCurrentEvent();
        }

        /// <summary>
        /// Vardiya bitir
        /// </summary>
        /// <returns></returns>
        public string RelayEnd()
        {
            if (CurrentEvent == EventLogType.LOGOUT)
            {
                Message = "Kullanıcı online durumunda değil";
                Code = ServiceCode.USER_IN_PROCESS;
            }

            EventLogHelper.AddEventLog(EventLogType.RELAY_ENDED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);

            return GetCurrentEvent();
        }

        /// <summary>
        /// Yemek Başlat
        /// </summary>
        /// <returns></returns>
        public string MealStart()
        {
            if (CurrentEvent == EventLogType.LOGOUT)
            {
                Message = "Kullanıcı online durumunda değil";
                Code = ServiceCode.USER_IN_PROCESS;
            }
            else if (CheckButton(ButtonType.MealButton.ToString()))
            {
                Message = "Bu işlem yönetici tarafından engellendi";
                Code = ServiceCode.EVENT_CLOSED;
            }
            else
            {
                EventLogHelper.AddEventLog(EventLogType.MEAL_STARTED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);

            }


            return GetCurrentEvent();
        }

        /// <summary>
        /// Yemek Bitir
        /// </summary>
        /// <returns></returns>
        public string MealEnd()
        {
            if (CurrentEvent == EventLogType.LOGOUT)
            {
                Message = "Kullanıcı online durumunda değil";
                Code = ServiceCode.USER_IN_PROCESS;
            }

            EventLogHelper.AddEventLog(EventLogType.FAULT_ENDED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);

            return GetCurrentEvent();
        }

        public EventEndQuestionModel EventEnd()
        {
            CurrentEvent += 1;//Aktif olan eventin bitişi     

            EventEndQuestionModel result = new EventEndQuestionModel();
            var helper = new QuestionHelper(CurrentUser.CurrentVehicleId.Value);
            result.AnyFinishSurvey = helper.AnyFinishSurvey(CurrentUser.UserId, CurrentUser.CurrentVehicleId.Value);


            //if (CurrentEvent == EventLogType.LOGOUT)
            //{
            //    Message = "Kullanıcı online durumunda değil";
            //    Code = ServiceCode.USER_IN_PROCESS;
            //}
            if ((int)CurrentEvent % 2 == 0)
            {

                EventLogHelper.AddEventLog(CurrentEvent, CurrentUser.UserId, CurrentUser.CurrentVehicleId, result.AnyFinishSurvey);
            }
            else
            {
                EventLogHelper.AddEventLog(EventLogType.RELAY_ENDED, CurrentUser.UserId, CurrentUser.CurrentVehicleId);
            }


            result.CurrentEvent = GetCurrentEvent();

            return result;
        }

        #endregion

        private bool CheckButton(string button)
        {
            return db.Options.Where(w => w.Key == button).FirstOrDefault().Value == "1" ? false : true;
        }


    }
}