﻿using DPM.Api.Constants;
using DPM.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static DPM.Api.Constants.Enums;
using static DPM.Model.ApiModels.LogModels;
using static DPM.Model.ApiModels.VehicleModel;

namespace DPM.Api.Helpers
{
    public class VehicleHelper : MainHelper
    {
        public VehicleHelper()
        {
            db = new DubaiPortDB();
        }

        public void AddVehicle(VehiclePostModel model)
        {
            //foreach (var item in model)
            //{
            //}
            AddVehicleType(model);

            db.Vehicles.Add(new Vehicle
            {
                Name = model.VehicleName,
                VehicleTypeId = model.VehicleType,
                MacAddress = model.MacAddress,
                Ip = model.Ip,
                ItemId = model.ItemId,
                SerialNumber = model.SerialNumber,
                Description = model.Description
            });


            DbSavechanges();
        }

        public List<VehicleGetModel> GetVehicles()
        {
            List<VehicleGetModel> vehicles = new List<VehicleGetModel>();

            vehicles = db.Vehicles.Include(i => i.VehicleType).Where(w => w.Status == 1).Select(s => new VehicleGetModel
            {
                Id = s.VehicleId,
                Name = s.Name

            }).ToList();

            return vehicles;
        }

        private void AddVehicleType(VehiclePostModel model)
        {
            VehicleType type = db.VehicleTypes.Where(w => w.VehicleTypeId == model.VehicleType).FirstOrDefault();

            if (type == null)
            {
                db.VehicleTypes.Add(new VehicleType
                {
                    Code = model.VehicleCode,
                    Description = model.Description,

                });

                DbSavechanges();
            }
        }

        public void AddVehicleType(VehicleTypePostModel model)
        {
            VehicleType type = db.VehicleTypes.Where(w => w.Code == model.Code).FirstOrDefault();

            if (type == null)
            {
                db.VehicleTypes.Add(new VehicleType
                {
                    Code = model.Code,
                    Description = model.Description,

                });

                DbSavechanges();
            }
            else
            {
                Message = "Aynı Kodlu araç tipi olamaz";
            }
        }

        public List<AdminVehicleModel> GetVehicles(int vehicleTypeId)
        {
            return db.Vehicles.Where(w => w.Status == (byte)GeneralStatus.ACTIVE && w.VehicleTypeId == vehicleTypeId).Select(s => new AdminVehicleModel
            {
                CreatedDate = s.CreatedDate,
                UpdatedDate = s.UpdatedDate,
                VehicleId = s.VehicleId,
                VehicleName = s.Name,
                Operators = s.UserVehicleRelationships.Select(s2 => s2.User.FirstName).ToList(),
            }).ToList();
        }

        public List<AdminVehicleModel> GetAllVehicles()
        {
            return db.Vehicles
                .Where(w => w.Status == (byte)GeneralStatus.ACTIVE)
                .Select(s => new AdminVehicleModel
                {
                    CreatedDate = s.CreatedDate,
                    UpdatedDate = s.UpdatedDate,
                    VehicleId = s.VehicleId,
                    VehicleName = s.Name,
                    Operators = s.UserVehicleRelationships.Select(s2 => s2.User.FirstName).ToList(),
                    Description = s.Description,
                    ItemId = s.ItemId,
                    SerialNumber = s.SerialNumber,
                    Ip = s.Ip,
                    MacAddress = s.MacAddress,
                    Model = s.Model,
                    VehicleType = s.VehicleType.Code
                }).ToList();
        }

        public List<AdminVehiclePostModel> GetVehiclesForAdmin()
        {
            List<AdminVehiclePostModel> model = new List<AdminVehiclePostModel>();

            var vehicles = db.Vehicles
                .Where(w => w.Status == (int)GeneralStatus.ACTIVE)
                .GroupBy(g => g.VehicleTypeId)
                .ToList();

            foreach (var item in vehicles)
            {
                AdminVehiclePostModel postModel = new AdminVehiclePostModel();


                postModel.Vehicles = item.Select(s => new AdminVehicleModel
                {
                    CreatedDate = s.CreatedDate,
                    UpdatedDate = s.UpdatedDate,
                    VehicleName = s.Name,
                    VehicleId = s.VehicleId,

                })
                .ToList();

                postModel.VehicleTypeName = item.Select(s => s.VehicleType.Code).FirstOrDefault();

                model.Add(postModel);
            }

            return model;
        }

        public DataTableModel<AdminVehicleStateModel> GetVehicleStatesForAdmin()
        {
            DataTableModel<AdminVehicleStateModel> result = new DataTableModel<AdminVehicleStateModel>();

            AdminVehicleState adminVehicleState = new AdminVehicleState();

            //List<AdminVehicleStatePostModel> model = new List<AdminVehicleStatePostModel>();

            var vehicles = db.Vehicles
                .Where(w => w.Status == (byte)GeneralStatus.ACTIVE)
                .ToList();

            result.meta = new Meta();

            try
            {
                AdminVehicleStatePostModel stateModel = new AdminVehicleStatePostModel();

                stateModel.VehicleStates = vehicles.Select(s => new AdminVehicleStateModel
                {
                    CurrentState = s.CurrentState.ToEvent(),
                    CurrentStateType = s.CurrentState,
                    VehicleCode = s.VehicleType.Code,
                    VehicleName = s.Name,
                    Vehicletype = s.VehicleTypeId,
                    UserId = s.Users.Where(w => w.CurrentState != (byte)EventLogType.LOGOUT && w.CurrentState != (byte)EventLogType.PASSIVE).Select(s2 => s2.UserId).FirstOrDefault(),
                    UserName = s.Users.Where(w => w.CurrentState != (byte)EventLogType.LOGOUT && w.CurrentState != (byte)EventLogType.PASSIVE).Select(s2 => string.Concat(s2.FirstName, s2.LastName)).FirstOrDefault(),
                    VehicleId = s.VehicleId,
                    Order = s.CurrentState.ToOrderEvent(),
                })
                .ToList();

                result.data.AddRange(stateModel.VehicleStates);
                result.meta.field = "Order";

                //stateModel.VehcileTypeName = item.Select(s => s.VehicleType.Code).FirstOrDefault();
                //stateModel.rowCount = item.Count();

                //model.Add(stateModel);
                result.data = result.data.OrderBy(o => o.Order).ToList();
            }
            catch (System.Exception ex)
            {
                Code = ServiceCode.FAIL;
                Message = ex.ToString();
            }

            //adminVehicleState.States = model;

            //result.data.Add(adminVehicleState);

            return result;
        }

        public List<VehicleTypeForAdminModel> GetVehicleTypes()
        {
            return db.VehicleTypes.Where(w => w.Status == (byte)GeneralStatus.ACTIVE).Select(s => new VehicleTypeForAdminModel
            {
                Id = s.VehicleTypeId,
                Name = s.Code
            }).ToList();
        }

        public bool RemoveVehicle(int vehicleId)
        {
            bool result = false;

            Vehicle vehicle = db.Vehicles
                .Where(w => w.VehicleId == vehicleId)
                .FirstOrDefault();

            if (vehicle != null)
            {
                try
                {
                    var userVehicles = db.UserVehicleRelationships
                    .Where(w => w.VehicleId == vehicleId)
                    .ToList();

                    userVehicles.ForEach(f => f.Status = (int)GeneralStatus.PASSIVE);

                    DbSavechanges();

                    vehicle.Status = (int)GeneralStatus.PASSIVE;

                    DbSavechanges();

                    result = true;
                }
                catch (System.Exception e)
                {
                    Message = e.ToString();
                    Code = ServiceCode.FAIL;
                }

            }
            else
            {
                Message = "Araç bulunamadı";
                Code = ServiceCode.NOT_FOUND;
            }

            return result;
        }



    }
}