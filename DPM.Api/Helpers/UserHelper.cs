﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DPM.Model.ApiModels;
namespace DPM.Api.Helpers
{
    public class UserHelper : MainHelper
    {
        public UserHelper()
        {
            db = new Model.DubaiPortDB();
        }
        public void AddUser(UserModel model)
        {


            db.Users.Add(new Model.User
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Password = model.Password,
                Role = model.Role
            });


            DbSavechanges();
        }

        public List<UserModel> GetUsers()
        {
            List<UserModel> Users = new List<UserModel>();

            Users = db.Users.Where(w => w.Status == 1).Select(s => new UserModel
            {
                UserId = s.UserId,
                FirstName = s.FirstName,
                LastName = s.LastName,
                Email = s.Email,
                Password = s.Password,
                Role = s.Role

            }).ToList();

            return Users;
        }

        public UserModel GetUser(int id)
        {
            UserModel User = new UserModel();

            User = db.Users.Where(w => w.Status == 1 && w.UserId == id).Select(s => new UserModel
            {
                UserId = s.UserId,
                FirstName = s.FirstName,
                LastName = s.LastName,
                Email = s.Email,
                Password = s.Password,
                Role = s.Role

            }).FirstOrDefault();

            return User;
        }
        public bool RemoveUser(int userId)
        {
            bool result = false;

            Model.User user = db.Users
                .Where(w => w.UserId == userId)
                .FirstOrDefault();

            if (user != null)
            {
                try
                {
                    var token = db.UserTokens.Where(x => x.UserId == userId).FirstOrDefault();
                    db.UserTokens.Remove(token);
                    
                    db.Users.Remove(user);
                    DbSavechanges();
                    result = true;
                }
                catch (System.Exception e)
                {
                    Message = e.ToString();
                    Code = Constants.Enums.ServiceCode.FAIL;
                }

            }

            return result;
        }

        public void UpdateUser(UserModel userModel)
        {
            Model.User user = db.Users
                 .Where(w => w.UserId == userModel.UserId)
                 .FirstOrDefault();



            user.FirstName = userModel.FirstName;
            user.LastName = userModel.LastName;
            user.Email = userModel.Email;
            user.Password = userModel.Password;
            user.Role = userModel.Role;
            DbSavechanges();
        }
    }
}