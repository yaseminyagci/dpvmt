﻿using DPM.Model;
using DPM.Model.ApiModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using static DPM.Api.Constants.Enums;

namespace DPM.Api.Helpers
{
    public class SurveyHelper : MainHelper
    {
        private List<Survey> surveys = new List<Survey>();
        public User CurrentUser { get; set; }
        public EventHelper eventHelper { get; set; }

        public SurveyHelper(int userId)
        {
            surveys = db.Surveys.Where(w => w.UserVehicleRelationship.UserId == userId).ToList();
            CurrentUser = db.Users.Where(w => w.UserId == userId).FirstOrDefault();
            eventHelper = new EventHelper(CurrentUser.UserId);
        }

        public List<SurveyModel.SurveyHistory> GetUserSurveyHistory()
        {
            List<SurveyModel.SurveyHistory> histories = new List<SurveyModel.SurveyHistory>();

            List<int> surveyIds = surveys.Select(s => s.SurveyId).ToList();

            List<SurveyAnswer> answers = db.SurveyAnswers.Where(w => surveyIds.Contains(w.SurveyId)).ToList();

            foreach (var item in surveys)
            {
                SurveyModel.SurveyHistory history = new SurveyModel.SurveyHistory();

                history.Survey = item;
                history.SurveyTime = item.CreatedDate;
                history.SurveyVehicle = item.UserVehicleRelationship.Vehicle.Name;
                history.Answer = answers.Where(w => w.SurveyId == item.SurveyId).ToList();

                histories.Add(history);
            }

            return histories;
        }

        public bool AddSurvey(SurveyModel.AnswerPostModel model)
        {
            try
            {

                UserVehicleRelationship userVehicle = db.UserVehicleRelationships
                .Where(w => w.UserId == CurrentUser.UserId
                && w.VehicleId == CurrentUser.CurrentVehicleId)
                .FirstOrDefault();

                Survey survey = db.Surveys.Add(new Survey
                {
                    CreatedDate = DateTime.Now,
                    SurveyTypeId = model.SurveyType,
                    UserDeviceRelationshipId = userVehicle.UserVehicleRelationId,

                });

                DbSavechanges();

                foreach (var item in model.AnswerList)
                {
                    db.SurveyAnswers.Add(new SurveyAnswer
                    {
                        Answer = item.Answer,
                        QuestionId = item.QuestionId,
                        SurveyId = survey.SurveyId,
                    });

                    DbSavechanges();
                }

                foreach (var item in model.NoteList)
                {
                    if (item.Text != null)
                    {
                        db.SurveyNotes.Add(new SurveyNote
                        {
                            Note = item.Text,
                            SurveyId = survey.SurveyId,
                            CreatedDate = DateTime.Now,
                        });

                        DbSavechanges();
                    }

                }

                List<SurveyAnswer> answers = db.SurveyAnswers
                    .Where(w => w.SurveyId == survey.SurveyId)
                    .Include(i => i.Question)
                    .ToList();

                if (answers.Where(w => !w.Answer).Any())
                {
                    ImportanceLevel importanceLevel;
                    var failAnswers = answers.Where(w => !w.Answer).ToList();

                    foreach (var answer in failAnswers)
                    {
                        var category = answer.Question
                            .QuestionVehicleRelationships
                            .Where(w => w.VehicleTypeId == userVehicle.Vehicle.VehicleTypeId)
                            .Select(s => s.Category)
                            .FirstOrDefault();

                        if (Enum.TryParse(category.ImportanceLevel.ToString(), out importanceLevel))
                        {

                            switch (importanceLevel)
                            {
                                case ImportanceLevel.CRITIC://A
                                    Code = ServiceCode.SURVEY_ANSWER_CRITIC;
                                    break;
                                case ImportanceLevel.IMPORTANT://B
                                    if (Code != ServiceCode.SURVEY_ANSWER_CRITIC)
                                        Code = ServiceCode.SURVEY_ANSWER_IMPORTANT;
                                    break;
                                case ImportanceLevel.FAIL://C
                                    if (Code != ServiceCode.SURVEY_ANSWER_CRITIC && Code != ServiceCode.SURVEY_ANSWER_IMPORTANT)
                                        Code = ServiceCode.SURVEY_ANSWER_FAIL;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }


                    List<Option> options = db.Options.ToList();

                    var criticEnable = options.Where(w => w.Key == "ACategoryFault").FirstOrDefault().Value == "1" ? true : false;//A kategorisi için hayır cevabı verildiğinde arızaya düşsün mü?
                    var importantEnable = options.Where(w => w.Key == "BCategoryFault").FirstOrDefault().Value == "1" ? true : false;//B kategorisi için hayır cevabı verildiğinde arızaya düşsün mü?
                    var failEnable = options.Where(w => w.Key == "CCategoryFault").FirstOrDefault().Value == "1" ? true : false;//C kategorisi için hayır cevabı verildiğinde arızaya düşsün mü?


                    if (Code == ServiceCode.SURVEY_ANSWER_CRITIC && criticEnable)
                    {
                        eventHelper.FaultStart();
                    }
                    else if (Code == ServiceCode.SURVEY_ANSWER_IMPORTANT && importantEnable)
                    {
                        eventHelper.FaultStart();
                    }
                    else if (Code == ServiceCode.SURVEY_ANSWER_FAIL && failEnable)
                    {
                        eventHelper.FaultStart();
                    }
                    else
                    {
                    }
                    Message = survey.SurveyId.ToString();


                    return false;

                }


                eventHelper.RelayStart();

                return true;
            }
            catch (Exception e)
            {
                Message = e.ToString();
            }
            return false;

        }



        public bool AddSurveyEnd(SurveyModel.AnswerPostModel model)
        {
            try
            {

                UserVehicleRelationship userVehicle = db.UserVehicleRelationships
                .Where(w => w.UserId == CurrentUser.UserId
                && w.VehicleId == CurrentUser.CurrentVehicleId)
                .FirstOrDefault();

                Survey survey = db.Surveys.Add(new Survey
                {
                    CreatedDate = DateTime.Now,
                    SurveyTypeId = model.SurveyType,
                    UserDeviceRelationshipId = userVehicle.UserVehicleRelationId,

                });

                DbSavechanges();

                foreach (var item in model.AnswerList)
                {
                    db.SurveyAnswers.Add(new SurveyAnswer
                    {
                        Answer = item.Answer,
                        QuestionId = item.QuestionId,
                        SurveyId = survey.SurveyId,
                    });

                    DbSavechanges();
                }

                foreach (var item in model.NoteList)
                {
                    if (item.Text != null)
                    {
                        db.SurveyNotes.Add(new SurveyNote
                        {
                            Note = item.Text,
                            SurveyId = survey.SurveyId,
                            CreatedDate = DateTime.Now,
                        });

                        DbSavechanges();
                    }

                }

                List<SurveyAnswer> answers = db.SurveyAnswers
                    .Where(w => w.SurveyId == survey.SurveyId)
                    .Include(i => i.Question)
                    .ToList();

                if (answers.Where(w => !w.Answer).Any())
                {
                    ImportanceLevel importanceLevel;
                    var failAnswers = answers.Where(w => !w.Answer).ToList();

                    foreach (var answer in failAnswers)
                    {
                        var category = answer.Question
                            .QuestionVehicleRelationships
                            .Where(w => w.VehicleTypeId == userVehicle.Vehicle.VehicleTypeId)
                            .Select(s => s.Category)
                            .FirstOrDefault();

                        if (Enum.TryParse(category.ImportanceLevel.ToString(), out importanceLevel))
                        {

                            switch (importanceLevel)
                            {
                                case ImportanceLevel.CRITIC:
                                    Code = ServiceCode.SURVEY_ANSWER_CRITIC;
                                    break;
                                case ImportanceLevel.IMPORTANT:
                                    if (Code != ServiceCode.SURVEY_ANSWER_CRITIC)
                                        Code = ServiceCode.SURVEY_ANSWER_IMPORTANT;
                                    break;
                                case ImportanceLevel.FAIL:
                                    if (Code != ServiceCode.SURVEY_ANSWER_CRITIC && Code != ServiceCode.SURVEY_ANSWER_IMPORTANT)
                                        Code = ServiceCode.SURVEY_ANSWER_FAIL;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    eventHelper.FaultStart();
                    Message = survey.SurveyId.ToString();


                    return false;

                }


                eventHelper.RelayEnd();

                return true;
            }
            catch (Exception e)
            {
                Message = e.ToString();
            }
            return false;

        }


    }
}