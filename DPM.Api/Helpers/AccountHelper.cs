﻿using DPM.Api.Properties;
using DPM.Model;
using DPM.Model.ApiModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using static DPM.Api.Constants.Enums;
using static DPM.Model.ApiModels.AccountModel;

namespace DPM.Api.Helpers
{
    public class AccountHelper : MainHelper
    {

        public AccountHelper()
        {
            Code = ServiceCode.OK;
            db = new DubaiPortDB();
        }

        public AccountModel.LoginResult Login(LoginRequest model)
        {
            LoginResult loginResult = new LoginResult();

            string url = Settings.Default.ZodiacLoginUrl;
            url += $"?email={model.Email}&password={model.Password}";

            //var device = db.Devices
            //       .Where(w => w.MacAddress == model.MacAddress
            //       && w.Status == (byte)GeneralStatus.ACTIVE)
            //       .FirstOrDefault();

            User user;
            bool userCheck=db.Users.Where(w => w.Email == model.Email && w.Password == model.Password && w.Role == 1).Any();

            if (LoginFromAPI(model) && model.VehicleId != 0 && userCheck == true)//zodiac request
            {
                user = new User();

                Vehicle currentVehicle = db.Vehicles.Where(w => w.VehicleId == model.VehicleId).FirstOrDefault();

                if (currentVehicle.CurrentState == (byte)EventLogType.FAULT_STARTED)//Araç Arıza bakımda ise
                {
                    Code = ServiceCode.VEHICLE_IN_FAULT_PROCESS;

                    return loginResult;
                }



               /* if (!db.Users.Where(w => w.Email == model.Email && w.Password == model.Password).Any())//User DB de kayıtlı değilse local data olarak ekle
                {

                    //Code = ServiceCode.NOT_FOUND;

                    //return loginResult;

                    user = db.Users.Add(new User
                    {
                        FirstName = model.Email,
                        LastName = model.Email,
                        Email = model.Email,
                        Password = model.Password,
                        CurrentVehicleId = model.VehicleId,
                    });

                    DbSavechanges();
                }
                else
                {*/
                    user = db.Users
                            .Where(w => w.Email == model.Email
                             && w.Password == model.Password)
                            .FirstOrDefault();
             //   }

                if (user.CurrentState != (byte)EventLogType.PASSIVE && user.CurrentState != (byte)EventLogType.LOGOUT)//State'i bu durumlarda değilse relay,break,arıza durumundadır.
                {
                    Code = ServiceCode.ANOTHER_SESSION_CONTINUED;
                    loginResult.CurrentEvent = user.Event.Description;

                    return loginResult;
                }

                UserToken userToken = db.UserTokens.Where(w => w.UserId == user.UserId).FirstOrDefault();

                string newToken = CreateToken();

                if (userToken == null)
                {
                    db.UserTokens.Add(new UserToken
                    {
                        TokenValue = newToken,
                        UserId = user.UserId,

                    });
                }
                else
                {
                    userToken.TokenValue = newToken;
                }

                user.CurrentVehicleId = model.VehicleId;

                UserVehicleRelationship userVehicle = db.UserVehicleRelationships
                    .Where(w => w.UserId == user.UserId && w.VehicleId == model.VehicleId)
                    .FirstOrDefault();

                if (userVehicle == null)
                {
                    db.UserVehicleRelationships.Add(new UserVehicleRelationship
                    {
                        UserId = user.UserId,
                        VehicleId = model.VehicleId,
                    });

                }

                DbSavechanges();


                loginResult.Token = newToken;

                EventLogHelper.AddEventLog(EventLogType.LOGIN, user.UserId, user.CurrentVehicleId);
            }
            else if (!LoginFromAPI(model))
            {
                Code = ServiceCode.NOT_FOUND;
            }
            else
            {
                Code = ServiceCode.INSTALL_REQUİRED;
                Message = "Araç için kurulum gerekli lütfen yöneticiyle irtibata geçiniz";
            }

            return loginResult;
        }

        public void Logout(string token)
        {
            var tokenUser = db.UserTokens.Where(w => w.TokenValue == token).Include(i => i.User).FirstOrDefault();

            if (tokenUser == null)
            {
                Code = ServiceCode.NOT_FOUND;
            }
            else
            {
                EventHelper eventHelper = new EventHelper(tokenUser.UserId);

                eventHelper.EventEnd();

                tokenUser.TokenValue = null;
                DbSavechanges();
            }
        }

        public void AdminLogout(string token)
        {
            var tokenUser = db.UserTokens.Where(w => w.TokenValue == token).Include(i => i.User).FirstOrDefault();

            if (tokenUser == null)
            {
                Code = ServiceCode.NOT_FOUND;
            }
            else
            {
                tokenUser.TokenValue = null;
                DbSavechanges();

                tokenUser.User.CurrentState = (byte)EventLogType.LOGOUT;

                DbSavechanges();
            }
        }

        public AccountModel.LoginResult AdminLogin(LoginRequest model)
        {
            LoginResult loginResult = new LoginResult();
            User user = new User();

            if (!db.Users.Where(w => w.Email == model.Email && w.Password == model.Password && (w.Role == 2 || w.Role == 3)).Any())//User DB de kayıtlı değilse local data olarak ekle
            {
                Code = ServiceCode.NOT_FOUND;

                return loginResult;
            }

            user = db.Users
                    .Where(w => w.Email == model.Email
                     && w.Password == model.Password
                     && (w.Role == 2 || w.Role == 3))
                    .FirstOrDefault();

            //if (user.CurrentState != (byte)EventLogType.PASSIVE && user.CurrentState != (byte)EventLogType.LOGOUT)//State'i bu durumlarda değilse relay,break,arıza durumundadır.
            //{
            //    Code = ServiceCode.ANOTHER_SESSION_CONTINUED;
            //    loginResult.CurrentEvent = user.Event.Description;

            //    return loginResult;
            //}

            UserToken userToken = db.UserTokens.Where(w => w.UserId == user.UserId).FirstOrDefault();

            string newToken = CreateToken();

            if (userToken == null)
            {
                db.UserTokens.Add(new UserToken
                {
                    TokenValue = newToken,
                    UserId = user.UserId,

                });
            }
            else
            {
                userToken.TokenValue = newToken;
            }

            user.CurrentState = (int)EventLogType.LOGIN;
            DbSavechanges();

            loginResult.Token = newToken;

            return loginResult;
        }

        private static bool LoginFromAPI(LoginRequest model)
        {
            bool result = false;

            //using (WebClient client = new WebClient())
            //{
            //    string json = client.DownloadString(url);

            //    result = new JavaScriptSerializer().Deserialize<bool>(json);
            //}

            string ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList.Where(o => o.AddressFamily == AddressFamily.InterNetwork).First().ToString();

            //var client = new RestClient("http://uat-app/ZodiacWebService/ZodiacService.asmx/serviceRequestWithMessageType");
            var client = new RestClient("http://zodiacapp01/ZodiacWebService/ZodiacService.asmx/serviceRequestWithMessageType");
             
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", "serviceName=\"SECURITY_SVC\"&" +
                "messageType=\"Authenticate\"&" +
                "message=\"<request>" +
                "<reference></reference>" +
                "<user>" + model.Email + "/c</user>" +
                "<company>TEST</company>" +
                "<locale>en-US</locale>" +
                "<msg_body>" +
                "<query>" +
                "<userid>" + model.Email + "</userid>" +
                "<password>" + model.Password + "</password>" +
                "<ip>" + ip + "</ip>" +
                "</query>" +
                "</msg_body>" +
                "</request>\"", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            return result;
        }

        public bool CheckMacAdress(string address)
        {
            return db.Devices.Where(w => w.MacAddress == address && w.Status == (byte)GeneralStatus.ACTIVE).Any();
        }

        public void AddMacAddress(MacAddressAddModel model)
        {
            var admin = db.Users.Where(w => w.Email == model.Email && w.Password == model.Password).FirstOrDefault();

            if (admin != null)
            {
                var device = db.Devices.Where(w => w.MacAddress == model.MacAddress && w.Status == (byte)GeneralStatus.ACTIVE).FirstOrDefault();

                if (device != null)
                {
                    db.DeviceChangeLogs.Add(new DeviceChangeLog
                    {
                        OldVehicleType = device.VehicleId,
                        DeviceId = device.DeviceId,
                        LogDate = DateTime.Now
                    });

                    DbSavechanges();

                    device.VehicleId = model.VehicleId;

                }
                else
                {
                    db.Devices.Add(new Device
                    {
                        MacAddress = model.MacAddress,
                        VehicleId = model.VehicleId,
                        CreatedDate = DateTime.Now
                    });

                }

                DbSavechanges();
            }
            else
            {
                Code = ServiceCode.UNAUTHORIZED_ACCESS;
                Message = "Yetkisiz Giriş";
            }

        }

        public bool CheckVehicleFault(string macAddress)
        {
            var device = db.Devices.Where(w => w.MacAddress == macAddress).FirstOrDefault();

            if (device == null)
                return false;

            if (device.Vehicle.CurrentState != (int)EventLogType.FAULT_STARTED)
                return true;

            return false;
        }

        /// <summary>
        /// A kategorisinde hayır seçildi
        /// </summary>
        /// <param name="surveyId"></param>
        public void CallSuperVisor(int surveyId)
        {
            var options = db.Options.ToList();

            var mailEnabled = options.Where(w => w.Key == "ACategoryMail").FirstOrDefault().Value == "1" ? true : false;
            var faultEnabled = options.Where(w => w.Key == "ACategoryFault").FirstOrDefault().Value == "1" ? true : false;

            if (mailEnabled)
            {
                EmailSender emailSender = new EmailSender();

                emailSender.SendMail(new List<string>() { Settings.Default.SuperVisorEmail }, new List<string>() { }, "Operatör Arıza Bildirimi", GetLastFailSurveyInfo(surveyId));
            }

            if (!mailEnabled && !faultEnabled)
            {
                Code = ServiceCode.SURVEY_FAULT_NOT_START_AND_MAIL_NOT_SENT;
            }
            else if (!mailEnabled)
            {
                Code = ServiceCode.SURVEY_MAIL_NOT_SENT;
            }
            else if (!faultEnabled)
            {
                Code = ServiceCode.SURVEY_FAULT_NOT_START;
            }
            else
            {
                Code = ServiceCode.SURVEY_ANSWER_CRITIC;
            }

        }

        /// <summary>
        /// C Kategorisinde hayır seçildi
        /// </summary>
        /// <param name="surveyId"></param>
        public void CallMobileVisor(int surveyId)
        {
            var options = db.Options.ToList();

            var mailEnabled = options.Where(w => w.Key == "BCategoryMail").FirstOrDefault().Value == "1" ? true : false;
            var faultEnabled = options.Where(w => w.Key == "BCategoryFault").FirstOrDefault().Value == "1" ? true : false;

            if (!mailEnabled)
            {
                EmailSender emailSender = new EmailSender();

                emailSender.SendMail(new List<string>() { Settings.Default.MobileVisorEmail, Settings.Default.MobileVisorEmail2 }, new List<string>() { }, "Operatör Arıza Bildirimi", GetLastFailSurveyInfo(surveyId));
            }

            if (!mailEnabled && !faultEnabled)
            {
                Code = ServiceCode.SURVEY_FAULT_NOT_START_AND_MAIL_NOT_SENT;
            }
            else if (!mailEnabled)
            {
                Code = ServiceCode.SURVEY_MAIL_NOT_SENT;
            }
            else if (!faultEnabled)
            {
                Code = ServiceCode.SURVEY_FAULT_NOT_START;
            }
            else
            {
                Code = ServiceCode.SURVEY_ANSWER_FAIL;
            }

        }

        /// <summary>
        /// B kategorisinde hayır seçildi
        /// </summary>
        /// <param name="surveyId"></param>
        public void CallChief(int surveyId)
        {
            var options = db.Options.ToList();

            var mailEnabled = options.Where(w => w.Key == "BCategoryMail").FirstOrDefault().Value == "1" ? true : false;
            var faultEnabled = options.Where(w => w.Key == "BCategoryFault").FirstOrDefault().Value == "1" ? true : false;

            if (mailEnabled)
            {
                EmailSender emailSender = new EmailSender();

                emailSender.SendMail(new List<string>() { Settings.Default.ChiefEmail }, new List<string>() { }, "Operatör Arıza Bildirimi", GetLastFailSurveyInfo(surveyId));
            }

            if (!mailEnabled && !faultEnabled)
            {
                Code = ServiceCode.SURVEY_FAULT_NOT_START_AND_MAIL_NOT_SENT;
            }
            else if (!mailEnabled)
            {
                Code = ServiceCode.SURVEY_MAIL_NOT_SENT;
            }
            else if (!faultEnabled)
            {
                Code = ServiceCode.SURVEY_FAULT_NOT_START;
            }
            else
            {
                Code = ServiceCode.SURVEY_ANSWER_IMPORTANT;
            }

        }

        public object GetVehicle(int itemId)
        {
            Vehicle vehicle = db.Vehicles.Where(w => w.ItemId == itemId).FirstOrDefault();

            if (vehicle == null)
            {
                Code = ServiceCode.NOT_FOUND;

                return new { };
            }

            return new { VehicleId = vehicle.VehicleId, Name = vehicle.Name };
        }

        private List<string> GetLastFailSurveyInfo(int surveyId)
        {
            List<string> resultMessage = new List<string>();

            Survey lastSurvey = db.Surveys.Where(w => w.SurveyId == surveyId)
                .Include(i => i.UserVehicleRelationship.Vehicle)
                .Include(i => i.UserVehicleRelationship.User)
                .FirstOrDefault();

            List<SurveyAnswer> survey = db.SurveyAnswers.Where(w => w.SurveyId == lastSurvey.SurveyId && !w.Answer)
                .Include(i => i.Question.QuestionVehicleRelationships)
                .ToList();

            SurveyNote message = db.SurveyNotes.Where(w => w.SurveyId == surveyId).FirstOrDefault();

            resultMessage.Add($"{lastSurvey.UserVehicleRelationship.User.FirstName} {lastSurvey.UserVehicleRelationship.User.FirstName} isimli operatör tarafından {lastSurvey.UserVehicleRelationship.Vehicle.Name } aracında aşağıdaki sorunlar bildirilmiştir.");

            if (message != null)
            {
                resultMessage.Add($"{message.Note}");
            }

            ImportanceLevel importanceLevel;


            foreach (var answer in survey)
            {
                var category = answer.Question
                    .QuestionVehicleRelationships
                    .Where(w => w.VehicleTypeId == lastSurvey.UserVehicleRelationship.Vehicle.VehicleTypeId)
                    .Select(s => s.Category)
                    .FirstOrDefault();

                if (Enum.TryParse(category.ImportanceLevel.ToString(), out importanceLevel))
                {
                    resultMessage.Add($"{category.Code} bölümünde {answer.Question.Text} sorusuna hayır cevabı verildi");
                }
            }

            return resultMessage;
        }

        public string GetUserName(string token)
        {
            string userName = string.Empty;
            var userToken = db.UserTokens.Where(w => w.TokenValue == token).Include(i => i.User).FirstOrDefault();

            if (userToken != null)
                userName = $"{userToken.User.FirstName} {userToken.User.LastName}";

            return userName;
        }
        public string GetUserRole(string token)
        {
            string userRole = string.Empty;
            var userToken = db.UserTokens.Where(w => w.TokenValue == token).Include(i => i.User).FirstOrDefault();

            if (userToken != null) { 
                userRole = userToken.User.Role.ToString();
                
            }

            return userRole;
        }

        public bool CloseSession(string email, string password)
        {
            var user = db.Users
                .Where(w => w.Role == 1
                && w.Status == (byte)GeneralStatus.ACTIVE
                && w.Email == email
                && w.Password == password)
                .FirstOrDefault();

            if (user.CurrentState % 2 == 0)//Bitirilmiş bir event ise direk logout setle
            {
                user.CurrentState = (int)EventLogType.LOGOUT;

                db.SaveChanges();

                Code = ServiceCode.OK;
            }
            else//Bitmemiş bir eventsa bitirir
            {
                var eventHelper = new EventHelper(user.UserId);

                eventHelper.EventEnd();

                Code = eventHelper.Code;
            }

            return Code == ServiceCode.OK ? true : false;
        }
    }
}