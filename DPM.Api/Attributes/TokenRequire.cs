﻿using DPM.Api.Models;
using DPM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using static DPM.Api.Constants.Enums;

namespace DPM.Api.Attributes
{
    public class TokenRequire: ActionFilterAttribute
    {
        private void EndFailResult(HttpActionContext actionContext, ServiceCode resultCode)
        {
            var result = new ResponseResult<string>();
            result.Code = resultCode;
            result.Message = result.Code.ToString();
            actionContext.Response = actionContext.Request.CreateResponse(
                System.Net.HttpStatusCode.Forbidden,
                result,
                actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string token = HttpContext.Current.Request["Token"];

            if (string.IsNullOrEmpty(token))
            {
                EndFailResult(actionContext, ServiceCode.TOKEN_REQUIRED);
                return;
            }

            User user = new User();

            using (DubaiPortDB db= new DubaiPortDB())
            {
                var userToken = db.UserTokens.Where(w => w.TokenValue == token).FirstOrDefault();

                user = userToken?.User;
            }

            if(!string.IsNullOrEmpty(token) && user == null)
            {
                EndFailResult(actionContext, ServiceCode.TOKEN_EXPIRED);
                return;
            }

            if (user == null)
            {
                EndFailResult(actionContext, ServiceCode.NOT_FOUND);
                return;
            }

            base.OnActionExecuting(actionContext);
        }
    }
}