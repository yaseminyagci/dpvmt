﻿using DPM.Api.Attributes;
using DPM.Api.Helpers;
using DPM.Api.Models;
using DPM.Model.ApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DPM.Api.Controllers
{
    [TokenRequire]
    public class PageController : BaseController
    {
        
        public ResponseResult<PageModel.DetailPageModel> GetDetailPage()
        {
            var result = new ResponseResult<PageModel.DetailPageModel>(CurrentUser);

            var helper = new PageHelper(CurrentUser.UserId);

            result.Data = helper.GetDetailPage();

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }
    }
}