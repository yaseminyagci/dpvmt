﻿using DPM.Api.Helpers;
using DPM.Api.Models;
using DPM.Model.ApiModels;

using System.Collections.Generic;
using System.Web.Http;
using static DPM.Api.Constants.Enums;
using static DPM.Model.ApiModels.LogModels;
using static DPM.Model.ApiModels.PageModel;
using static DPM.Model.ApiModels.QuestionModel;
using static DPM.Model.ApiModels.VehicleModel;


namespace DPM.Api.Controllers
{
    public class AdminController : BaseController
    {

        [HttpPost]
        public ResponseResult<bool> AddVehicle([FromBody]VehiclePostModel model)
        {
            var result = new ResponseResult<bool>();

            var helper = new VehicleHelper();
            helper.AddVehicle(model);

            result.Data = helper.Code == ServiceCode.OK ? true : false;
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpPost]
        public ResponseResult<bool> AddQuestion([FromBody]QuestionAddPostModel model)
        {
            var result = new ResponseResult<bool>();

            var helper = new QuestionHelper();

            helper.AddQuestion(model);

            result.Data = helper.Code == ServiceCode.OK ? true : false;
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        public ResponseResult<bool> EditQuestion(GetQuestionModel model)
        {
            var result = new ResponseResult<bool>();

            var helper = new QuestionHelper();

            helper.EditQuestion(model);

            result.Data = helper.Code == ServiceCode.OK ? true : false;
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<List<AdminQuestionModel>> GetAllQuestions()
        {
            var result = new ResponseResult<List<AdminQuestionModel>>();

            var helper = new QuestionHelper();

            result.Data = helper.GetQuestionsForAdmin();
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<GetQuestionModel> GetQuestionById(int id)
        {
            var result = new ResponseResult<GetQuestionModel>();

            var helper = new QuestionHelper();

            result.Data = helper.GetQuestionById(id);
            result.Code = ServiceCode.OK;

            return result;
        }

        [HttpGet]
        public ResponseResult<bool> RemoveQuestion(int questionId)
        {
            var result = new ResponseResult<bool>();

            var helper = new QuestionHelper();

            result.Data = helper.RemoveQuestion(questionId);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        //public ResponseResult<List<AdminVehiclePostModel>> GetAllVehicles()
        //{
        //    var result = new ResponseResult<List<AdminVehiclePostModel>>();

        //    var helper = new VehicleHelper();

        //    result.Data = helper.GetVehiclesForAdmin();

        //    result.Code = helper.Code;
        //    result.Message = helper.Message;

        //    return result;
        //}

        [HttpGet]
        public ResponseResult<bool> RemoveVehicle(int vehicleId)
        {
            var result = new ResponseResult<bool>();

            var helper = new VehicleHelper();

            result.Data = helper.RemoveVehicle(vehicleId);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public DataTableModel<AdminVehicleStateModel> GetVehicleStates()
        {
            var result = new DataTableModel<AdminVehicleStateModel>();

            var helper = new VehicleHelper();

            result = helper.GetVehicleStatesForAdmin();


            return result;
        }

        [HttpGet]
        public DataTableModel<UserLogModel> GetUserLog(int[] userIds)
        {
            var result = new DataTableModel<UserLogModel>();

            result = EventLogHelper.GetUserLogs(userIds);

            return result;
        }

        [HttpGet]
        public ResponseResult<List<UserLogModel>> GetVehicleLog(int[] vehicleIds)
        {
            var result = new ResponseResult<List<UserLogModel>>();

            result.Data = EventLogHelper.GetVehicleLogs(vehicleIds);

            result.Code = ServiceCode.OK;

            return result;
        }

        [HttpGet]
        public DataTableModel<UserLogModel> GetVehicleLogDataTable(int vehicleId)
        {
            var result = new DataTableModel<UserLogModel>();

            result.data = EventLogHelper.GetVehicleLogs(new int[] { vehicleId });

            return result;
        }

        [HttpGet]
        public ResponseResult<List<VehicleTypeForAdminModel>> GetVehicleType()
        {
            var result = new ResponseResult<List<VehicleTypeForAdminModel>>();

            var helper = new VehicleHelper();

            result.Data = helper.GetVehicleTypes();

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<List<AdminVehicleModel>> GetVehicles(int vehicleTypeId)
        {
            var result = new ResponseResult<List<AdminVehicleModel>>();

            var helper = new VehicleHelper();

            result.Data = helper.GetVehicles(vehicleTypeId);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<List<AdminVehicleModel>> GetAllVehicles()
        {
            var result = new ResponseResult<List<AdminVehicleModel>>();

            var helper = new VehicleHelper();

            result.Data = helper.GetAllVehicles();

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }
        /*User CRUD*/
        [HttpGet]
        public ResponseResult<List<UserModel>> GetAllUsers()
        {
            var result = new ResponseResult<List<UserModel>>();

            var helper = new UserHelper();

            result.Data = helper.GetUsers();

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }
        [HttpGet]
        public ResponseResult<UserModel> GetUserById(int id)
        {
            var result = new ResponseResult<UserModel>();

            var helper = new UserHelper();

            result.Data = helper.GetUser(id);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }
        [HttpPost]
        public ResponseResult<bool> AddUser([FromBody]UserModel model)
        {
            var result = new ResponseResult<bool>();

            var helper = new UserHelper();
            helper.AddUser(model);

            result.Data = helper.Code == ServiceCode.OK ? true : false;
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }
        [HttpGet]
        public ResponseResult<bool> RemoveUser(int userId)
        {
            var result = new ResponseResult<bool>();

            var helper = new UserHelper();

            result.Data = helper.RemoveUser(userId);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpPost]
        public ResponseResult<bool> UpdateUser([FromBody]UserModel model)
        {
            var result = new ResponseResult<bool>();

            var helper = new UserHelper();
            helper.UpdateUser(model);

            result.Data = helper.Code == ServiceCode.OK ? true : false;
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }
        /*User CRUD*/
        [HttpGet]
        public ResponseResult<AdminModel.Buttons> CheckButtons()
        {
            var result = new ResponseResult<AdminModel.Buttons>();

            var helper = new PageHelper(CurrentUser.UserId);

            result.Data = helper.CheckButtons();
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpPost]
        public ResponseResult<AdminModel.Buttons> ChangeButtons(AdminModel.Buttons buttons)
        {
            var result = new ResponseResult<AdminModel.Buttons>();

            var helper = new PageHelper(CurrentUser.UserId);

            result.Data = helper.ChangeButtons(buttons);
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<AdminModel.NotificationOptions> CheckSurveyOptions()
        {
            var result = new ResponseResult<AdminModel.NotificationOptions>();

            var helper = new PageHelper(CurrentUser.UserId);

            result.Data = helper.CheckNotificationOptions();
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpPost]
        public ResponseResult<AdminModel.NotificationOptions> ChangeSurveyOptions(AdminModel.NotificationOptions options)
        {
            var result = new ResponseResult<AdminModel.NotificationOptions>();

            var helper = new PageHelper(CurrentUser.UserId);

            result.Data = helper.ChangeNotificationOptions(options);
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<ChartModel> GetChart(int vehicleTypeId)
        {
            var result = new ResponseResult<ChartModel>();

            var helper = new PageHelper(CurrentUser.UserId);

            result.Data = helper.GetChart(vehicleTypeId);
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<bool> SetFault(int vehicleId)
        {
            var result = new ResponseResult<bool>();

            var helper = new PageHelper(CurrentUser.UserId);

            result.Data = helper.SetFault(vehicleId);
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<bool> RemoveFault(int vehicleId)
        {
            var result = new ResponseResult<bool>();

            var helper = new PageHelper(CurrentUser.UserId);

            result.Data = helper.RemoveFault(vehicleId);
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<bool> AdminLogout(string Token)
        {
            var result = new ResponseResult<bool>();

            var helper = new AccountHelper();

            helper.AdminLogout(Token);

            result.Data = helper.Code == ServiceCode.OK ? true : false;
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }
    }
}
