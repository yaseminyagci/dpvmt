﻿using DPM.Api.Attributes;
using DPM.Api.Helpers;
using DPM.Api.Models;
using DPM.Model.ApiModels;
using System.Collections.Generic;
using System.Web.Http;
using static DPM.Api.Constants.Enums;

namespace DPM.Api.Controllers
{
    [TokenRequire]
    public class SurveyController : BaseController
    {
        [System.Web.Http.HttpPost]
        public ResponseResult<bool> AddSurvey([FromBody]SurveyModel.AnswerPostModel model)
        {

            var result = new ResponseResult<bool>(CurrentUser);

            var helper = new SurveyHelper(CurrentUser.UserId);

            result.Data = helper.AddSurvey(model);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;

        }

        [System.Web.Http.HttpPost]
        public ResponseResult<bool> AddSurveyEnd([FromBody]SurveyModel.AnswerPostModel model)
        {

            var result = new ResponseResult<bool>(CurrentUser);

            var helper = new SurveyHelper(CurrentUser.UserId);

            result.Data = helper.AddSurveyEnd(model);

            result.Code = helper.Code;
            result.Message = helper.Message;

            MainHelper.ClearSession(CurrentUser);

            return result;

        }

        public ResponseResult<List<SurveyModel.SurveyHistory>> GetSurveyHistory()
        {
            var result = new ResponseResult<List<SurveyModel.SurveyHistory>>(CurrentUser);

            if (CurrentUser.CurrentState == (int)EventLogType.LOGOUT)
            {
                var helper = new SurveyHelper(CurrentUser.UserId);

                result.Data = helper.GetUserSurveyHistory();

                result.Code = helper.Code;
                result.Message = helper.Message;
            }
            else
            {
                result.Code = ServiceCode.ANOTHER_SESSION_CONTINUED;
                result.Message = "Kullanıcı vardiyayı tamamlamadı";
            }
            

            return result;
        }
        
    }
}