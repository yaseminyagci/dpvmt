﻿using DPM.Api.Attributes;
using DPM.Api.Helpers;
using DPM.Api.Models;
using DPM.Model.ApiModels;
using System.Collections.Generic;
using System.Web.Mvc;
using static DPM.Model.ApiModels.LogModels;
using static DPM.Model.ApiModels.QuestionModel;

namespace DPM.Api.Controllers
{
    [TokenRequire]
    public class EventController : BaseController
    {
        public EventHelper eventHelper { get; set; }

        [HttpGet]
        public ResponseResult<PageModel.EventResponseModel> GetCurrentEvent()
        {
            eventHelper = new EventHelper(CurrentUser.UserId);


            var result = new ResponseResult<PageModel.EventResponseModel>(CurrentUser);

            PageModel.EventResponseModel resp = new PageModel.EventResponseModel();
            resp.CurrentEvent = eventHelper.GetCurrentEvent();
            resp.EventId = (int)eventHelper.CurrentEvent;

            result.Data = resp;
            result.Code = eventHelper.Code;
            result.Message = eventHelper.Message;

            return result;
        }

        #region EventAction

        [HttpPost]
        public ResponseResult<string> BreakStart()//Mola Başlat
        {
            eventHelper = new EventHelper(CurrentUser.UserId);

            var result = new ResponseResult<string>(CurrentUser);

            result.Data = eventHelper.BreakStart();
            result.Code = eventHelper.Code;
            result.Message = eventHelper.Message;

            return result;
        }

        [HttpPost]
        public ResponseResult<string> FaultStart()//Arıza Başlat
        {
            eventHelper = new EventHelper(CurrentUser.UserId);

            var result = new ResponseResult<string>(CurrentUser);

            result.Data = eventHelper.FaultStart();
            result.Code = eventHelper.Code;
            result.Message = eventHelper.Message;

            return result;
        }

        [HttpPost]
        public ResponseResult<string> RelayStart()//Vardiya Başlat
        {
            eventHelper = new EventHelper(CurrentUser.UserId);

            var result = new ResponseResult<string>(CurrentUser);

            result.Data = eventHelper.RelayStart();
            result.Code = eventHelper.Code;
            result.Message = eventHelper.Message;

            return result;
        }

        [HttpPost]
        public ResponseResult<string> MealStart()//Yemek Molası Başla
        {
            eventHelper = new EventHelper(CurrentUser.UserId);

            var result = new ResponseResult<string>(CurrentUser);

            result.Data = eventHelper.MealStart();
            result.Code = eventHelper.Code;
            result.Message = eventHelper.Message;

            return result;
        }

        public ResponseResult<EventEndQuestionModel> EventEnd()//Başlamış eventi bitirir
        {
            eventHelper = new EventHelper(CurrentUser.UserId);

            var result = new ResponseResult<EventEndQuestionModel>(CurrentUser);

            result.Data = eventHelper.EventEnd();
            result.Code = eventHelper.Code;
            result.Message = eventHelper.Message;

            return result;
        }

        [HttpGet]
        public DataTableModel<UserLogModel> GetUserLogs()
        {
            var result = new DataTableModel<UserLogModel>();

            var eventLogHelper = new EventLogHelper();

            result = EventLogHelper.GetUserLogs(new int[] { CurrentUser.UserId });

            return result;
        }

        #endregion
    }
}