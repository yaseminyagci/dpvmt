﻿using DPM.Api.Helpers;
using DPM.Api.Models;
using DPM.Model.ApiModels;
using System.Collections.Generic;
using System.Web.Http;
using static DPM.Model.ApiModels.VehicleModel;

namespace DPM.Api.Controllers
{
    public class VehicleController : BaseController
    {
        [HttpGet]
        public ResponseResult<List<VehicleGetModel>> GetVehicles()
        {
            var result = new ResponseResult<List<VehicleGetModel>>(CurrentUser);

            var helper = new VehicleHelper();

            result.Data = helper.GetVehicles();
            result.Code = helper.Code;

            return result;
        }

        //public ResponseResult<bool> AddVehicle(VehiclePostModel model)
        //{
        //    var result = new ResponseResult<bool>(CurrentUser);

        //    var helper = new VehicleHelper();

        //    helper.AddVehicle(model);

        //    result.Code = helper.Code;

        //    return result;
        //}

        public ResponseResult<bool> AddVehicleType(VehicleTypePostModel model)
        {
            var result = new ResponseResult<bool>(CurrentUser);

            var helper = new VehicleHelper();

            helper.AddVehicleType(model);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;

        }

        public ResponseResult<bool> AddMacAddress(AccountModel.MacAddressAddModel model)
        {
            var result = new ResponseResult<bool>();

            var helper = new AccountHelper();

            helper.AddMacAddress(model);

            result.Data = helper.Code == Constants.Enums.ServiceCode.OK ? true : false;
            result.Code = helper.Code;
            result.Message = helper.Message;


            return result;
        }

        public ResponseResult<bool> CheckMacAddress(string macAddress)
        {
            var result = new ResponseResult<bool>();

            var helper = new AccountHelper();

            result.Data = helper.CheckMacAdress(macAddress);
            result.Code = helper.Code;
            result.Message = helper.Message;


            return result;
        }
        
    }
}