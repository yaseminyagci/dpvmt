﻿using DPM.Api.Attributes;
using DPM.Api.Constants;
using DPM.Api.Helpers;
using DPM.Api.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using static DPM.Model.ApiModels.QuestionModel;

namespace DPM.Api.Controllers
{
    [TokenRequire]
    public class QuestionController : BaseController
    {

        [HttpGet]
        public ResponseResult<QuestionReturnModel> GetQuestions()
        {
            var result = new ResponseResult<QuestionReturnModel>(CurrentUser);

            var helper = new QuestionHelper(CurrentUser.CurrentVehicleId.Value);

            result.Data = helper.GetQuestions(CurrentUser.UserId, CurrentUser.CurrentVehicleId.Value,CurrentUser.CurrentState.ToGroupType());
            result.Code = helper.Code;

            return result;
        }

        //[HttpPost]
        //public ResponseResult<bool> AddQuestions(List<QuestionAddPostModel> model)
        //{
        //    var result = new ResponseResult<bool>(CurrentUser);

        //    var helper = new QuestionHelper();

        //    helper.AddQuestion(model);

        //    result.Data = helper.Code == Constants.Enums.ServiceCode.OK ? true : false;
        //    result.Code = helper.Code;

        //    return result;
        //}
    }
}