﻿using DPM.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace DPM.Api.Controllers
{
    public class BaseController : ApiController
    {
        public string CurrentToken { get; set; }
        public User CurrentUser { get; set; }

        public override Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
        {
            using (DubaiPortDB db = new DubaiPortDB())
            {

                var userGuid = HttpContext.Current.Request["Token"];

                var userToken = db.UserTokens
                    .Where(w => w.TokenValue == userGuid)
                    .FirstOrDefault();

                if (userToken != null)
                {
                    CurrentToken = userGuid;
                    CurrentUser = db.Users
                        .Where(w => w.UserId == userToken.UserId)
                        .Include(i=>i.Event)
                        .FirstOrDefault();
                    
                }
            }

            return base.ExecuteAsync(controllerContext, cancellationToken);
        }
    }
}
