﻿using DPM.Api.Helpers;
using DPM.Api.Models;
using DPM.Model;
using DPM.Model.ApiModels;
using System.Web.Http;

namespace DPM.Api.Controllers
{
    public class UserController : ApiController
    {
        [HttpPost]
        public ResponseResult<AccountModel.LoginResult> Login([FromBody]AccountModel.LoginRequest authModel)
        {
            var result = new ResponseResult<AccountModel.LoginResult>();

            var helper = new AccountHelper();

            result.Data = helper.Login(authModel);
            result.Code = helper.Code;
            result.Message = helper.Message;

            //if (!string.IsNullOrEmpty(result.Data.Token))
            //    CurrentToken = result.Data.Token;

            return result;
        }

        [HttpPost]
        public ResponseResult<bool> Logout([FromBody]string Token)
        {
            var result = new ResponseResult<bool>();

            var helper = new AccountHelper();

            helper.Logout(Token);

            result.Data = helper.Code == Constants.Enums.ServiceCode.OK ? true : false;
            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<bool> CheckFault(string macAddress)
        {

            var result = new ResponseResult<bool>();
            var helper = new AccountHelper();

            result.Data = helper.CheckVehicleFault(macAddress);
            result.Code = helper.Code;
            result.Message = helper.Message;


            return result;
        }

        [HttpPost]
        public ResponseResult<AccountModel.LoginResult> AdminLogin([FromBody]AccountModel.LoginRequest authModel)
        {
            var result = new ResponseResult<AccountModel.LoginResult>();

            var helper = new AccountHelper();

            result.Data = helper.AdminLogin(authModel);
            result.Code = helper.Code;
            result.Message = helper.Message;

            //if (!string.IsNullOrEmpty(result.Data.Token))
            //    CurrentToken = result.Data.Token;

            return result;
        }


        //public ResponseResult<bool> Logout()
        //{
        //    var result = new ResponseResult<bool>();

        //    var helper = new AccountHelper<bool>();

        //    string token = HttpContext.Current.Request["Token"];

        //    result.Data = helper.Logout(token);
        //    result.Code = helper.Result.Code;

        //    return result;
        //}

        /// <summary>
        /// A kategorisinde seçim için bu method çalışır
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpPost]
        public ResponseResult<bool> CallSuperVisor(int surveyId)
        {

            var result = new ResponseResult<bool>();

            var helper = new AccountHelper();

            helper.CallSuperVisor(surveyId);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;

        }

        /// <summary>
        /// B kategorisinde seçilen hayır seçimi için bu method çalışır
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpPost]
        public ResponseResult<bool> CallChief(int surveyId)
        {

            var result = new ResponseResult<bool>();

            var helper = new AccountHelper();

            helper.CallChief(surveyId);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;

        }

        /// <summary>
        /// C kategorisinde yapılan seçim için bu metod çalışr
        /// </summary>
        /// <param name="surveyId"></param>
        /// <returns></returns>
        [HttpPost]
        public ResponseResult<bool> CallMobileVisor(int surveyId)
        {

            var result = new ResponseResult<bool>();

            var helper = new AccountHelper();

            helper.CallMobileVisor(surveyId);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;

        }

        [HttpGet]
        public ResponseResult<object> GetVehicle(int itemId)
        {
            var result = new ResponseResult<object>();

            var helper = new AccountHelper();

            result.Data = helper.GetVehicle(itemId);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<string> GetUserName(string Token)
        {
            var result = new ResponseResult<string>();

            var helper = new AccountHelper();

            result.Data = helper.GetUserName(Token);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<string> GetUserRole(string Token)
        {
            var result = new ResponseResult<string>();

            var helper = new AccountHelper();

            result.Data = helper.GetUserRole(Token).ToString();

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }

        [HttpGet]
        public ResponseResult<bool> CloseSession(string email, string password)
        {
            var result = new ResponseResult<bool>();

            var helper = new AccountHelper();

            result.Data = helper.CloseSession(email,password);

            result.Code = helper.Code;
            result.Message = helper.Message;

            return result;
        }
    }
}
