﻿namespace DPM.Api.Constants
{
    public class Enums
    {
        public enum ServiceCode
        {
            OK = 200,
            USER_IN_PROCESS = 201,
            ANOTHER_SESSION_CONTINUED = 202,
            UNAUTHORIZED_ACCESS = 203,
            VEHICLE_IN_FAULT_PROCESS = 204,
            EVENT_CLOSED=205,

            TOKEN_REQUIRED = 301,
            TOKEN_EXPIRED = 302,

            FAIL = 500,

            FORBIDDEN = 403,
            NOT_FOUND = 404,
            INSTALL_REQUİRED = 405,

            SURVEY_ANSWER_CRITIC = 601,
            SURVEY_ANSWER_IMPORTANT = 602,
            SURVEY_ANSWER_FAIL = 603,

            /// <summary>
            ///aracın arızaya düşüp mailin gitmediği durumlarda 
            /// </summary>
            SURVEY_MAIL_NOT_SENT = 604,
            /// <summary>
            /// mailin gidip aracın arızaya düşmediği durum
            /// </summary>
            SURVEY_FAULT_NOT_START=605,
            /// <summary>
            /// hem mailin gitmediği hem aracın arızaya düşmediği ve tatsız olayların yaşanmadığı durum
            /// </summary>
            SURVEY_FAULT_NOT_START_AND_MAIL_NOT_SENT=606,
        }

        public enum GeneralStatus
        {
            ACTIVE = 1,
            PASSIVE = 2,
        }

        public enum EventLogType
        {
            PASSIVE = 0,
            LOGIN = 1,
            LOGOUT = 2,
            RELAY_STARTED = 3,
            RELAY_ENDED = 4,
            BREAK_STARTED = 5,
            BREAK_ENDED = 6,
            FAULT_STARTED = 7,
            FAULT_ENDED = 8,
            MEAL_STARTED = 9,
            MEAL_END = 10,
        }

        public enum EventType
        {
            ONLINE = 3,
            RELAY = 7,
            BREAK = 11,
            FAULT = 15,
            MEAL = 19,
        }

        public enum QuestionGroupType
        {
            RELAY_BEFORE = 1,
            RELAY_AFTER = 2
        }

        public enum ImportanceLevel
        {
            /// <summary>
            /// Kritik: acilen saha supervisorune bilgi gitmeli
            /// </summary>
            CRITIC = 1,
            /// <summary>
            /// Vardiya amiri onaylayana kadar operasyona katılamaz
            /// </summary>
            IMPORTANT = 2,
            /// <summary>
            /// Kullanılabilir fakat mobil super visorune rapor gitmeli
            /// </summary>
            FAIL = 3
        }
        public enum ErrorType
        {
            FAIL = 1,
            EXCEPTION = 2,
            MAIL_EXCEPTION = 3,
        }

        public enum ButtonType
        {
            BreakButton = 2,
            MealButton = 3,
            FaultButton = 4,
            RelayEndButton = 5
        }

        public enum Options
        {
            /// <summary>
            /// Arıza Butonu
            /// </summary>
            FaultButton,
            /// <summary>
            /// Mola Butonu
            /// </summary>
            BreakButton,
            /// <summary>
            /// Yemek Butonu
            /// </summary>
            MealButton,
            /// <summary>
            /// Vardiyayı Bitir Butonu
            /// </summary>
            RelayEndButton,
            /// <summary>
            /// A kategorisinde mail gitme optionu
            /// </summary>
            ACategoryMail,
            /// <summary>
            /// A kategorsinde prosesin devam etmesinin optionu
            /// </summary>
            ACategoryFault,
            /// <summary>
            /// B kategorisinde mail gitme optionu
            /// </summary>
            BCategoryMail,
            /// <summary>
            /// B kategorsinde prosesin devam etmesinin optionu
            /// </summary>
            BCategoryFault,
            /// <summary>
            /// C kategorisinde mail gitme optionu
            /// </summary>
            CCategoryMail,
            /// <summary>
            /// C kategorsinde prosesin devam etmesinin optionu
            /// </summary>
            CCategoryFault
        }
    }
}