﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using static DPM.Api.Constants.Enums;

namespace DPM.Api.Constants
{
    public static class Extensions
    {
        public static IQueryable<TSource> WhereIf<TSource>(
      this IQueryable<TSource> source, bool condition,
  Expression<Func<TSource, bool>> predicate)
        {
            if (condition)
                return source.Where(predicate);
            else
                return source;
        }

        public static string ToEvent(this int value)
        {
            string result = string.Empty;

            switch (value)
            {
                case (int)EventLogType.PASSIVE:
                    result = "Pasif";
                    break;
                case (int)EventLogType.LOGIN:
                    result = "Login";
                    break;
                case (int)EventLogType.LOGOUT:
                    result = "Logout";
                    break;
                case (int)EventLogType.RELAY_STARTED:
                    result = "Vardiya";
                    break;
                case (int)EventLogType.RELAY_ENDED:
                    result = "Vardiya Bitti";
                    break;
                case (int)EventLogType.BREAK_STARTED:
                    result = "Mola";
                    break;
                case (int)EventLogType.BREAK_ENDED:
                    result = "Mola Bitti";
                    break;
                case (int)EventLogType.FAULT_STARTED:
                    result = "Arıza";
                    break;
                case (int)EventLogType.FAULT_ENDED:
                    result = "Arıza Bitti";
                    break;
                case (int)EventLogType.MEAL_STARTED:
                    result = "Yemek Molası";
                    break;
                case (int)EventLogType.MEAL_END:
                    result = "Yemek Molası Bitti";
                    break;
                default:
                    break;
            }

            return result;
        }

        public static string ToEvent(this EventType value)
        {
            string result = string.Empty;

            switch (value)
            {
                case EventType.ONLINE:
                    result = "Oturum";
                    break;
                case EventType.RELAY:
                    result = "Vardiya";
                    break;
                case EventType.BREAK:
                    result = "Mola";
                    break;
                case EventType.FAULT:
                    result = "Arıza";
                    break;
                case EventType.MEAL:
                    result = "Yemek";
                    break;
                default:
                    break;
            }

            return result;
        }

        public static int ToOrderEvent(this int value)
        {
            int result = 0;

            switch (value)
            {
                case (int)EventLogType.PASSIVE:
                    result = 10;
                    break;
                case (int)EventLogType.LOGIN:
                    result = 5;
                    break;
                case (int)EventLogType.LOGOUT:
                    result = 10;
                    break;
                case (int)EventLogType.RELAY_STARTED:
                    result = 4;
                    break;
                case (int)EventLogType.RELAY_ENDED:
                    result = 4;
                    break;
                case (int)EventLogType.BREAK_STARTED:
                    result = 2;
                    break;
                case (int)EventLogType.BREAK_ENDED:
                    result = 2;
                    break;
                case (int)EventLogType.FAULT_STARTED:
                    result = 1;
                    break;
                case (int)EventLogType.FAULT_ENDED:
                    result = 2;
                    break;
                case (int)EventLogType.MEAL_STARTED:
                    result = 3;
                    break;
                case (int)EventLogType.MEAL_END:
                    result = 3;
                    break;
                default:
                    result = 4;
                    break;
            }

            return result;
        }

        public static QuestionGroupType ToGroupType(this int value)
        {
            QuestionGroupType type = QuestionGroupType.RELAY_BEFORE;

            if (value == (int)EventLogType.FAULT_ENDED)
            {
                type = QuestionGroupType.RELAY_BEFORE;
            }

            return type;
        }
    }
}