﻿using DPM.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static DPM.Api.Constants.Enums;

namespace DPM.Api.Models
{
    public class ResponseResult<T>
    {
        public ResponseResult()
        {
            _User = new User();
        }

        public ResponseResult(User user)
        {
            if (user != null)
            {
                _User = user;

                EventId = user.Event.EventId;
                CurrentEvent = user.Event.Description;
                CurrentVehicleId = user.CurrentVehicleId ?? 0;
            }
            
        }

        private User _User { get; set; }

        private ServiceCode code;
        /// <summary>
        /// 
        /// </summary>
        public ServiceCode Code
        {
            get { return code; }
            set
            {
                code = value;
                if (string.IsNullOrEmpty(Message))
                {
                    Message = code.ToString();
                }
            }
        }
        /// <summary>
        /// Response 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public T Data { get; set; }
        

        public int EventId { get; set; }
        public string CurrentEvent { get; set; }
        public int CurrentVehicleId { get; set; }

    }
}