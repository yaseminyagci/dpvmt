﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DPM.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
            ViewBag.WebUrl = ConfigurationManager.AppSettings["weburl"].ToString();
            base.OnActionExecuting(filterContext);
        }
    }
}