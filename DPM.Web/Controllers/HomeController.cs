﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DPM.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(string vehicle)
        { 
            return View();
        }

        public ActionResult RelayEnd()
        {
            return View();
        } 
    }
}