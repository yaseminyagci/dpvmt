﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DPM.Web.Controllers
{
    public class AuthController : Controller
    {
        public ActionResult Login()
        {
            ViewBag.ApiUrl = ConfigurationManager.AppSettings["apiurl"].ToString();
            ViewBag.WebUrl = ConfigurationManager.AppSettings["weburl"].ToString();
            return View();
        }
         
        [HttpPost]
        public JsonResult Authorization(string token)
        {
            FormsAuthentication.SetAuthCookie(token, false);
            return Json(true);
        }
    }
}