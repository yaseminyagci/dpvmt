﻿(function () {
    var DEF = {
        BIND_EVENTS: function (callback) {
            DEF.ELEMENTS.BreakButton.on('click', function (e) {
                e.preventDefault();

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Molaya çıkmak istediğinize emin misiniz?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/event/breakstart', 'post', {}, function (response) {
                            if (response.Code == 200) {
                                DEF.HELPERS.ButtonDisable(response.Data);
                                KTApp.unblockPage();

                                Swal.fire("Mola durumuna geçiş yapıldı.");
                            }
                            else if (response.Code == 205) {
                                Swal.fire(response.Message);
                            }
                        });
                    }
                });



            });

            DEF.ELEMENTS.MealButton.on('click', function (e) {
                e.preventDefault();
                KTApp.blockPage();

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Yemeğe çıkmak istediğinize emin misiniz?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/event/mealstart', 'post', {}, function (response) {
                            if (response.Code == 200) {
                                DEF.HELPERS.ButtonDisable(response.Data);
                                KTApp.unblockPage();

                                Swal.fire("Yemek durumuna geçiş yapıldı.");
                            }
                            else if (response.Code == 205) {
                                Swal.fire(response.Message);
                            }
                        });
                    }
                });



            });

            DEF.ELEMENTS.FaultButton.on('click', function (e) {
                e.preventDefault();

                KTApp.blockPage();

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Araç arıza/bakım durumuna geçecektir. Teknik ekibe email ile bildirim sağlanacaktır.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/event/faultstart', 'post', {}, function (response) {
                            if (response.Code == 200) {
                                KTApp.unblockPage();

                                appBase.Helpers.SetCookie("Token", null);

                                KTApp.blockPage({
                                    overlayColor: '#000000',
                                    type: 'v2',
                                    state: 'success',
                                    message: 'Yönlendiriliyorsunuz. Lütfen bekleyiniz...'
                                });

                                setTimeout(function () {
                                    window.location = "/web/vehicle/fault";
                                }, 3000);
                            }
                            else if (response.Code == 205) {
                                Swal.fire(response.Message);
                            }
                        });
                    }
                });


            });

            DEF.ELEMENTS.RelayButton.on('click', function (e) {
                e.preventDefault();
                KTApp.blockPage();
                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Vardiya bitimi yapılacaktır.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {
                        appBase.Helpers.Request('api/event/EventEnd', 'post', {}, function (response) {
                            if (response.Code == 200) {

                                if (response.Data.CurrentEvent == 'Vardiya Bitti' || response.Data.CurrentEvent == 'Logout') {

                                    if (!response.Data.AnyFinishSurvey) {
                                        appBase.Helpers.SetCookie("Token", null);


                                        KTApp.blockPage({
                                            overlayColor: '#000000',
                                            type: 'v2',
                                            state: 'success',
                                            message: 'Giriş sayfasına yönlendiriliyorsunuz. Lütfen Bekleyiniz...'
                                        });

                                        setTimeout(function () {
                                            window.location = "/web/auth/login";
                                        }, 3000);

                                    }
                                    else {
                                        KTApp.blockPage({
                                            overlayColor: '#000000',
                                            type: 'v2',
                                            state: 'success',
                                            message: 'Operasyon sonrası anket sayfasına yönlendiriliyorsunuz. Lütfen Bekleyin...'
                                        });

                                        setTimeout(function () {
                                            window.location = "/web/home/relayend";
                                        }, 3000);

                                    }
                                }
                                else {
                                    DEF.HELPERS.ButtonEnable(response.Data);
                                    KTApp.unblockPage();
                                }
                            }
                            else if (response.Code == 205) {
                                Swal.fire(response.Message);
                            }
                        });
                    }
                    KTApp.unblockPage();

                });

                
            });

            DEF.ELEMENTS.EndButton.on('click', function (e) {
                e.preventDefault();
                KTApp.blockPage();

                Swal.fire({
                    title: 'Emin misiniz?',
                    text: "Vardiyayı bitirip çıkış yapmak istediğinize emin misiniz?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Evet',
                    cancelButtonText: 'Hayır',
                }).then(function (result) {
                    if (result.value) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Lütfen bekleyiniz...'
                        });

                        appBase.Helpers.Request('api/event/eventend', 'post', {}, function (response) {
                            if (response.Code == 200) {

                                if (response.Data.CurrentEvent == 'Vardiya Bitti' || response.Data.CurrentEvent == 'Logout') {

                                    if (!response.Data.AnyFinishSurvey) {
                                        appBase.Helpers.SetCookie("Token", null);
                                        window.location = "/web/auth/login";
                                    }
                                    else {

                                        window.location = "/web/home/relayend";
                                    }
                                }
                                else {

                                    DEF.HELPERS.ButtonEnable(response.Data);
                                    KTApp.unblockPage();

                                    KTApp.blockPage({
                                        overlayColor: '#000000',
                                        type: 'v2',
                                        state: 'success',
                                        message: 'Zodiac açılıyor lütfen bekleyiniz...'
                                    });

                                    setTimeout(function () {
                                        window.location = "/web/home/detail?vehicle=run";
                                    }, 3000);
                                }
                            }
                            //Token Expired
                            else if (response.Code == 302) {
                                appBase.HELPERS.SetCookie("Token", null);
                                window.location = "/web/auth/login";
                            }
                        });

                    }
                });

                
            });


            setInterval(function () {
                DEF.HELPERS.CheckButtons();
            }, 5000);

            if (callback)
                callback();

        },

        ELEMENTS: {
            BreakButton: $('#break'),
            MealButton: $('#meal'),
            FaultButton: $('#fault'),
            RelayButton: $('#relay'),
            DetailButtoms: $('.detail-buttons'),
            EndButton: $('#currentStatusButton'),
            CurrentStatusLabel: $('#currentStatusLabel'),
            HoursLabel: $('#hours'),
            MinutesLabel: $('#minutes'),
            SecondsLabel: $('#seconds'),
            Table: $('.kt-datatable'),

        },
        CONSTANT: {
            LogTable: null,
            Counter: null,
            TotalSeconds: 0,
            LogUserId: [],
        },

        HELPERS: {
            ButtonDisable: function (currentState) {
                //DEF.ELEMENTS.DetailButtoms.find('button').addClass('disabled');
                //DEF.ELEMENTS.DetailButtoms.find('button').attr('disabled', 'disabled');

                DEF.ELEMENTS.CurrentStatusLabel.find('span').text(currentState);
                DEF.ELEMENTS.CurrentStatusLabel.show();
                DEF.ELEMENTS.EndButton.show();
            },
            ButtonEnable: function () {
                //DEF.ELEMENTS.DetailButtoms.find('button').removeClass('disabled');
                //DEF.ELEMENTS.DetailButtoms.find('button').removeAttr('disabled');

                DEF.ELEMENTS.CurrentStatusLabel.find('span').text('');
                DEF.ELEMENTS.CurrentStatusLabel.hide();
                DEF.ELEMENTS.EndButton.hide();

            },
            GetCurrentEvent: function () {
                KTApp.blockPage();

                appBase.Helpers.Request('api/event/getcurrentevent', 'get', {}, function (response) {
                    if (response.Code == 200) {
                        var currentState = response.Data.EventId;

                        if (currentState == 3 || currentState % 2 == 0) {
                            DEF.HELPERS.ButtonEnable();
                            //DEF.HELPERS.TimerDeactive();

                        }
                        else {
                            DEF.HELPERS.ButtonDisable(response.Data.CurrentEvent);

                            //DEF.HELPERS.TimerActive(); 
                        }
                        KTApp.unblockPage();
                    }

                }, function (responseError) {

                    console.log(responseError)
                    //if (responseError.responseJSON.Code == 302) {//Token Expired
                    //    appBase.Helpers.SetCookie("Token", null);
                    //    window.location = "/auth/login";
                    //}
                });
            },
            TimerActive: function () {
                DEF.CONSTANT.Counter = setInterval(DEF.HELPERS.SetTime, 1000);
            },
            TimerDeactive: function () {
                clearInterval(DEF.CONSTANT.Counter);
            },
            SetTime: function () {
                ++DEF.CONSTANT.TotalSeconds;
                DEF.ELEMENTS.SecondsLabel.text(DEF.HELPERS.Pad(DEF.CONSTANT.TotalSeconds % 60));
                DEF.ELEMENTS.MinutesLabel.text(DEF.HELPERS.Pad(parseInt(DEF.CONSTANT.TotalSeconds / 60)));
                DEF.ELEMENTS.HoursLabel.text(DEF.HELPERS.Pad(parseInt(DEF.CONSTANT.TotalSeconds / 3600)));
            },
            Pad: function (val) {
                var valString = val + "";
                if (valString.length < 2) {
                    return "0" + valString;
                } else {
                    return valString;
                }
            },
            GetUserLogs: function () { 
                KTDatatableRemoteAjaxDemo.init();
            },
            CheckButtons: function () {

                appBase.Helpers.Request("api/Admin/CheckButtons", "get", {}, function (response) {
                    if (response.Code == 200) {

                        var buttons = response.Data;

                        if (buttons.BreakButton)
                            DEF.ELEMENTS.BreakButton.show();
                        else
                            DEF.ELEMENTS.BreakButton.hide();

                        if (buttons.FaultButton)
                            DEF.ELEMENTS.FaultButton.show();
                        else
                            DEF.ELEMENTS.FaultButton.hide();

                        if (buttons.MealButton)
                            DEF.ELEMENTS.MealButton.show();
                        else
                            DEF.ELEMENTS.MealButton.hide();

                        if (buttons.RelayEndButton)
                            DEF.ELEMENTS.RelayButton.show();
                        else
                            DEF.ELEMENTS.RelayButton.hide();

                    }
                });
            }
        },

        MODELS: {}

    }


    $(function () {
        DEF.BIND_EVENTS();
        DEF.HELPERS.GetCurrentEvent(); 
        //DEF.HELPERS.GetUserLogs(); 
    });

    window.surveyDetail = DEF;
})();
