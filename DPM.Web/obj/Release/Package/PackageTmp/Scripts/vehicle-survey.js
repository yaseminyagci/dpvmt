﻿; (function () {

    var DEF = {
        ELEMENTS: {
            SendSurveyButton: $('#sendSurvey'),
            SurveyContent: $('#surver-content'),
            NoteRepeater: $('#kt_repeater_1'),
            NoteInputs: $('.note-input'),
            SurveyId: null,
            ComplateButton: $('#buttonComplated'),
            UserNameinSurvey: $('#CurrentUser'),
        },

        HELPERS: {
            GetSurvey: function (callback) {
                KTApp.blockPage()
                appBase.Helpers.Request(
                    'api/Question/GetQuestions',
                    'GET', {},
                    function (response) {
                        console.log(response);

                        var template = $('#surveritem-template').html();
                        var rendered = Mustache.render(template, response.Data);
                        $('#survey-header-text').text(response.Data.VehicleName);

                        $('#surver-content').append(rendered);

                        KTApp.unblockPage();

                        callback();
                    },
                    function (response) {
                    }
                );
            },
            InitWizard: function () {
                var KTWizard3 = function () {
                    // Base elements
                    var wizardEl;
                    var formEl;
                    var validator;
                    var wizard;

                    // Private functions
                    var initWizard = function () {
                        // Initialize form wizard
                        wizard = new KTWizard('kt_wizard_v3', {
                            startStep: 1, // initial active step number
                            clickableSteps: true  // allow step clicking
                        });

                        // Validation before going to next page
                        wizard.on('beforeNext', function (wizardObj) {
                            //if (validator.form() !== true) {
                            //    wizardObj.stop();  // don't go to the next step
                            //} 

                            //$.each($('.kt-wizard-v3__content[data-ktwizard-state="current"]').find('.kt-option'), function (i, v) {
                            //    var currentRadio = $(v).find('input[name="radio-' + $(v).data('questionid') + '"]');
                            //    if (!currentRadio.is(':checked')) {
                            //        swal.fire({
                            //            type: 'warning',
                            //            text: 'Araç kurulumu yapılmamıştır. Kurulum yapılması gereklidir.',
                            //            onClose: function () {
                            //                window.location.href = "/vehicle/setup";
                            //            }
                            //        });

                            //        wizardObj.stop();
                            //        return false;
                            //    }
                            //});

                            if (!DEF.HELPERS.WizardValidator(wizardObj)) {
                                return false;
                            }

                        });

                        wizard.on('beforePrev', function (wizardObj) {
                            if (validator.form() !== true) {
                                wizardObj.stop();  // don't go to the next step
                            }
                        });

                        // Change event
                        wizard.on('change', function (wizard, wizardObj) {
                            KTUtil.scrollTop();
                        });
                    }

                    var initValidation = function () {
                        validator = formEl.validate({
                            // Validate only visible fields
                            ignore: ":hidden",

                            // Validation rules
                            rules: {},

                            // Display error
                            invalidHandler: function (event, validator) {
                                KTUtil.scrollTop();

                                swal.fire({
                                    "title": "",
                                    "text": "There are some errors in your submission. Please correct them.",
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary"
                                });
                            },

                            // Submit valid form
                            submitHandler: function (form) {

                            }
                        });
                    }

                    var initSubmit = function () {

                        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

                        btn.on('click', function (e, wizardObj) {
                            e.preventDefault();

                            if (!DEF.HELPERS.WizardValidator()) {
                                return false;
                            }

                            $('#sendModal').modal('show');
                        });
                    }

                    return {
                        // public functions
                        init: function () {
                            wizardEl = KTUtil.get('kt_wizard_v3');
                            formEl = $('#kt_form');

                            initWizard();
                            initValidation();
                            initSubmit();
                        }
                    };
                }();
                return KTWizard3;
            },
            WizardValidator: function (wizardObj) {

                var result = true;

                $.each($('.kt-wizard-v3__content[data-ktwizard-state="current"]').find('.kt-option'), function (i, v) {

                    if (!result) {
                        return false;
                    }
                    var currentRadio = $(v).find('input[name="radio-' + $(v).data('questionid') + '"]');
                    if (!currentRadio.is(':checked')) {
                        swal.fire({
                            type: 'warning',
                            text: 'Tüm durumların işaretlenmesi gerekmektedir.',
                            //onClose: function () {
                            //    window.location.href = "/vehicle/setup";
                            //}
                        });

                        if (typeof wizardObj != 'undefined') {
                            wizardObj.stop();
                        }

                        result = false;
                    }

                });

                return result;
            },
            ComplateOperation: function () {

                var answerList = [];
                var noteList = [];

                $.each(DEF.ELEMENTS.SurveyContent.find('.kt-option'), function (i, v) {

                    var $this = $(v);

                    var questionID = $(v).data('questionid');

                    var currentRadio = $this.find('input[name="radio-' + questionID + '"]');

                    var checkedRadioVal = $this.find('input[name="radio-' + questionID + '"]:checked').val() == "1";

                    answerList.push({
                        QuestionId: questionID,
                        AnswerType: 1,
                        Answer: checkedRadioVal.toString()
                    })
                });

                //$.each(DEF.ELEMENTS.NoteRepeater.find('.note-input'), function (i, v) {

                //    if ($(v).val() !== '') {
                //        noteList.push({
                //            Text: $(v).val()
                //        })
                //    }
                //});


                noteList.push({ Text: $('.note-input').val() });

                DEF.MODELS.PostModel.AnswerList = answerList;
                DEF.MODELS.PostModel.NoteList = noteList;



                appBase.Helpers.Request('api/Survey/AddSurvey', 'POST', DEF.MODELS.PostModel, function (response) {
                    KTApp.unblockPage();

                    if (response.Code == 200 && response.Data == true) {

                        KTApp.blockPage({
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'success',
                            message: 'Cevaplar gönderildi. Zodiac açılıyor lütfen bekleyiniz...'
                        });

                        setTimeout(function () {
                            window.location = "/web/home/detail?vehicle=run";
                        }, 3000);
                    }
                    else if (response.Code == 601) {//Kritik kategoride hayır cevabı verildi

                        DEF.ELEMENTS.SurveyId = response.Message;

                        swal.fire({
                            type: 'error',
                            text: 'A kategorisinde hayır cevabı verdiğiniz için supervisora bilgi verilecektir',
                        }).then(function (result) {

                            KTApp.blockPage({
                                overlayColor: '#000000',
                                type: 'v2',
                                state: 'success',
                                message: 'Lütfen Bekleyiniz...'
                            });

                            appBase.Helpers.SetCookie("Token", "");

                            if (result.value) {

                                $.ajax({
                                    url: generalAPIUrl + "api/User/CallSuperVisor?surveyId=" + DEF.ELEMENTS.SurveyId,
                                    method: "POST",
                                    success: function (result) {
                                        if (result.Code == 601) {

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail başarıyla gönderildi.. Arıza sayfasına yönlendiriliyorsunuz."
                                            });

                                            setTimeout(function () {
                                                window.location = "/web/vehicle/fault";
                                            }, 3000);
                                        }
                                        else if (result.Code == 604) {//mail gitmedi ama araç arızaya düştü

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail gönderimi iptal edildi.. Arıza sayfasına yönlendiriliyorsunuz."
                                            });

                                            setTimeout(function () {
                                                window.location = "/web/vehicle/fault";
                                            }, 3000);

                                        }
                                        else if (result.Code == 605) {//mail gitti, arıza başlamadı

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail başarıyla gönderildi.. Zodiac açılıyor lütfen bekleyiniz..."
                                            });


                                            setTimeout(function () {
                                                window.location = "/web/home/detail?vehicle=run";
                                            }, 3000);

                                        }
                                        else {

                                            KTApp.unblockPage();

                                            swal.fire({
                                                type: 'error',
                                                text: 'Mail gönderilemedi',
                                            });
                                        }
                                    },
                                });
                            }

                        });

                    }
                    else if (response.Code == 602) {//Acil kategorisinde hayır cevabı verildi

                        DEF.ELEMENTS.SurveyId = response.Message;


                        swal.fire({
                            type: 'error',
                            text: 'B kategorisinde hayır cevabı verdiğiniz için supervisora bilgi verilecek onaylıyor musunuz?',
                        }).then(function (result) {

                            if (result.value) {


                                $.ajax({
                                    url: generalAPIUrl + "api/User/CallChief?surveyId=" + DEF.ELEMENTS.SurveyId,
                                    method: "POST",
                                    success: function (result) {
                                        if (result.Code == 602) {

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail başarıyla gönderildi.. Arıza sayfasına yönlendiriliyorsunuz."
                                            });

                                            setTimeout(function () {
                                                window.location = "/web/vehicle/fault";
                                            }, 3000);
                                        }
                                        else if (result.Code == 604) {//mail gitmedi ama araç arızaya düştü

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail gönderimi iptal edildi.. Arıza sayfasına yönlendiriliyorsunuz."
                                            });

                                            setTimeout(function () {
                                                window.location = "/web/vehicle/fault";
                                            }, 3000);

                                        }
                                        else if (result.Code == 605) {//mail gitti, arıza başlamadı

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail başarıyla gönderildi.. Zodiac açılıyor lütfen bekleyiniz..."
                                            });


                                            setTimeout(function () {
                                                window.location = "/web/home/detail?vehicle=run";
                                            }, 3000);

                                        }
                                        else {

                                            KTApp.unblockPage();

                                            swal.fire({
                                                type: 'error',
                                                text: 'Mail gönderilemedi',
                                            });
                                        }
                                    },
                                });
                            }
                        });

                    }
                    else if (response.Code == 603) {//Hata kategorsinde hayır cevabı verildi.

                        DEF.ELEMENTS.SurveyId = response.Message;

                        swal.fire({
                            type: 'error',
                            text: 'C kategorisinde hayır cevabı verdiğiniz için mobil supervisora bilgi verilecek onaylıyor musunuz?',
                        }).then(function (result) {

                            if (result.value) {


                                $.ajax({
                                    url: generalAPIUrl + "api/User/CallMobileVisor?surveyId=" + DEF.ELEMENTS.SurveyId,
                                    method: "POST",
                                    success: function (result) {
                                        if (result.Code == 603) {

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail başarıyla gönderildi.. Arıza sayfasına yönlendiriliyorsunuz."
                                            });

                                            setTimeout(function () {
                                                window.location = "/web/vehicle/fault";
                                            }, 3000);
                                        }
                                        else if (result.Code == 604) {//mail gitmedi ama araç arızaya düştü

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail gönderimi iptal edildi.. Arıza sayfasına yönlendiriliyorsunuz."
                                            });

                                            setTimeout(function () {
                                                window.location = "/web/vehicle/fault";
                                            }, 3000);

                                        }
                                        else if (result.Code == 605) {//mail gitti, arıza başlamadı

                                            KTApp.blockPage({
                                                overlayColor: '#000000',
                                                type: 'v2',
                                                state: 'success',
                                                message: "Mail başarıyla gönderildi.. Zodiac açılıyor lütfen bekleyiniz..."
                                            });


                                            setTimeout(function () {
                                                window.location = "/web/home/detail?vehicle=run";
                                            }, 3000);

                                        }
                                        else {

                                            KTApp.unblockPage();

                                            swal.fire({
                                                type: 'error',
                                                text: 'Mail gönderilemedi',
                                            });
                                        }
                                    },
                                });
                            }

                        });
                    }
                    else {
                        swal.fire("Bir hata ile karşılaşıldı.");
                        KTApp.unblock('#sendModal')
                        console.log(response);
                    }
                }, function (errorResponse) {
                    KTApp.unblock('#sendModal');
                    swal.fire("Bir hata ile karşılaşıldı.");

                });
            },
            SetUserName: function () {

                var userName = window.appBase.Helpers.GetCookie("UserName")

                if (userName == "") {
                    setTimeout(function () {
                        DEF.HELPERS.SetUserName();
                    }, 1000);
                }
                else {
                    DEF.ELEMENTS.UserNameinSurvey.text(userName);

                }

            }
        },

        MODELS: {
            PostModel: {
                AnswerList: [],
                NoteList: [],
                SurveyType: 1
            }
        },

        BindEvents: function () {
            DEF.ELEMENTS.SendSurveyButton.on('click', function (e) {
                e.preventDefault();


                $.each(DEF.ELEMENTS.SurveyContent.find('.kt-option'), function (i, v) {
                    var currentRadio = $(v).find('input[name="radio-' + $(v).data('questionid') + '"]');
                    if (!currentRadio.is(':checked')) {
                        swal.fire('Lütfen tüm maddeleri işaretleyiniz.', '', 'warning');
                        return false;
                    }
                });

                var answerList = [];
                var noteList = [];

                $.each(DEF.ELEMENTS.SurveyContent.find('.kt-option'), function (i, v) {

                    var $this = $(v);

                    var questionID = $(v).data('questionid');

                    var currentRadio = $this.find('input[name="radio-' + questionID + '"]');

                    var checkedRadioVal = $this.find('input[name="radio-' + questionID + '"]:checked').val() == "1";

                    answerList.push({
                        QuestionId: questionID,
                        AnswerType: 1,
                        Answer: checkedRadioVal.toString()
                    })
                });

                $.each(DEF.ELEMENTS.NoteRepeater.find('.note-input'), function (i, v) {

                    if ($(v).val() !== '') {
                        noteList.push({
                            Text: $(v).val()
                        })
                    }
                });

                DEF.MODELS.PostModel.AnswerList = answerList;
                DEF.MODELS.PostModel.NoteList = noteList;

                appBase.Helpers.Request('api/Survey/AddSurvey', 'POST', DEF.MODELS.PostModel, function (response) {

                    if (response.Code == 200 && response.Data == true) {
                        window.location = "/web/home/detail";
                    }
                    else {
                        swal.fire("Bir hata ile karşılaşıldı.");
                        console.log(response);
                    }
                });

            });

            DEF.ELEMENTS.NoteRepeater.repeater({
                initEmpty: false,

                defaultValues: {
                    'text-input': 'foo'
                },

                show: function () {
                    $(this).slideDown();
                },

                hide: function (deleteElement) {
                    $(this).slideUp(deleteElement);
                }
            });

            DEF.ELEMENTS.ComplateButton.on('click', function (e) {
                e.preventDefault();

                KTApp.block('#sendModal', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'success',
                    message: 'Cevaplar gönderiliyor. Lütfen Bekleyiniz...'
                });

                DEF.HELPERS.ComplateOperation();
            });



        }
    }


    $(function () {

        DEF.BindEvents();
        DEF.HELPERS.GetSurvey(function () {
            DEF.HELPERS.InitWizard().init();
        });

        DEF.HELPERS.SetUserName();

    });

})();

