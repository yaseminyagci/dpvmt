"use strict";

// Class Definition
var KTLoginGeneral = function () {

    var login = $('#kt_login');

    var showErrorMsg = function (form, type, msg) {
        var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
			<span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }

    // Private Functions
    var displaySignUpForm = function () {
        login.removeClass('kt-login--forgot');
        login.removeClass('kt-login--signin');

        login.addClass('kt-login--signup');
        KTUtil.animateClass(login.find('.kt-login__signup')[0], 'flipInX animated');
    }

    var displaySignInForm = function () {
        login.removeClass('kt-login--forgot');
        login.removeClass('kt-login--signup');

        login.addClass('kt-login--signin');
        KTUtil.animateClass(login.find('.kt-login__signin')[0], 'flipInX animated');
        //login.find('.kt-login__signin').animateClass('flipInX animated');
    }

    var displayForgotForm = function () {
        login.removeClass('kt-login--signin');
        login.removeClass('kt-login--signup');

        login.addClass('kt-login--forgot');
        //login.find('.kt-login--forgot').animateClass('flipInX animated');
        KTUtil.animateClass(login.find('.kt-login__forgot')[0], 'flipInX animated');

    }

    var handleFormSwitch = function () {
        $('#kt_login_forgot').click(function (e) {
            e.preventDefault();
            displayForgotForm();
        });

        $('#kt_login_forgot_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
        });

        $('#kt_login_signup').click(function (e) {
            e.preventDefault();
            displaySignUpForm();
        });

        $('#kt_login_signup_cancel').click(function (e) {
            e.preventDefault();
            displaySignInForm();
        });
    }

    var handleSignInFormSubmit = function () {
        $('#kt_login_signin_submit').click(function (e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            var email = $('#username');
            var password = $('#password');
            var vehicleID = $('#vehicleId').val() == '' ? 1 : parseInt($('#vehicleId').val());


            $.ajax({
                url: generalAPIUrl + 'api/User/Login',
                type: 'POST',
                data: { VehicleID: vehicleID, Email: email.val(), Password: password.val() },
                success: function (response) {

                    switch (response.Code) {
                        case 200:
                            document.cookie = "Token=" + response.Data.Token + ";1;path=/";
                            //appBase.Helpers.SetCookie('Vehicle', vehicleID.val(), 2);

                            $.post('/web/auth/Authorization', { token: response.Data.Token }, function (response) {
                                if (response) {
                                    window.location.href = '/web/';
                                }
                            });
                            break;
                        case 202:
                            Swal.fire({
                                title: 'Kapanmamış oturumunuz bulunmaktadır. Burada oturum açmak için diğer oturumunuzun bitirilmesi gerekmektedir onaylıyor musunuz?',
                                text: "",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Evet',
                                cancelButtonText: 'Hayır',
                            }).then(function (result) {
                                if (result.value) {

                                    KTApp.blockPage({
                                        overlayColor: '#000000',
                                        type: 'v2',
                                        state: 'success',
                                        message: 'Lütfen bekleyiniz...'
                                    });

                                    $.ajax({
                                        url: generalAPIUrl + 'api/User/CloseSession',
                                        method: 'get',
                                        data: { email: email.val(), password: password.val() },
                                        success: function (response) {

                                            if (response.Code == 200 && response.Data) {

                                                KTApp.blockPage({
                                                    overlayColor: '#000000',
                                                    type: 'v2',
                                                    state: 'success',
                                                    message: 'Yönlendiriliyorsunuz Lütfen bekleyiniz...'
                                                });


                                                $.ajax({
                                                    url: generalAPIUrl + '/api/User/Login',
                                                    type: 'POST',
                                                    data: { VehicleID: vehicleID, Email: email.val(), Password: password.val() },
                                                    success: function (response) {
                                                        if (response.Code == 200) {
                                                            document.cookie = "Token=" + response.Data.Token + ";1;path=/";
                                                            //appBase.Helpers.SetCookie('Vehicle', vehicleID.val(), 2);

                                                            $.post('/web/auth/Authorization', { token: response.Data.Token }, function (response) {
                                                                if (response) {
                                                                    window.location.href = '/web/';
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            KTApp.unblockPage();

                                                            swal.fire("Bir Hata oluştu Lütfen daha sonra tekrar deneyiniz.");
                                                        }
                                                    },
                                                });
                                            }
                                            else {
                                                KTApp.unblockPage();


                                                swal("Bir Hata oluştu");
                                            }

                                        },
                                        error: function (req, status, error) {
                                            KTApp.unblockPage();

                                            swal("Bir Hata oluştu");
                                            swal(req.responseText);
                                        }
                                    });

                                }
                            });


                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

                            return;
                            break;
                        case 204:

                            KTApp.blockPage({
                                overlayColor: '#000000',
                                type: 'v2',
                                state: 'success',
                                message: 'Araç arızalıdır. Arıza sayfasına yönlendiriliyorsunuz. Lütfen bekleyiniz...'
                            });

                            setTimeout(function () {
                                window.location = "/web/vehicle/fault";
                            }, 3000);

                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            return;
                            break;
                        //case 405:
                        //    Swal.fire({
                        //        text: 'Araç kurulumu yapılmamıştır. Kurulum yapılması gereklidir.',
                        //        onClose: function () {
                        //            window.location.href = "/vehicle/setup";
                        //        }
                        //    });
                        //    break;
                        default:
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            swal.fire("Kullanıcı adı yada şifre yanlış.");
                            break;
                    }

                },
                error: function (err) {
                    console.log("ERROR")
                },
                statusCode: {
                    400: function () {
                        fail;
                    },
                    500: function () { console.log("Server") }
                }

            });

        });
    }

    var handleSignUpFormSubmit = function () {
        $('#kt_login_signup_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    fullname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    rpassword: {
                        required: true
                    },
                    agree: {
                        required: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '',
                success: function (response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function () {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        form.clearForm();
                        form.validate().resetForm();

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.kt-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        showErrorMsg(signInForm, 'success', 'Kullanıcı kaydı başarılı.');
                    }, 2000);
                }
            });
        });
    }

    var handleForgotFormSubmit = function () {
        $('#kt_login_forgot_submit').click(function (e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: '',
                success: function (response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function () {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false); // remove
                        form.clearForm(); // clear form
                        form.validate().resetForm(); // reset validation states

                        // display signup form
                        displaySignInForm();
                        var signInForm = login.find('.kt-login__signin form');
                        signInForm.clearForm();
                        signInForm.validate().resetForm();

                        showErrorMsg(signInForm, 'success', 'Yeni şifre talebiniz alındı. Yeni şifre bilgileriniz en kısa sürede e-posta adresinize iletilecektir.');
                    }, 2000);
                }
            });
        });
    }

    // Public Functions
    return {
        // public functions
        init: function () {
            handleFormSwitch();
            handleSignInFormSubmit();
            handleSignUpFormSubmit();
            handleForgotFormSubmit();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function () {
    KTLoginGeneral.init();
    KTApp.blockPage();

    var vehicleItemID = $('#vehicle-item-id').val() == '' ? 0 : parseInt($('#vehicle-item-id').val());

    $.get(generalAPIUrl + 'api/User/getvehicle', { itemId: vehicleItemID }, function (response) {

        if (response.Code == 200) {
            $('#vehicleName').text(response.Data.Name);
            $('#vehicleId').val(response.Data.VehicleId);
        }
        else {
            swal.fire("Cihaz bulunamadı.");
        }

        KTApp.unblockPage();
    });
});
