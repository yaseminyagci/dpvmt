﻿; (function () {

    var DEF = {
        ELEMENTS: {
            SendSurveyButton: $('#sendSurvey'),
            SurveyContent: $('#surver-content'),
            NoteRepeater: $('#kt_repeater_1'),
            NoteInputs: $('.note-input'),
        },

        HELPERS: {
            GetSurvey: function (callback) {
                KTApp.blockPage()
                appBase.Helpers.Request(
                    'api/Question/GetQuestions',
                    'GET', {},
                    function (response) {
                        console.log(response);

                        var template = $('#surveritem-template').html();
                        var rendered = Mustache.render(template, response.Data);
                        $('#survey-header-text').text(response.Data.VehicleName);

                        $('#surver-content').append(rendered);

                        KTApp.unblockPage();

                        callback();
                    },
                    function (response) {
                    }
                );
            },
            InitWizard: function () {
                var KTWizard3 = function () {
                    // Base elements
                    var wizardEl;
                    var formEl;
                    var validator;
                    var wizard;

                    // Private functions
                    var initWizard = function () {
                        // Initialize form wizard
                        wizard = new KTWizard('kt_wizard_v3', {
                            startStep: 1, // initial active step number
                            clickableSteps: true  // allow step clicking
                        });

                        // Validation before going to next page
                        wizard.on('beforeNext', function (wizardObj) {
                            //if (validator.form() !== true) {
                            //    wizardObj.stop();  // don't go to the next step
                            //} 

                            var validate = true;
                            $.each($('.kt-wizard-v3__content[data-ktwizard-state="current"]').find('.kt-option'), function (i, v) {
                                var currentRadio = $(v).find('input[name="radio-' + $(v).data('questionid') + '"]');
                                if (!currentRadio.is(':checked')) {
                                    //swal.fire('Lütfen tüm maddeleri işaretleyiniz.', '', 'warning').then(function (result) {
                                    //    if (result.value) {
                                    //        //KTUtil.scrollTop();
                                    //    };
                                    //});

                                    wizardObj.stop();
                                    return false;
                                }
                            });

                        });

                        wizard.on('beforePrev', function (wizardObj) {
                            if (validator.form() !== true) {
                                wizardObj.stop();  // don't go to the next step
                            }
                        });

                        // Change event
                        wizard.on('change', function (wizard) {
                            KTUtil.scrollTop();
                        });
                    }

                    var initValidation = function () {
                        validator = formEl.validate({
                            // Validate only visible fields
                            ignore: ":hidden",

                            // Validation rules
                            rules: {},

                            // Display error
                            invalidHandler: function (event, validator) {
                                KTUtil.scrollTop();

                                swal.fire({
                                    "title": "",
                                    "text": "There are some errors in your submission. Please correct them.",
                                    "type": "error",
                                    "confirmButtonClass": "btn btn-secondary"
                                });
                            },

                            // Submit valid form
                            submitHandler: function (form) {

                            }
                        });
                    }

                    var initSubmit = function () {
                        var btn = formEl.find('[data-ktwizard-type="action-submit"]');


                        btn.on('click', function (e) {
                            e.preventDefault();

                            var answerList = [];
                            var noteList = [];

                            $.each(DEF.ELEMENTS.SurveyContent.find('.kt-option'), function (i, v) {

                                var $this = $(v);

                                var questionID = $(v).data('questionid');

                                var currentRadio = $this.find('input[name="radio-' + questionID + '"]');

                                var checkedRadioVal = $this.find('input[name="radio-' + questionID + '"]:checked').val() == "1";

                                answerList.push({
                                    QuestionId: questionID,
                                    AnswerType: 1,
                                    Answer: checkedRadioVal.toString()
                                })
                            });

                            $.each(DEF.ELEMENTS.NoteRepeater.find('.note-input'), function (i, v) {

                                if ($(v).val() !== '') {
                                    noteList.push({
                                        Text: $(v).val()
                                    })
                                }
                            });

                            DEF.MODELS.PostModel.AnswerList = answerList;
                            DEF.MODELS.PostModel.NoteList = noteList;

                            KTApp.blockPage({
                                overlayColor: '#000000',
                                type: 'v2',
                                state: 'success',
                                message: 'Cevaplar gönderiliyor. Lütfen Bekleyiniz...'
                            });

                            appBase.Helpers.Request('api/Survey/AddSurveyEnd', 'POST', DEF.MODELS.PostModel, function (response) {
                                if (response.Code == 200 && response.Data == true) {
                                    appBase.Helpers.SetCookie("Token", null);

                                    KTApp.blockPage({
                                        overlayColor: '#000000',
                                        type: 'v2',
                                        state: 'success',
                                        message: 'Cevaplar gönderildi. Yönlendiriliyorsunuz...'
                                    });

                                    setTimeout(function () {
                                        window.location = "/web/auth/login";
                                    }, 3000);
                                }
                                else if (response.Code == 201) {
                                    swal.fire(response.Message);
                                    console.log(response);
                                }
                                else {
                                    swal.fire("Bir hata ile karşılaşıldı.");
                                    console.log(response);
                                }
                            })
                        });
                    }

                    return {
                        // public functions
                        init: function () {
                            wizardEl = KTUtil.get('kt_wizard_v3');
                            formEl = $('#kt_form');

                            initWizard();
                            initValidation();
                            initSubmit();
                        }
                    };
                }();
                return KTWizard3;
            }
        },

        MODELS: {
            PostModel: {
                AnswerList: [],
                NoteList: [],
                SurveyType: 1
            }
        },

        BindEvents: function () {
            DEF.ELEMENTS.SendSurveyButton.on('click', function (e) {
                e.preventDefault();


                $.each(DEF.ELEMENTS.SurveyContent.find('.kt-option'), function (i, v) {
                    var currentRadio = $(v).find('input[name="radio-' + $(v).data('questionid') + '"]');
                    if (!currentRadio.is(':checked')) {
                        swal.fire('Lütfen tüm maddeleri işaretleyiniz.', '', 'warning');
                        return false;
                    }
                });

                var answerList = [];
                var noteList = [];

                $.each(DEF.ELEMENTS.SurveyContent.find('.kt-option'), function (i, v) {

                    var $this = $(v);

                    var questionID = $(v).data('questionid');

                    var currentRadio = $this.find('input[name="radio-' + questionID + '"]');

                    var checkedRadioVal = $this.find('input[name="radio-' + questionID + '"]:checked').val() == "1";

                    answerList.push({
                        QuestionId: questionID,
                        AnswerType: 1,
                        Answer: checkedRadioVal.toString()
                    })
                });

                $.each(DEF.ELEMENTS.NoteRepeater.find('.note-input'), function (i, v) {

                    if ($(v).val() !== '') {
                        noteList.push({
                            Text: $(v).val()
                        })
                    }
                });

                DEF.MODELS.PostModel.AnswerList = answerList;
                DEF.MODELS.PostModel.NoteList = noteList;

                appBase.Helpers.Request('api/Survey/AddSurveyEnd', 'POST', DEF.MODELS.PostModel, function (response) {

                    if (response.Code == 200 && response.Data == true) {
                        window.location = "/web/auth/login";
                    }
                    else if (response.Code == 201) {
                        swal.fire(response.Message);
                        console.log(response);
                    }
                    else {
                        swal.fire("Bir hata ile karşılaşıldı.");
                        console.log(response);
                    }
                })

            });

            DEF.ELEMENTS.NoteRepeater.repeater({
                initEmpty: false,

                defaultValues: {
                    'text-input': 'foo'
                },

                show: function () {
                    $(this).slideDown();
                },

                hide: function (deleteElement) {
                    $(this).slideUp(deleteElement);
                }
            });


        }
    }


    $(function () {
        DEF.BindEvents();
        DEF.HELPERS.GetSurvey(function () {
            DEF.HELPERS.InitWizard().init();
        });
    });

})();