﻿"use strict";
// Class definition

var KTDatatableRemoteAjaxDemo = function () {
    // Private functions

    // basic demo
    var demo = function () {

        var datatable = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: appBase.ApiUrl + "api/Event/GetUserLogs",
                        params: { Token: appBase.Helpers.GetCookie("Token") },
                        // sample custom headers
                        //headers: { 'x-my-custokt-header': 'some value', 'x-test-header': 'the value' },
                        //map: function (raw) {
                        //    // sample data mapping
                        //    var dataSet = raw;
                        //    if (typeof raw.data !== 'undefined') {
                        //        dataSet = raw.data;
                        //    }
                        //    return dataSet;
                        //},
                        method: 'get',
                    },
                },
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: true,
                footer: false,
            },

            // column sorting
            sortable: true,

            pagination: true,

            //search: {
            //    input: $('#generalSearch'),
            //},

            // columns definition
            columns: [
                {
                    field: 'Event',
                    title: 'İşlem',
                    width: 150,
                    autoHide: false
                },
                {
                    field: 'StartTime',
                    title: 'Başlangıç Zamanı',
                    format: 'DD/MM/YYYY HH:mm:ss',
                    template: function (row) {
                        return moment(row.StartTime).format("DD/MM/YYYY HH:mm:ss")
                    },
                    autoHide: false
                },
                {
                    field: 'EndTime',
                    title: 'Bitiş Zamanı',
                    format: 'DD/MM/YYYY HH:mm:ss',
                    textAlign: 'center',
                    template: function (row) {
                        return moment(row.EndTime).format("DD/MM/YYYY HH:mm:ss")
                    },
                    autoHide: false
                },
                {
                    //width: 40,
                    field: 'PassingTime',
                    title: 'Toplam Süre',
                    template: function (row) {
                        return moment(row.PassingTime, 'HH:mm:ss').format("HH:mm:ss")
                    },
                    autoHide: false
                },
                {
                    field: 'Vehicle',
                    title: 'Araç',
                    autoHide: false
                },
                
            ],
        });

    };

    return {
        // public functions
        init: function () {
            demo();
        },
    };
}();

jQuery(document).ready(function () {
    //KTDatatableRemoteAjaxDemo.init();
});