﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPM.Model.ApiModels
{
    public class LogModels
    {
        public class UserLogModel
        {
            public int UserId { get; set; }
            public string UserName { get; set; }
            public int UserRole { get; set; }
            public string UserRoleName { get; set; }
            public string Event { get; set; }
            public string Vehicle { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public TimeSpan PassingTime { get; set; }
        }
        public class Meta
        {
            public int page { get; set; } = 1;
            public int pages { get; set; } = 1;
            public int perpage { get; set; } = 10;
            public int total { get; set; }
            public string sort { get; set; } = "asc";
            public string field { get; set; }


        }

        public class DataTableModel<T>
        {
            public Meta meta { get; set; }
            public List<T> data = new List<T>();
        }
    }
}
