﻿using System;
using System.Collections.Generic;

namespace DPM.Model.ApiModels
{
    public class SurveyModel
    {
        public class AnswerModel
        {
            public int QuestionId { get; set; }
            public int AnswerType { get; set; } = 1;
            public bool Answer { get; set; }
        }
        public class NoteModel
        {
            public string Text { get; set; }
        }

        public class AnswerPostModel
        {
            public AnswerPostModel()
            {
                AnswerList = new List<AnswerModel>();
                NoteList = new List<NoteModel>();
            }
            public IEnumerable<AnswerModel> AnswerList { get; set; }
            public IEnumerable<NoteModel> NoteList { get; set; }
            public int SurveyType { get; set; }
        }

        public class SurveyHistory
        {
            public List<SurveyAnswer> Answer = new List<SurveyAnswer>();
            public DateTime SurveyTime { get; set; }
            public string SurveyVehicle { get; set; }
            public Survey Survey { get; set; }
        }
    }
}
