﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPM.Model.ApiModels
{
    public class AccountModel
    {
        public class LoginResult
        {
            public string Token { get; set; }
            public int VehicleTypeId { get; set; } = 0;
            public string CurrentEvent { get; set; }
        }

        public class LoginRequest
        {
            public string MacAddress { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public int VehicleId { get; set; }
        }

        public class MacAddressAddModel
        {
            public string MacAddress { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public int VehicleId { get; set; }
        }
    }
}
