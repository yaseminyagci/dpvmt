﻿using System;
using System.Collections.Generic;

namespace DPM.Model.ApiModels
{
    public class UserModel
    {
        public UserModel()
        {
            UserGuid = System.Guid.NewGuid();
            CurrentState = 2;
            Role = 3;
            CreatedDate = System.DateTime.Now;
            Status = 1;
            UserTokens = new List<UserToken>();
        }
        public int UserId { get; set; } // UserID (Primary key)
        public string FirstName { get; set; } // FirstName (length: 255)
        public string LastName { get; set; } // LastName (length: 255)
        public string Email { get; set; } // Email (length: 255)
        public string Password { get; set; } // Password (length: 255)
        public System.Guid UserGuid { get; set; } // UserGuid
        public int CurrentState { get; set; } // CurrentState
        public byte Role { get; set; } // Role
        public System.DateTime CreatedDate { get; set; } // CreatedDate
        public byte Status { get; set; } // Status
        public System.DateTime? UpdateDate { get; set; } // UpdateDate
        List<UserToken> UserTokens { get; set; }
    


    }

}
