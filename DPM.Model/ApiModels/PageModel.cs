﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPM.Model.ApiModels
{
    public class PageModel
    {
        public class EventLogs
        {
            public string Event { get; set; }
            public DateTime EventStart { get; set; }
            public DateTime EventEnd { get; set; }
            public TimeSpan Time { get; set; }
            public string UsedVehicle { get; set; }
        }

        public class DetailPageModel
        {
            public string CurrentVehicle { get; set; }
            public string CurrentEvent { get; set; }
            public List<EventLogs> Logs { get; set; }
        }

        public class EventResponseModel
        {
            public int EventId { get; set; }
            public string CurrentEvent { get; set; }
        }

        public class ChartModel
        {
            public double[] Rates = new double[4];
            public TimeSpan TotalSession { get; set; }
        }
    }
}
