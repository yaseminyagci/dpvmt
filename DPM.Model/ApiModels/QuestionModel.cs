﻿using System.Collections.Generic;

namespace DPM.Model.ApiModels
{
    public class QuestionModel
    {
        public class QuestionAddPostModel
        {
            public string QuestionText { get; set; }
            public int CategoryId { get; set; }
            public int GroupType { get; set; }
            public int[] VehicleId { get; set; }
        }

        public class QuestionResponseModel
        {
            public int QuestionId { get; set; }
            public string QuestionText { get; set; }
            public string Class { get; set; }

        }

        public class CategorizedQuestionModel
        {

            public List<QuestionResponseModel> Questions { get; set; } = new List<QuestionResponseModel>();
            public string CategoryText { get; set; }
            public string CategoryName { get; set; }
            private int _Partial { get; set; }
            public int Partial
            {
                get
                {
                    return _Partial;
                }
                set
                {
                    switch (value)
                    {
                        case 1:
                            _Partial = 12;
                            break;
                        case 2:
                            _Partial = 6;
                            break;
                        case 3:
                            _Partial = 4;
                            break;
                        case 4:
                            _Partial = 3;
                            break;
                        default:
                            _Partial = 4;
                            break;
                    }
                }
            }
            public int CategoryIndex { get; set; }
            public string ActiveClass { get; set; }


        }

        public class QuestionReturnModel
        {
            public List<CategorizedQuestionModel> CategorizedModel { get; set; } = new List<CategorizedQuestionModel>();
            public string VehicleName { get; set; }
        }

        public class EventEndQuestionModel
        {
            public string CurrentEvent { get; set; }
            public bool AnyFinishSurvey { get; set; }
        }

        public class AdminQuestionModel
        {
            public int QuestionId { get; set; }
            public int GroupType { get; set; }
            public string Text { get; set; }
            public string Category { get; set; }
            public string VehicleType { get; set; }
        }

        public class AdminQuestionPostModel
        {
            public List<AdminQuestionModel> Questions = new List<AdminQuestionModel>();
            public string VehicleTypeName { get; set; }
        }

        public class GetQuestionModel
        {
            public int QuestionId { get; set; }
            public string Question { get; set; }
            public int Category { get; set; }
            public int Group { get; set; }
            public int[] Vehicle { get; set; }
        }
    }
}
