﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPM.Model.ApiModels
{
    public class AdminModel
    {
        public class Buttons
        {
            public bool BreakButton { get; set; }
            public bool MealButton { get; set; }
            public bool FaultButton { get; set; }
            public bool RelayEndButton { get; set; }
        }

        public class NotificationOptions
        {
            public bool ACategoryMail { get; set; }
            public bool ACategoryFault { get; set; }

            public bool BCategoryMail { get; set; }
            public bool BCategoryFault { get; set; }

            public bool CCategoryMail { get; set; }
            public bool CCategoryFault { get; set; }
        }

    
}
}
