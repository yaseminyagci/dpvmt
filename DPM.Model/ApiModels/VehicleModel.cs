﻿using System;
using System.Collections.Generic;

namespace DPM.Model.ApiModels
{
    public class VehicleModel
    {
        public class VehiclePostModel
        {
            public string VehicleName { get; set; }
            public string VehicleCode { get; set; }
            public int VehicleType { get; set; }
            public string Description { get; set; }
            public string SerialNumber { get; set; } // SerialNumber (length: 255)
            public string Model { get; set; } // Model (length: 255)
            public string Ip { get; set; } // IP (length: 255)
            public int? ItemId { get; set; } // ItemID 
            public string MacAddress { get; set; } // MacAddress (length: 255)
        }
        public class VehicleTypePostModel
        {
            public string Code { get; set; }
            public string Description { get; set; }
        }

        public class VehicleTypeForAdminModel
        {
            public int Id { get; set; }
            public string Name { get; set; }


        }

        public class VehicleGetModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string VehicleType { get; set; }
            public string Description { get; set; }
            public string SerialNumber { get; set; } // SerialNumber (length: 255)
            public string Model { get; set; } // Model (length: 255)
            public string Ip { get; set; } // IP (length: 255)
            public int? ItemId { get; set; } // ItemID 
            public string MacAddress { get; set; } // MacAddress (length: 255)
        }

        public class AdminVehicleModel
        {
            public int VehicleId { get; set; }
            public string VehicleName { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime? UpdatedDate { get; set; }
            public List<string> Operators { get; set; }
            public string Actions { get; set; }
            public string VehicleType { get; set; }
            public string Description { get; set; }
            public string SerialNumber { get; set; } // SerialNumber (length: 255)
            public string Model { get; set; } // Model (length: 255)
            public string Ip { get; set; } // IP (length: 255)
            public int? ItemId { get; set; } // ItemID 
            public string MacAddress { get; set; } // MacAddress (length: 255)
        }

        public class AdminVehiclePostModel
        {
            public List<AdminVehicleModel> Vehicles = new List<AdminVehicleModel>();
            public string VehicleTypeName { get; set; }
        }

        public class AdminVehicleStateModel
        {
            public string VehicleName { get; set; }
            public string VehicleCode { get; set; }
            public int Vehicletype { get; set; }
            public string CurrentState { get; set; }
            public int CurrentStateType { get; set; }
            public int? UserId { get; set; }
            public string UserName { get; set; } = string.Empty;
            public int VehicleId { get; set; }
            public int Order { get; set; }
        }

        public class AdminVehicleStatePostModel
        {
            private int _rowCount { get; set; } = 1;

            public List<AdminVehicleStateModel> VehicleStates = new List<AdminVehicleStateModel>();
            public string VehcileTypeName { get; set; }
            public int rowCount
            {
                get
                {
                    return _rowCount;
                }
                set
                {
                    if (value > 4)
                        _rowCount = value / 4;

                }
            }
        }

        public class AdminVehicleState
        {
            public List<AdminVehicleStatePostModel> States = new List<AdminVehicleStatePostModel>();
            public int VehicleType { get; set; } = 3;
        }
    }
}
