﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomKeyBoard
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            SetStartup(Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location));
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            int Width = Screen.PrimaryScreen.Bounds.Width;
            int Height = Screen.PrimaryScreen.Bounds.Height;
            this.Location =new Point(Width-136, 0);
        }
        private void KeyBoard(object sender, EventArgs e)
        {
            try
            {
                Process myProcess = new Process();
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.FileName = @"c:\Windows\Sysnative\cmd.exe";
                myProcess.StartInfo.Arguments = "/c osk.exe";
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;

                myProcess.Start();
            }
            catch (Exception exp)
            {
                MessageBox.Show("Bir hata meydana geldi : " +exp.Message.ToString());
            }
        }
        private void SetStartup(string appName)
        {
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (!IsStartupItem(appName))
            {
                rkApp.SetValue(appName, Application.ExecutablePath.ToString());
            }
        }

        private bool IsStartupItem(string appName)
        {
            // The path to the key where Windows looks for startup applications
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (rkApp.GetValue(appName) == null)
            {
                // The value doesn't exist, the application is not set to run at startup

                return false;
            }
            else
            {
                if (rkApp.GetValue(appName).ToString() != Application.ExecutablePath.ToString())
                {
                   
                    rkApp.DeleteValue(appName);
                    rkApp.SetValue(appName, Application.ExecutablePath.ToString());
                }
               string a= rkApp.GetValue(appName).ToString();
            }
                return true;
        }
       
    }
}
