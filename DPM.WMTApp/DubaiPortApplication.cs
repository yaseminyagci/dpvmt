﻿using DPM.WMTApp.Helpers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace DPM.WMTApp
{
    public partial class DubaiPortApplication : Form
    {
        public DubaiPortApplication()
        {
            string path = Directory.GetCurrentDirectory() + "\\KeyBoard\\CustomKeyBoard.exe";
            KeyBoard(path);

            Process.Start(@"C:\Windows\System32\taskkill.exe", @"/F /IM explorer.exe");

            InitializeComponent();
        }

        public static string VehicleItemID { get; set; }

        private void DubaiPortApplication_Load(object sender, EventArgs e)
        {

            #region Full Screen
            this.WindowState = FormWindowState.Normal;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.BringToFront();

            #endregion



            SetStartup(Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location));

            WebBrowserHelper.FixBrowserVersion();


            RunWebBrowser(ConfigurationManager.AppSettings["currenturl"].ToString());


        }

        private void WebBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)

        {

            switch (e.Url.LocalPath + e.Url.Query)
            {
                case "/web/auth/login":
                    SetValue("vehicle-item-id", VehicleItemID);
                    break;
                case "/web/home/detail?vehicle=run":


                    string path = @"C:\apps\zodiac\RDT\HandHeld.exe";
                    string path2 = @"C:\app\zodiac\RDT\HandHeld.exe";

                    bool fileExist = File.Exists(path);
                    bool fileExist2 = File.Exists(path2);

                    if (fileExist == true)
                    {
                        RunZodiac(@"C:\apps\zodiac\RDT\HandHeld.exe");
                    }
                    else if (fileExist2 == true)
                    {
                        RunZodiac(@"C:\app\zodiac\RDT\HandHeld.exe");
                    }



                    break;
                case "/web/vehicle/fault":
                    SetValue("macaddress", MacAddressHelper.GetMACAddress());
                    break;
                default:
                    break;
            }
        }

        private void RunWebBrowser(string url)
        {
            webBrowser1.Navigate(url);
            webBrowser1.IsWebBrowserContextMenuEnabled = false;
            webBrowser1.AllowWebBrowserDrop = false;
        }

        private void SetValue(string id, object value)
        {
            webBrowser1.Document.GetElementById(id).SetAttribute("value", value.ToString());
        }

        private void RunZodiac(string exePath)
        {
            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            startInfo.FileName = exePath;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.

                Process exeProcess = Process.Start(startInfo);

                // startKeyboard();
                exeProcess.WaitForExit();


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }
        private void KeyBoard(string exePath)
        {
            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = exePath;

            try
            {
                Process exeProcess = Process.Start(startInfo);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }

        private void WebBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            switch (e.Url.LocalPath + e.Url.Query)
            {
                case "/web/auth/login":
                    VehicleItemID = GetVehicleItemID();
                    break;
                default:
                    break;
            }
        }

        private string GetVehicleItemID()
        {
            string path = $@"C:\apps\zodiac\RDT";

            string path2 = $@"C:\app\zodiac\RDT";

            // Determine whether the directory exists.
            if (Directory.Exists(path))
            {
                if (!File.Exists(path+"\\Local.ini"))
                {
                    File.Create(path+"\\Local.ini");
                }
            }
            else if (Directory.Exists(path2))
            {

                if (!File.Exists(path2+ "\\Local.ini"))
                {
                    File.Create(path2 + "\\Local.ini");
                }
            }
            string[] configTextSplitted = new string[2]; 

            if (Directory.Exists(path))
            {
                configTextSplitted = File.ReadAllText(path + "\\Local.ini").Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            }
            else if (Directory.Exists(path2))
            {
                configTextSplitted = File.ReadAllText(path2 + "\\Local.ini").Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            }
            var configText = configTextSplitted[1].Split('=')[1];

            return configText;

        }

        private void SetStartup(string appName)
        {
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            string keyboardPath = "";
            if (!IsStartupItem(appName))
            {

                keyboardPath = Application.ExecutablePath.ToString();
                keyboardPath = keyboardPath.Replace("DPM.WMTApp.exe", "KeyBoard\\CustomKeyBoard.exe");
                rkApp.SetValue(appName, Application.ExecutablePath.ToString());
                rkApp.SetValue("keyboard", keyboardPath);
            }

        }

        private bool IsStartupItem(string appName)
        {
            string keyboardPath = "";
            // The path to the key where Windows looks for startup applications
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (rkApp.GetValue(appName) == null || rkApp.GetValue("keyboard") == null)
            {
                // The value doesn't exist, the application is not set to run at startup

                return false;
            }
            else
            {
                if (rkApp.GetValue(appName).ToString() != Application.ExecutablePath.ToString())
                {

                    rkApp.DeleteValue(appName);
                    rkApp.SetValue(appName, Application.ExecutablePath.ToString());
                    /*Keyboard*/
                    rkApp.DeleteValue("keyboard");
                    keyboardPath = Application.ExecutablePath.ToString();
                    keyboardPath = keyboardPath.Replace("DPM.WMTApp.exe", "KeyBoard\\CustomKeyBoard.exe");
                    rkApp.SetValue("keyboard", keyboardPath);
                }
                string a = rkApp.GetValue(appName).ToString();
            }
            return true;
        }



        private void DubaiPortApplication_FormClosed(object sender, FormClosedEventArgs e)
        {
            Process.Start(@"explorer.exe");
        }
    }
}
