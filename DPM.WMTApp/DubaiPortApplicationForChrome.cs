﻿using CefSharp;
using CefSharp.WinForms;
using DPM.WMTApp.Helpers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DPM.WMTApp
{
    public partial class DubaiPortApplicationForChrome : Form
    {
        public DubaiPortApplicationForChrome()
        {
            Process.Start(@"C:\Windows\System32\taskkill.exe", @"/F /IM explorer.exe");
            InitializeComponent();
        }

        public static string VehicleItemID { get; set; }

        private void DubaiPortApplicationForChrome_Load(object sender, EventArgs e)
        { 
            #region Full Screen
            this.WindowState = FormWindowState.Normal;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.BringToFront();

            #endregion

            //SetStartup(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetExecutingAssembly().Location));

            ChromiumWebBrowser chromeBrowser;


            CefSettings settings = new CefSettings();
            Cef.Initialize(settings);
            chromeBrowser = new ChromiumWebBrowser(ConfigurationManager.AppSettings["currenturl"].ToString());
            this.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;

            chromeBrowser.RenderProcessMessageHandler = new RenderProcessMessageHandler();


        }


        public class RenderProcessMessageHandler : IRenderProcessMessageHandler
        {
            public void OnContextReleased(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame)
            {
                //throw new NotImplementedException();
            }

            public void OnFocusedNodeChanged(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, IDomNode node)
            {
                //throw new NotImplementedException();
            }

            public void OnUncaughtException(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, JavascriptException exception)
            {
                //throw new NotImplementedException();
            }

            // Wait for the underlying `Javascript Context` to be created, this is only called for the main frame.
            // If the page has no javascript, no context will be created.
            void IRenderProcessMessageHandler.OnContextCreated(IWebBrowser browserControl, IBrowser browser, IFrame frame)
            {
                //const string script = "document.addEventListener('DOMContentLoaded', function(){ alert('DomLoaded'); });";

                //frame.ExecuteJavaScriptAsync(script);

                if (frame.Url.ToLower().Contains("/web/auth/login"))
                {
                    frame.ExecuteJavaScriptAsync($"document.getElementById('vehicle-item-id').value = {GetVehicleItemID()};");
                }

                if (frame.Url.ToLower().Contains("/web/home/detail?vehicle=run"))
                {
                    string path = @"C:\apps\zodiac\RDT\HandHeld.exe";
                    string path2 = @"C:\app\zodiac\RDT\HandHeld.exe";
                    bool fileExist = File.Exists(path);
                    bool fileExist2 = File.Exists(path2);
                   
                    if (fileExist == true)
                    {
                        RunZodiac(@"C:\apps\zodiac\RDT\HandHeld.exe");
                    }
                    else if(fileExist2 == true)
                    {
                        RunZodiac(@"C:\app\zodiac\RDT\HandHeld.exe");
                    }                   
                }

                if (frame.Url.ToLower().Contains("/web/vehicle/fault"))
                {
                    frame.ExecuteJavaScriptAsync($"document.getElementById('macaddress').value = {MacAddressHelper.GetMACAddress()};");
                }
            }

            private void RunZodiac(string exePath)
            {
                // Use ProcessStartInfo class
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = false;
                startInfo.UseShellExecute = false;
                startInfo.WindowStyle = ProcessWindowStyle.Maximized;
                startInfo.FileName = exePath;

                try
                {
                    // Start the process with the info we specified.
                    // Call WaitForExit and then the using statement will close.

                    Process exeProcess = Process.Start(startInfo);
                    exeProcess.WaitForExit();
                    

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            private string GetVehicleItemID()
            {

                string path = $@"C:\apps\zodiac\RDT";

                string path2 = $@"C:\app\zodiac\RDT";

                // Determine whether the directory exists.
                if (Directory.Exists(path))
                {
                    if (!File.Exists(path + "\\Local.ini"))
                    {
                        File.Create(path + "\\Local.ini");
                    }
                }
                else if (Directory.Exists(path2))
                {

                    if (!File.Exists(path2 + "\\Local.ini"))
                    {
                        File.Create(path2 + "\\Local.ini");
                    }
                }

                string[] configTextSplitted = new string[2];

                if (Directory.Exists(path))
                {
                    configTextSplitted = File.ReadAllText(path + "\\Local.ini").Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                }
                else if (Directory.Exists(path2))
                {
                    configTextSplitted = File.ReadAllText(path2 + "\\Local.ini").Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                }

              
                var configText = configTextSplitted[1].Split('=')[1];

                return configText; 
            }
        }

    }
}
