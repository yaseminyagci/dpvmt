﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPM.WMTApp.Helpers
{
    public static class Definations
    {
        public static string GetPageURL()
        {
            return ConfigurationManager.AppSettings["currenturl"].ToString();
        }
    }
}
